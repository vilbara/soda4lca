##License##

Unless stated otherwise, all source code of the soda4LCA project is licensed under the GNU Affero General Public License, version 3 (AGPL). Please see the [LICENSE.txt](LICENSE.txt) file in the root directory of the source code.


## Documentation ##

Browse the following documentation online:

[Release Notes](Doc/src/RELEASE_NOTES.txt) 

[Installation Guide](Doc/src/Installation_Guide/Installation_Guide.md)

[Administration Guide](Doc/src/Administration_Guide/Administration_Guide.md)

[User Guide (Node)](Doc/src/Node_User_Guide/Node_User_Guide.md)

[FAQ](Doc/src/FAQ/FAQ.md)

[Service API](Doc/src/Service_API/Service_API.md)

Developer Guides:

[Developer Guide](/Doc/src/Developer_Guide/Developer_Guide.md)

[Eclipse IDE Setup](/Doc/src/Developer_Guide/Eclipse_Setup.md)



## Mailing List ##

There is a public mailing list for announcements and discussion that you can subscribe to by sending an email to

```
#!

soda4lca-request@lists.kit.edu
```

or by following the instructions at [https://www.lists.kit.edu/wws/info/soda4lca](https://www.lists.kit.edu/wws/info/soda4lca).


## Contributors ##

see [CONTRIBUTORS.txt](Doc/src/CONTRIBUTORS.txt)