package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of source resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/sources" )
public class SourceStockAwareResource extends SourceResource {

}
