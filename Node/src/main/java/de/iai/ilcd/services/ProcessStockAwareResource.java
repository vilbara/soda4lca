package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of process resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/processes" )
public class ProcessStockAwareResource extends ProcessResource {

}
