package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of unit group resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/unitgroups" )
public class UnitGroupStockAwareResource extends UnitGroupResource {

}
