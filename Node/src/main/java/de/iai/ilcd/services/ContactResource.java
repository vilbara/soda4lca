package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.contact.Contact;
import de.iai.ilcd.model.dao.ContactDao;
import de.iai.ilcd.model.dao.DataSetDao;

/**
 * REST Web Service for Contact
 */
@Component
@Path( "contacts" )
public class ContactResource extends AbstractDataSetResource<Contact> {

	public ContactResource() {
		super( DataSetType.CONTACT, ILCDTypes.CONTACT );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataSetDao<Contact, ?, ?> getFreshDaoInstance() {
		return new ContactDao();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getXMLTemplatePath() {
		return "/xml/contact.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDataSetTypeName() {
		return StringUtils.lowerCase( DataSetType.CONTACT.name() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getHTMLDatasetDetailTemplatePath() {
		return "/html/contact.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean userRequiresDatasetDetailRights() {
		return false;
	}

}
