package de.iai.ilcd.services;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.api.binding.generated.common.ExchangeDirectionValues;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.FlowDaoWrapper;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.dao.ProductFlowDao;
import de.iai.ilcd.model.flow.ElementaryFlow;
import de.iai.ilcd.model.flow.Flow;
import de.iai.ilcd.model.flow.ProductFlow;
import de.iai.ilcd.model.process.Process;

/**
 * REST Web Service for Flow
 */
@Component
@Path( "flows" )
public class FlowResource extends AbstractDataSetResource<Flow> {

	public FlowResource() {
		super( DataSetType.FLOW, ILCDTypes.FLOW );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDatasetDetailUrl( Flow dataset, String uuid, String version, String stockName, String language ) {
		return "../datasetdetail/" + (dataset instanceof ElementaryFlow ? "elementary" : "product") + "Flow.xhtml?uuid=" + uuid
				+ (StringUtils.isNotBlank( version ) ? "&version=" + version : "") + (StringUtils.isNotBlank( stockName ) ? "&stock=" + stockName : "")
				+ (StringUtils.isNotBlank( language ) ? "&lang=" + language : "");
	}

	/**
	 * Get availability of producers for specified flow (processes with this flow as {@link ExchangeDirection#OUTPUT
	 * output} flow)
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @return empty {@link Response} with status {@link Status#OK OK} if producers for specified UUID are available,
	 *         else with status {@link Status#NO_CONTENT NO_CONTENT}
	 */
	@HEAD
	@Path( "{uuid}/producers" )
	@Produces( "text/plain" )
	public Response getAvailabilityOfProducers( @PathParam( "uuid" ) String uuid ) {
		return this.getAvailabilityOfInOutputProcesses( uuid, ExchangeDirectionValues.OUTPUT );
	}

	/**
	 * Get availability of consumers for specified flow (processes with this flow as {@link ExchangeDirection#INPUT
	 * input} flow)
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @return empty {@link Response} with status {@link Status#OK OK} if consumers for specified UUID are available,
	 *         else with status {@link Status#NO_CONTENT NO_CONTENT}
	 */
	@HEAD
	@Path( "{uuid}/consumers" )
	@Produces( "text/plain" )
	public Response getAvailabilityOfConsumers( @PathParam( "uuid" ) String uuid ) {
		return this.getAvailabilityOfInOutputProcesses( uuid, ExchangeDirectionValues.INPUT );
	}

	/**
	 * Get HEAD response for input (consumer) or output (producer) request
	 * 
	 * @param flowUuid
	 *            UUID of the flow in question
	 * @param direction
	 *            the direction of the exchange
	 * @return empty {@link Response} with status {@link Status#OK OK} if processes for specified UUID and direction are
	 *         available, else with status {@link Status#NO_CONTENT NO_CONTENT}
	 * @see #getAvailabilityOfProducers(String)
	 * @see #getAvailabilityOfConsumers(String)
	 */
	private Response getAvailabilityOfInOutputProcesses( String flowUuid, ExchangeDirectionValues direction ) {
		ProcessDao dao = new ProcessDao();
		Response.Status status;
		if ( dao.getProcessesForExchangeFlowCount( flowUuid, direction ) > 0 ) {
			status = Response.Status.OK;
		}
		else {
			status = Response.Status.NO_CONTENT;
		}
		return Response.status( status ).type( "text/plain" ).build();
	}

	/**
	 * Get availability of producers for specified flow (processes with this flow as {@link ExchangeDirection#OUTPUT
	 * output} flow)
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param startIndex
	 *            start index
	 * @param pageSize
	 *            page size
	 * @param format
	 *            format
	 * @param callback
	 *            callback for JSONP
	 * @return empty {@link Response} with status {@link Status#OK OK} if producers for specified UUID are available,
	 *         else with status {@link Status#NO_CONTENT NO_CONTENT}
	 */
	@GET
	@Path( "{uuid}/producers" )
	@Produces( { "application/xml", "application/javascript" } )
	public Response getProducers( @PathParam( "uuid" ) String uuid, @DefaultValue( "0" ) @QueryParam( "startIndex" ) final int startIndex,
			@DefaultValue( "500" ) @QueryParam( "pageSize" ) final int pageSize, @QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format,
			@DefaultValue( "fn" ) @QueryParam( "callback" ) String callback, @QueryParam( "lang" ) String language ) {

		return this.getInOutputProcesses( uuid, ExchangeDirectionValues.OUTPUT, startIndex, pageSize, format, callback, language );
	}

	/**
	 * Get availability of consumers for specified flow (processes with this flow as {@link ExchangeDirection#INPUT
	 * input} flow)
	 * 
	 * @param uuid
	 *            UUID of the flow
	 * @param startIndex
	 *            start index
	 * @param pageSize
	 *            page size
	 * @param format
	 *            format
	 * @param callback
	 *            callback for JSONP
	 * @return empty {@link Response} with status {@link Status#OK OK} if consumers for
	 *         specified UUID are available,
	 *         else with status {@link Status#NO_CONTENT NO_CONTENT}
	 */
	@GET
	@Path( "{uuid}/consumers" )
	@Produces( { "application/xml", "application/javascript" } )
	public Response getConsumers( @PathParam( "uuid" ) String uuid, @DefaultValue( "0" ) @QueryParam( "startIndex" ) final int startIndex,
			@DefaultValue( "500" ) @QueryParam( "pageSize" ) final int pageSize, @QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format,
			@DefaultValue( "fn" ) @QueryParam( "callback" ) String callback, @QueryParam( "lang" ) String language ) {

		return this.getInOutputProcesses( uuid, ExchangeDirectionValues.INPUT, startIndex, pageSize, format, callback, language );
	}

	/**
	 * Get GET response for input (consumer) or output (producer) request
	 * 
	 * @param flowUuid
	 *            UUID of the flow
	 * @param direction
	 *            the direction of the exchange
	 * @param startIndex
	 *            index of the first item (for pagination)
	 * @param pageSize
	 *            pagination page size
	 * @return created output
	 * @see #getResponseForDatasetListVOList(List, int, int, int, String, String)
	 */
	private Response getInOutputProcesses( String flowUuid, ExchangeDirectionValues direction, int startIndex, int pageSize, String format, String callback,
			String language ) {
		ProcessDao dao = new ProcessDao();
		long count = dao.getProcessesForExchangeFlowCount( flowUuid, direction );
		List<Process> lst = dao.getProcessesForExchangeFlow( flowUuid, direction, startIndex, pageSize );
		if ( CollectionUtils.isEmpty( lst ) ) {
			return Response.noContent().build();
		}
		return super.getResponseForDatasetListVOList( lst, (int) count, startIndex, pageSize, format, callback, language );
	}

	/**
	 * Get the ancestor of specified product flow
	 * 
	 * @param uuid
	 *            UUID of flow in question
	 * @param productType
	 *            <code>nothing | specific | generic</code>
	 * @return ancestors of specified product flow
	 */
	@GET
	@Path( "{uuid}/ancestor" )
	@Produces( "application/xml" )
	public Response getAncestor( @PathParam( "uuid" ) String uuid, @QueryParam( "productType" ) String productType ) {
		ProductFlowDao pflowDao = new ProductFlowDao();
		Boolean specific = this.parseProductType( productType );
		ProductFlow pFlow = pflowDao.getAncestor( uuid, specific );

		if ( pFlow == null ) {
			return Response.status( Response.Status.NO_CONTENT ).entity( "No ancestor found for provided product flow UUID" ).type( MediaType.TEXT_PLAIN ).build();
		}
		else {
			return Response.ok( this.generateOverview( pFlow, AbstractDataSetResource.FORMAT_XML ), MediaType.APPLICATION_XML ).build();
		}

	}

	/**
	 * Get the ancestors of specified product flow
	 * 
	 * @param uuid
	 *            UUID of flow in question
	 * @param maxDepth
	 *            max depth to follow
	 * @param format
	 *            format
	 * @param callback
	 *            callback for JSONP
	 * @return ancestors of specified product flow
	 */
	@GET
	@Path( "{uuid}/ancestors" )
	@Produces( { "application/xml", "application/javascript" } )
	public Response getAncestors( @PathParam( "uuid" ) String uuid, @DefaultValue( "0" ) @QueryParam( "depth" ) final int maxDepth,
			@QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format, @DefaultValue( "fn" ) @QueryParam( "callback" ) String callback,
			@QueryParam( "lang" ) String language ) {
		ProductFlowDao pflowDao = new ProductFlowDao();
		List<ProductFlow> lst = pflowDao.getAncestors( uuid, maxDepth );
		return super.getResponseForDatasetListVOList( lst, (int) lst.size(), 0, lst.size(), format, callback, language );
	}

	/**
	 * Get the descendants of specified product flow
	 * 
	 * @param uuid
	 *            UUID of flow in question
	 * @param maxDepth
	 *            max depth to follow
	 * @param format
	 *            format
	 * @param callback
	 *            callback for JSONP
	 * @return descendants of specified product flow
	 */
	@GET
	@Path( "{uuid}/descendants" )
	@Produces( { "application/xml", "application/javascript" } )
	public Response getDescendants( @PathParam( "uuid" ) String uuid, @DefaultValue( "0" ) @QueryParam( "depth" ) final int maxDepth,
			@QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format, @DefaultValue( "fn" ) @QueryParam( "callback" ) String callback,
			@QueryParam( "lang" ) String language ) {
		ProductFlowDao pflowDao = new ProductFlowDao();
		List<ProductFlow> lst = pflowDao.getDescendants( uuid, maxDepth );
		return super.getResponseForDatasetListVOList( lst, (int) lst.size(), 0, lst.size(), format, callback, language );
	}

	/**
	 * Get the products
	 * 
	 * @param startIndex
	 *            start index
	 * @param pageSize
	 *            page size
	 * @param productType
	 *            <code>nothing | specific | generic</code>
	 * @param format
	 *            format
	 * @param callback
	 *            callback for JSONP
	 * @return products
	 */
	@GET
	@Path( "products" )
	@Produces( "application/xml" )
	public Response getProducts( @QueryParam( "startIndex" ) final int startIndex, @DefaultValue( "500" ) @QueryParam( "pageSize" ) final int pageSize,
			@QueryParam( "productType" ) String productType, @QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format,
			@DefaultValue( "fn" ) @QueryParam( "callback" ) String callback, @QueryParam( "lang" ) String language ) {
		ProductFlowDao dao = new ProductFlowDao();
		Boolean specific = this.parseProductType( productType );

		return super.getResponseForDatasetListVOList( dao.getProducts( specific, startIndex, pageSize ), dao.getProductsCount( specific ).intValue(),
				startIndex, pageSize, format, callback, language );
	}

	/**
	 * Parse product type from URL (all ignoring case):
	 * <ul>
	 * <li><code>specific</code> &rArr; {@link Boolean#TRUE}</li>
	 * <li><code>generic</code> &rArr; {@link Boolean#FALSE}</li>
	 * <li><i>everything else</i> &rArr; <code>null</code></li>
	 * <li></li>
	 * </ul>
	 * 
	 * @param productType
	 *            product type to parse
	 * @return parsed product type
	 */
	private Boolean parseProductType( String productType ) {
		productType = StringUtils.lowerCase( productType );
		if ( "specific".equals( productType ) ) {
			return Boolean.TRUE;
		}
		if ( "generic".equals( productType ) ) {
			return Boolean.FALSE;
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataSetDao<Flow, ?, ?> getFreshDaoInstance() {
		return new FlowDaoWrapper();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getXMLTemplatePath() {
		return "/xml/flow.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDataSetTypeName() {
		return StringUtils.lowerCase( DataSetType.FLOW.name() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getHTMLDatasetDetailTemplatePath() {
		return "/html/flow.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean userRequiresDatasetDetailRights() {
		return false;
	}

}
