package de.iai.ilcd.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.iai.ilcd.model.adapter.DataStockListAdapter;
import de.iai.ilcd.model.dao.CommonDataStockDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.datastock.DataStockMetaData;
import de.iai.ilcd.security.SecurityUtil;
import de.iai.ilcd.security.UserAccessBean;
import de.iai.ilcd.webgui.controller.admin.DataExportController;

/**
 * REST web service for data stocks
 */
@Component
@Path( "datastocks" )
public class DataStockResource {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( DataStockResource.class );

	/**
	 * Get list of data stocks
	 * 
	 * @return an instance of javax.ws.rs.core.StreamingOutput
	 */
	@GET
	@Produces( "application/xml" )
	public StreamingOutput getDataStocks() {
		UserAccessBean user = new UserAccessBean();
		CommonDataStockDao stockDao = new CommonDataStockDao();
		List<AbstractDataStock> stocks = stockDao.getAllReadable( user.getUserObject() );

		final DataStockListAdapter adapter = new DataStockListAdapter( stocks );
		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO dao = new ServiceDAO();
				dao.setRenderSchemaLocation( true );
				try {
					dao.marshal( adapter, out );
				}
				catch ( JAXBException e ) {
					DataStockResource.LOGGER.error( "error marshalling data", e );
					// if ( e.getCause().getCause() instanceof SocketException ) {
					// DataStockResource.LOGGER.warn( "exception occurred during marshalling - " + e );
					// }
					// else {
					// DataStockResource.LOGGER.error( "error marshalling data", e );
					// }
				}
			}
		};

	}

	/**
	 * Get data stock
	 * 
	 * @param stockIdentifier
	 *            stock identifier (name or UUID)
	 * @return an instance of javax.ws.rs.core.StreamingOutput
	 */
	@GET
	@Path( "{stockIdentifier}" )
	@Produces( "application/xml" )
	public StreamingOutput getDataStock( @PathParam( "stockIdentifier" ) String stockIdentifier ) {
		CommonDataStockDao stockDao = new CommonDataStockDao();
		AbstractDataStock ads = stockDao.getDataStockByIdentifier( stockIdentifier );

		SecurityUtil.assertCanRead( ads );

		final DataStockVO dsVo = DataStockListAdapter.getServiceApiVo( ads );

		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO dao = new ServiceDAO();
				dao.setRenderSchemaLocation( true );
				try {
					dao.marshal( dsVo, out );
				}
				catch ( JAXBException e ) {
					DataStockResource.LOGGER.error( "error marshalling data", e );
				}
			}
		};

	}

	/**
	 * Export data stock
	 * 
	 * @param stockIdentifier
	 *            stock identifier (name or UUID)
	 * @return an instance of javax.ws.rs.core.StreamingOutput
	 */
	@GET
	@Path( "{stockIdentifier}/export" )
	@Produces( "application/zip" )
	public Response export( @PathParam( "stockIdentifier" ) String stockIdentifier) {

		CommonDataStockDao stockDao = new CommonDataStockDao();

		AbstractDataStock ads = stockDao.getDataStockByIdentifier( stockIdentifier );

		if ( LOGGER.isDebugEnabled() )
			LOGGER.debug( "exporting datastock " + ads.getName() );

		try {
			SecurityUtil.assertCanExport( ads );
		}
		catch ( AuthorizationException e1 ) {
			return Response.status( Response.Status.FORBIDDEN ).type( "text/plain" ).build();
		}

		InputStream in = null;

		if ( !ads.getExportTag().isModified() ) {
			if ( LOGGER.isDebugEnabled() )
				LOGGER.debug( "serving cached file " + ads.getExportTag().getFile() );
			try {
				in = new FileInputStream( ads.getExportTag().getFile() );
			}
			catch ( IOException e ) {
				LOGGER.info( "cached zip file not found, generating fresh one" );
				in = doExport( ads );
			}
		}
		else {
			if ( LOGGER.isDebugEnabled() )
				LOGGER.debug( "cached file is not up-to-date, generating fresh one" );
			in = doExport( ads );
		}

		final InputStream inputStream = in;

		StreamingOutput stream = new StreamingOutput() {
			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				IOUtils.copy( inputStream, out );
			}
		};

		return Response.ok( stream ).header( "Content-Disposition", "attamchment; filename=" + ads.getName() + ".zip" ).build();
		
	}

	private InputStream doExport( AbstractDataStock ads ) {
		DataExportController exportController = new DataExportController();
		exportController.setExportAllVersions( false );
		exportController.setStock( new DataStockMetaData( ads ) );
		return exportController.getFile().getStream();
	}
	
}
