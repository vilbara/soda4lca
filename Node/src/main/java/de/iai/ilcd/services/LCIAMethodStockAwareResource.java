package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of LCIA method resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/lciamethods" )
public class LCIAMethodStockAwareResource extends LCIAMethodResource {

}
