package de.iai.ilcd.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.service.client.impl.vo.AuthenticationInfo;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.iai.ilcd.model.dao.CommonDataStockDao;
import de.iai.ilcd.model.dao.UserDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.security.IlcdSecurityRealm;
import de.iai.ilcd.security.ProtectionType;
import de.iai.ilcd.security.StockAccessRight;
import de.iai.ilcd.security.StockAccessRightDao;
import de.iai.ilcd.security.UserAccessBean;

/**
 * REST Web Service
 * 
 * @author clemens.duepmeier
 */

@Component
@Path( "authenticate" )
public class AuthenticateResource {

	private static final Logger logger = LoggerFactory.getLogger( AuthenticateResource.class );

	@Context
	private UriInfo context;

	/** Creates a new instance of AuthenticateResource */
	public AuthenticateResource() {
	}

	@GET
	@Path( "status" )
	@Produces( "application/xml" )
	public AuthenticationInfo status() {
		logger.debug( "authenticate/status" );

		UserAccessBean user = new UserAccessBean();

		AuthenticationInfo authInfo = new AuthenticationInfo();

		boolean authenticated = user.isLoggedIn();

		authInfo.setAuthenticated( authenticated );
		logger.debug( "user authenticated: " + authenticated );

		if ( !authenticated ) {
			return authInfo;
		}

		authInfo.setUserName( user.getUserName() );
		logger.debug( "username: " + authInfo.getUserName() );

		if ( user.hasAdminAreaAccessRight() )
			authInfo.getRoles().add( "ADMIN" );
		if ( user.hasSuperAdminPermission() )
			authInfo.getRoles().add( "SUPER_ADMIN" );

		CommonDataStockDao stockDao = new CommonDataStockDao();
		StockAccessRightDao sarDao = new StockAccessRightDao();

		List<AbstractDataStock> readableStocks = stockDao.getAllReadable( user.getUserObject() );

		for ( AbstractDataStock ads : readableStocks ) {
			DataStockVO stockVO = new DataStockVO();
			stockVO.setUuid( ads.getUuidAsString() );
			stockVO.setShortName( ads.getName() );

			List<StockAccessRight> permissions = sarDao.get( user.getUserObject() );
			for ( StockAccessRight permission : permissions ) {
				if ( permission.getStockId() == ads.getId() ) {
					ProtectionType[] roles = ProtectionType.toTypes( permission.getValue() );
					for ( ProtectionType protectionType : roles ) {
						if ( !stockVO.getRoles().contains( protectionType.name() ) )
							stockVO.getRoles().add( protectionType.name() );
					}
				}
			}
			authInfo.getDataStockRoles().add( stockVO );
		}

		return authInfo;

	}

	/**
	 * Retrieves representation of an instance of de.iai.ilcd.services.AuthenticateResource
	 * 
	 * @return an instance of java.lang.String
	 */
	@GET
	@Path( "login" )
	@Produces( "text/plain" )
	public String login( @QueryParam( "userName" ) String userName, @QueryParam( "password" ) String password) {
		// TODO return proper representation object
		logger.info( "authenticate/login" );
		Subject currentUser = SecurityUtils.getSubject();
		if ( !currentUser.isAuthenticated() ) {
			// no user currently logged in
			if ( userName == null || password == null ) {
				Response.status( Status.UNAUTHORIZED );
				return "user name and password must have a value";
			}

			UserDao dao = new UserDao();
			String salt = dao.getSalt( userName );
			if ( salt == null ) {
				Response.status( Status.UNAUTHORIZED );
				return "incorrect password or user name";
			}

			UsernamePasswordToken token = new UsernamePasswordToken( userName, IlcdSecurityRealm.getEncryptedPassword( password, salt ) );
			// token.setRememberMe(true);
			try {
				currentUser.login( token );

				return "Login successful";
				// message.setSummary("Your login was successful");
			}
			catch ( UnknownAccountException uae ) {
				Response.status( Status.UNAUTHORIZED );
				return "unknown error while verifying credentials";
			}
			catch ( IncorrectCredentialsException ice ) {
				Response.status( Status.UNAUTHORIZED );
				return "incorrect password or user name";
			}
			catch ( LockedAccountException lae ) {
				Response.status( Status.UNAUTHORIZED );
				return "account is locked; contact administrator";
			}
			catch ( AuthenticationException au ) {
				Response.status( Status.UNAUTHORIZED );
				return "incorrect password or user name";
			}
			catch ( Exception e ) {
				Response.status( Status.UNAUTHORIZED );
				return "unknown error while verifying credentials";
			}
		}
		return "You are already logged in as a user";
	}

	@GET
	@Path( "logout" )
	@Produces( "text/plain" )
	public String logout() {
		logger.info( "authenticate/logout" );
		Subject currentUser = SecurityUtils.getSubject();
		if ( currentUser.isAuthenticated() ) {
			SecurityUtils.getSecurityManager().logout( currentUser );
			Response.status( Status.OK );
			return "successfully logged out";
		}
		// no user currently logged in
		return "currently not authenticated";
	}

}
