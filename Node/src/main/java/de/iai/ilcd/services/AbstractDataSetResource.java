package de.iai.ilcd.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.view.ParameterTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import de.fzk.iai.ilcd.api.binding.helper.DatasetHelper;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.CategoryList;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockVO;
import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.ClassType;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.fzk.iai.ilcd.service.model.IFlowListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodListVO;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.ISourceListVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.adapter.ClClassAdapter;
import de.iai.ilcd.model.adapter.DataSetListAdapter;
import de.iai.ilcd.model.adapter.DataStockListAdapter;
import de.iai.ilcd.model.common.ClClass;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.DataSetVersion;
import de.iai.ilcd.model.common.GlobalRefUriAnalyzer;
import de.iai.ilcd.model.common.exception.FormatException;
import de.iai.ilcd.model.dao.AbstractDigitalFileProvider;
import de.iai.ilcd.model.dao.ClassificationDao;
import de.iai.ilcd.model.dao.CommonDataStockDao;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.datastock.DataStock;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.model.datastock.RootDataStock;
import de.iai.ilcd.model.source.Source;
import de.iai.ilcd.security.SecurityUtil;
import de.iai.ilcd.services.primeui.PUIDatasetList;
import de.iai.ilcd.services.primeui.PUIDatasetListSerializer;
import de.iai.ilcd.util.CategoryTranslator;
import de.iai.ilcd.util.SodaUtil;
import de.iai.ilcd.util.sort.ClClassIdComparator;
import de.iai.ilcd.webgui.controller.DirtyFlagBean;
import de.iai.ilcd.webgui.controller.ui.AvailableStockHandler;
import de.iai.ilcd.xml.read.DataSetImporter;

/**
 * REST web service for data sets. In order to support data stock awareness, sub-classes must have a {@link PathParam
 * path parameter} <code>stockIdentifier</code>, e.g.:
 * 
 * <pre>
 * &#64;Path( "datastocks/{stockIdentifier}/processes" )
 * public class ProcessStockAwareResource extends ProcessResource {
 *    (...)
 * }
 * </pre>
 * 
 * @param <T>
 *            type of data set
 */
public abstract class AbstractDataSetResource<T extends DataSet> {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( AbstractDataSetResource.class );

	/**
	 * URL parameter key for view type
	 */
	public final static String PARAM_VIEW = "view";

	/**
	 * URL parameter key for format type
	 */
	public final static String PARAM_FORMAT = "format";

	/**
	 * URL parameter key for version
	 */
	public final static String PARAM_VERSION = "version";

	/**
	 * URL parameter value for HTML format type
	 */
	public final static String FORMAT_HTML = "html";

	/**
	 * URL parameter value for PrimeUI format type
	 */
	public final static String FORMAT_PRIMEUI = "primeui";

	/**
	 * URL parameter value for HTML format type
	 */
	public final static String FORMAT_HTML_ALTERNATIVE = "htmlalt";

	/**
	 * URL parameter value for XML format type
	 */
	public final static String FORMAT_XML = "xml";

	/**
	 * URL parameter value for overview view type
	 */
	public final static String VIEW_OVERVIEW = "overview";

	/**
	 * URL parameter value for dataset detail view type
	 * 
	 * @deprecated use {@link #VIEW_DETAIL}
	 */
	@Deprecated
	public final static String VIEW_FULL = "full";

	/**
	 * URL parameter value for dataset detail view type
	 */
	public final static String VIEW_DETAIL = "detail";

	/**
	 * URL parameter value for natural sort
	 */
	public final static String SORT_NATSORT = "nat";

	/**
	 * URL parameter value for meta data view type
	 */
	public final static String VIEW_METADATA = "metadata";

	/**
	 * URL parameter key for indicating sort order
	 */
	public final static String SORT = "sort";

	/**
	 * URL parameter value for indicating sort order by id
	 */
	public final static String SORT_ID = "id";

	/**
	 * URL parameter value for indicating sort order by name
	 */
	public final static String SORT_NAME = "name";

	/**
	 * Context, required for the velocity rendering
	 */
	@Context
	private UriInfo context;

	/**
	 * Headers, currently unused
	 */
	@Context
	private HttpHeaders headers;

	/**
	 * The request, required for request parameter evaluation
	 */
	@Context
	private HttpServletRequest request;

	/**
	 * The type in API definition, required for {@link #generateDataSetDetailAsHtml(DataSet)}
	 */
	private final ILCDTypes apiType;

	/**
	 * The type in model definition, required for
	 */
	private final DataSetType modelType;

	/**
	 * Create an abstract data set resource
	 * 
	 * @param modelType
	 *            model type value
	 * @param apiType
	 *            API type value
	 */
	public AbstractDataSetResource( DataSetType modelType, ILCDTypes apiType ) {
		if ( apiType == null ) {
			throw new IllegalArgumentException( "apiType must not be null!" );
		}
		if ( modelType == null ) {
			throw new IllegalArgumentException( "modelType must not be null!" );
		}

		this.apiType = apiType;
		this.modelType = modelType;
	}

	/**
	 * Create a fresh data access object to work with
	 * 
	 * @return fresh data access object
	 */
	protected abstract DataSetDao<T, ?, ?> getFreshDaoInstance();

	/**
	 * Get the path to the template for the XML single data set view This is typically something like
	 * <code>/xml/<i>$datasettype$</i>.vm</code>
	 * 
	 * @deprecated obsolete
	 * @return path to the template for the XML single data set view
	 */
	@Deprecated
	protected abstract String getXMLTemplatePath();

	/**
	 * Get the path to the template for the HTML dataset detail view page This is typically something like
	 * <code>/html/<i>$datasettype$</i>.vm</code>
	 * 
	 * @return path to the template for the HTML dataset detail view page
	 */
	protected abstract String getHTMLDatasetDetailTemplatePath();

	/**
	 * Get the name of the data set type for the creation of error/info messages
	 * 
	 * @return name of the data set type for the creation of error/info messages
	 */
	protected abstract String getDataSetTypeName();

	/**
	 * Flag to set, if dataset detail view rights shall be checked in user bean
	 * 
	 * @return <code>true</code> if dataset detail view rights shall be checked, else <code>false</code>
	 */
	protected abstract boolean userRequiresDatasetDetailRights();

	/**
	 * Retrieves representation of an instance of data set
	 * 
	 * @param search
	 *            search trigger
	 * @param startIndex
	 *            start index
	 * @param pageSize
	 *            page size
	 * @param format
	 *            format
	 * @param callback
	 *            JSONP callback
	 * @param stockName
	 *            name of data stock
	 * 
	 * @return a response object
	 */
	@GET
	@Produces( { "application/xml", "application/javascript" } )
	public Response getDataSets( @PathParam( "stockIdentifier" ) String stockIdentifier, @DefaultValue( "false" ) @QueryParam( "search" ) final boolean search,
			@DefaultValue( "0" ) @QueryParam( "startIndex" ) final int startIndex, @DefaultValue( "500" ) @QueryParam( "pageSize" ) final int pageSize,
			@QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format, @DefaultValue( "fn" ) @QueryParam( "callback" ) String callback,
			@QueryParam( "lang" ) final String language, @DefaultValue( "false" ) @QueryParam( "allVersions" ) final boolean allVersions) {

		List<? extends IDataSetListVO> dataSets;
		int count;

		IDataStockMetaData stock = null;
		if ( stockIdentifier != null ) {
			stock = this.getStockByIdentifier( stockIdentifier );
			if ( stock == null ) {
				return Response.status( Response.Status.FORBIDDEN ).type( "text/plain" ).build();
			}
		}
		IDataStockMetaData[] stocks = stock != null ? new IDataStockMetaData[] { stock } : this.getAvailableDataStocks();

		DataSetDao<T, ?, ?> daoObject = this.getFreshDaoInstance();

		if ( search ) {
			ParameterTool params = new ParameterTool( this.request );

			count = (int) daoObject.searchResultCount( params, !allVersions, stocks );
			dataSets = daoObject.search( params, startIndex, pageSize, !allVersions, stocks );
		}
		else {
			count = daoObject.getCount( stocks, language, !allVersions ).intValue();
			dataSets = daoObject.getDataSets( stocks, language, !allVersions, startIndex, pageSize );
		}

		return this.getResponseForDatasetListVOList( dataSets, count, startIndex, pageSize, format, callback, language );
	}

	/**
	 * Create a streaming output for a list of data sets
	 * 
	 * @param list
	 *            the list with the data
	 * @param totalCount
	 *            the total count of result items (for pagination)
	 * @param startIndex
	 *            index of the first item in the passed list in the total data (for pagination)
	 * @param pageSize
	 *            pagination page size
	 * @param format
	 *            output format
	 * @param callback
	 *            JSONP callback (may be <code>null</code> if format does not trigger JSON output)
	 * @param language
	 *            the language to filter for
	 * @return output for the client (which will be marshaled via JAXB)
	 */
	protected Response getResponseForDatasetListVOList( final List<? extends IDataSetListVO> list, final int totalCount, final int startIndex,
			final int pageSize, final String format, final String callback, final String language ) {

		if ( StringUtils.equalsIgnoreCase( AbstractDataSetResource.FORMAT_PRIMEUI, format ) ) {
			return Response.ok( this.getStreamingOutputForDatasetListVOListPrimeUIJSONP( list, totalCount, startIndex, pageSize, format, callback ),
					"application/javascript" ).build();
		}
		else {
			return Response.ok( this.getStreamingOutputForDatasetListVOListILCDXML( list, totalCount, startIndex, pageSize, language ),
					MediaType.APPLICATION_XML_TYPE ).build();
		}
	}

	/**
	 * Create a streaming output for a list of data sets
	 * 
	 * @param list
	 *            the list with the data
	 * @param totalCount
	 *            the total count of result items (for pagination)
	 * @param startIndex
	 *            index of the first item in the passed list in the total data (for pagination)
	 * @param pageSize
	 *            pagination page size
	 * @param format
	 *            output format
	 * @param callback
	 *            JSONP callback (may be <code>null</code> if format does not trigger JSON output)
	 * @return output for the client (which will be marshaled via JAXB)
	 */
	private StreamingOutput getStreamingOutputForDatasetListVOListPrimeUIJSONP( final List<? extends IDataSetListVO> list, final int totalCount,
			final int startIndex, final int pageSize, final String format, final String callback ) {

		return new StreamingOutput() {

			@Override
			public void write( OutputStream outStream ) throws IOException, WebApplicationException {

				GsonBuilder gsonBuilder = new GsonBuilder();
				PUIDatasetListSerializer.register( gsonBuilder );

				final String cb = StringUtils.isNotBlank( callback ) ? StringUtils.deleteWhitespace( callback ) : "fn";
				OutputStreamWriter w = new OutputStreamWriter( outStream );
				w.write( cb + "(" );
				gsonBuilder.create().toJson( new PUIDatasetList( list, pageSize, startIndex, totalCount ), w );
				w.write( " )" );
				w.flush();
			}
		};
	}

	/**
	 * Create a streaming output for a list of data sets in ILCD data set format
	 * 
	 * @param list
	 *            the list with the data
	 * @param totalCount
	 *            the total count of result items (for pagination)
	 * @param startIndex
	 *            index of the first item in the passed list in the total data (for pagination)
	 * @param pageSize
	 *            pagination page size
	 * @param language
	 *            the language to filter for
	 * @return output for the client (which will be marshaled via JAXB)
	 */
	private StreamingOutput getStreamingOutputForDatasetListVOListILCDXML( final List<? extends IDataSetListVO> list, final int totalCount,
			final int startIndex, final int pageSize, final String language ) {
		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				DataSetList resultList = new DataSetListAdapter( list, language );

				String baseUrl = ConfigurationService.INSTANCE.getNodeInfo().getBaseURL();

				// set xlink:href attribute for each dataset
				for ( DataSetVO ds : resultList.getDataSet() ) {
					StringBuffer buf = new StringBuffer( baseUrl );
					buf.append( AbstractDataSetResource.this.getURLSuffix( ds ) );
					buf.append( "/" );
					buf.append( ds.getUuidAsString() );
					ds.setHref( buf.toString() );
				}

				resultList.setTotalSize( totalCount );
				resultList.setPageSize( pageSize );
				resultList.setStartIndex( startIndex );

				DatasetVODAO dao = new DatasetVODAO();

				try {
					dao.marshal( resultList, out );
				}
				catch ( JAXBException e ) {
					if ( e.getCause().getCause() instanceof SocketException ) {
						LOGGER.warn( "exception occurred during marshalling - " + e );
					}
					else {
						LOGGER.error( "error marshalling data", e );
					}
				}
			}
		};
	}

	/**
	 * 
	 * @param dataset
	 *            data set instance
	 * @param version
	 *            version of the data set (may be blank)
	 * @param stockName
	 *            stock name (may be blank)
	 * @param uuid
	 *            UUID of data set
	 */
	protected String getDatasetDetailUrl( T dataset, String uuid, String version, String stockName, String language ) {
		return "../datasetdetail/" + this.modelType.name().toLowerCase() + ".xhtml?uuid=" + uuid + (StringUtils.isNotBlank( version ) ? "&version=" + version
				: "") + (StringUtils.isNotBlank( stockName ) ? "&stock=" + stockName : "") + (StringUtils.isNotBlank( language ) ? "&lang=" + language : "");
	}

	/**
	 * Get a data set by UUID string
	 * 
	 * @param uuid
	 *            UUID string
	 * @param versionStr
	 *            version string (may be blank / omitted)
	 * @param stockName
	 *            name of current stock (may be blank / omitted)
	 * @return XML or HTML response for client
	 */
	@GET
	@Path( "{uuid}" )
	@Produces( "application/xml, text/html" )
	public Response getDataSet( @PathParam( "stockIdentifier" ) String stockIdentifier, @PathParam( "uuid" ) String uuid,
			@QueryParam( AbstractDataSetResource.PARAM_VERSION ) String versionStr, @QueryParam( "stock" ) String stockNameLegacy,
			@QueryParam( "lang" ) String language) {

		IDataStockMetaData stock = null;
		if ( stockIdentifier == null && stockNameLegacy != null ) {
			stockIdentifier = stockNameLegacy;
		}
		if ( stockIdentifier != null ) {
			stock = this.getStockByIdentifier( stockIdentifier );
			if ( stock == null ) {
				return Response.status( Response.Status.NOT_FOUND ).type( "text/plain" ).build();
			}
		}

		IDataStockMetaData[] stocksToCheck = stock != null ? new IDataStockMetaData[] { stock } : this.getAvailableDataStocks();

		try {

			MultivaluedMap<String, String> queryParams = this.context.getQueryParameters( true );
			String view = queryParams.getFirst( AbstractDataSetResource.PARAM_VIEW );
			if ( view == null ) {
				view = AbstractDataSetResource.VIEW_DETAIL;
			}
			String format = queryParams.getFirst( AbstractDataSetResource.PARAM_FORMAT );
			if ( format == null ) {
				format = AbstractDataSetResource.FORMAT_HTML;
			}

			DataSetVersion version = null;
			if ( versionStr != null ) {
				try {
					version = DataSetVersion.parse( versionStr );
				}
				catch ( FormatException e ) {
					// nothing
				}
			}

			// fix uuid, if not in the right format
			GlobalRefUriAnalyzer analyzer = new GlobalRefUriAnalyzer( uuid );
			uuid = analyzer.getUuidAsString();

			DataSetDao<T, ?, ?> daoObject = this.getFreshDaoInstance();

			T dataset;
			if ( version != null ) {
				dataset = daoObject.getByUuidAndVersion( uuid, version );
			}
			else {
				dataset = daoObject.getByUuid( uuid );
			}

			// check the data stocks
			if ( !this.isDatasetInStocks( dataset, stocksToCheck ) ) {
				return Response.status( Response.Status.FORBIDDEN ).type( "text/plain" ).build();
			}

			// TODO: replace velocity
			if ( dataset == null ) {
				String errorTitle = this.getDataSetTypeName() + " data set not found";
				String errorMessage = "A " + this.getDataSetTypeName() + " data set with the uuid " + uuid + " cannot be found in the database";
				if ( format.equals( AbstractDataSetResource.FORMAT_HTML ) ) {
					return Response.status( Response.Status.NOT_FOUND ).entity( VelocityUtil.errorPage( this.context, errorTitle, errorMessage ) ).type(
							"text/html" ).build();
				}
				else {
					return Response.status( Response.Status.NOT_FOUND ).entity( errorMessage ).type( "text/plain" ).build();
				}
			}

			// handle detail and metadata mode by returning the XML file itself
			if ( view.equals( AbstractDataSetResource.VIEW_DETAIL ) || view.equals( AbstractDataSetResource.VIEW_FULL ) || view.equals(
					AbstractDataSetResource.VIEW_METADATA ) ) {
				final String renderEngine = ConfigurationService.INSTANCE.getHtmlRenderEngine();

				if ( (renderEngine.equals( ConfigurationService.RENDER_LEGACY ) && format.equals( AbstractDataSetResource.FORMAT_HTML )) || renderEngine.equals(
						ConfigurationService.RENDER_JSF ) && format.equals( AbstractDataSetResource.FORMAT_HTML_ALTERNATIVE ) ) {
					SecurityUtil.assertCanRead( dataset );
					return Response.ok( this.generateDataSetDetailAsHtml( dataset ), MediaType.TEXT_HTML_TYPE ).build();
				}
				else if ( (renderEngine.equals( ConfigurationService.RENDER_LEGACY ) && format.equals( AbstractDataSetResource.FORMAT_HTML_ALTERNATIVE )
						|| renderEngine.equals( ConfigurationService.RENDER_JSF ) && format.equals( AbstractDataSetResource.FORMAT_HTML )) ) {
					SecurityUtil.assertCanRead( dataset );
					try {
						return Response.seeOther( new URI( this.getDatasetDetailUrl( dataset, uuid, versionStr, (stock == null ? null : stock.getName()),
								language ) ) ).build();
					}
					catch ( Exception e ) {
						e.printStackTrace();
					}
					return null;
				}
				// return the xml file itself
				else {
					SecurityUtil.assertCanExport( dataset );
					ContentDisposition cd = ContentDisposition.type( "attachment" ).fileName( uuid + ".xml" ).build();
					return Response.ok( dataset.getXmlFile().getContent(), MediaType.APPLICATION_XML ).header( "Content-Disposition", cd ).build();
					// replaceFirst("<\\?xml-stylesheet.*\\?>", "")
				}
			}

			SecurityUtil.assertCanRead( dataset );
			// in all other cases we return the overview view as XML file
			return Response.ok( this.generateOverview( dataset, AbstractDataSetResource.FORMAT_XML ), MediaType.APPLICATION_XML ).build();
		}
		catch ( AuthorizationException e ) {
			return Response.status( Response.Status.UNAUTHORIZED ).entity( e.getMessage() ).type( "text/plain" ).build();
		}
	}

	/**
	 * Determine if data set is in one of the specified data stocks
	 * 
	 * @param dataset
	 *            data set to check
	 * @param stocksToCheck
	 *            stocks to check for
	 * @return <code>true</code> if data set is in one of the specified data stocks, <code>false</code> otherwise
	 */
	private boolean isDatasetInStocks( T dataset, IDataStockMetaData[] stocksToCheck ) {
		if ( dataset != null && ArrayUtils.isNotEmpty( stocksToCheck ) ) {
			// first: root data stock
			for ( IDataStockMetaData m : stocksToCheck ) {
				if ( m.getName().equals( dataset.getRootDataStock().getName() ) ) {
					return true;
				}
			}
			// second: non-root data stock
			for ( IDataStockMetaData m : stocksToCheck ) {
				for ( DataStock ds : dataset.getContainingDataStocks() ) {
					if ( m.getName().equals( ds.getName() ) ) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Get URL suffix for data set
	 * 
	 * @param vo
	 *            value object instance
	 * @return URL suffix
	 */
	protected String getURLSuffix( DataSetVO vo ) {
		if ( vo instanceof IProcessListVO ) {
			return DatasetTypes.PROCESSES.getValue();
		}
		else if ( vo instanceof IFlowListVO ) {
			return DatasetTypes.FLOWS.getValue();
		}
		else if ( vo instanceof IFlowPropertyListVO ) {
			return DatasetTypes.FLOWPROPERTIES.getValue();
		}
		else if ( vo instanceof IUnitGroupListVO ) {
			return DatasetTypes.UNITGROUPS.getValue();
		}
		else if ( vo instanceof ILCIAMethodListVO ) {
			return DatasetTypes.LCIAMETHODS.getValue();
		}
		else if ( vo instanceof ISourceListVO ) {
			return DatasetTypes.SOURCES.getValue();
		}
		else if ( vo instanceof IContactListVO ) {
			return DatasetTypes.CONTACTS.getValue();
		}
		else {
			return null;
		}
	}

	/**
	 * Generate overview. currently called statically with <code>type == xml</code>.
	 * 
	 * @param dataset
	 *            data set to generate overview for
	 * @param type
	 *            type of data set
	 * @return overview source to return to client
	 */
	protected String generateOverview( T dataset, String type ) {

		VelocityContext velocityContext = VelocityUtil.getServicesContext( this.context );
		velocityContext.put( "dataset", dataset );

		return VelocityUtil.parseTemplate( this.getXMLTemplatePath(), velocityContext );
	}

	/**
	 * Generate data set detail view as HTML view
	 * 
	 * @param dataset
	 *            data set to generate HTML view for
	 * @return HTML code to return to client
	 */
	private String generateDataSetDetailAsHtml( T dataset ) {
		VelocityContext velocityContext = VelocityUtil.getServicesContext( this.context );
		velocityContext.put( "dataset", dataset );

		de.fzk.iai.ilcd.api.dataset.DataSet xmlDataset = null;
		try {
			DatasetHelper helper = new DatasetHelper();
			String xmlFile = dataset.getXmlFile().getContent();
			InputStream bais = new ByteArrayInputStream( xmlFile.getBytes( "UTF-8" ) );
			xmlDataset = helper.unmarshal( bais, this.apiType );
		}
		catch ( JAXBException ex ) {
			AbstractDataSetResource.LOGGER.error( "cannot unmarshal xml information from " + this.getDataSetTypeName() );
			AbstractDataSetResource.LOGGER.error( "stack trace is: ", ex );
		}
		catch ( UnsupportedEncodingException ex ) {
			AbstractDataSetResource.LOGGER.error( "cannot unmarshal xml information from " + this.getDataSetTypeName() );
			AbstractDataSetResource.LOGGER.error( "stack trace is: ", ex );
		}
		velocityContext.put( "xmlDataset", xmlDataset );

		return VelocityUtil.parseTemplate( this.getHTMLDatasetDetailTemplatePath(), velocityContext );
	}

	/**
	 * PUT method for updating or creating an instance of ContactResource
	 * 
	 * @param content
	 *            representation for the resource
	 */
	@PUT
	@Consumes( "application/xml" )
	public void putXml( String content ) {
	}

	/**
	 * POST method for importing new process data set sent by form
	 * 
	 * @param fileInputStream
	 *            XML data to import (from form)
	 * @param stockUuid
	 *            UUID of the root stock to import to (or <code>null</code> for default)
	 * @return response for client
	 */
	@POST
	@Produces( { MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML } )
	@Consumes( "multipart/form-data" )
	public Response importByFileUpload( @FormDataParam( "file" ) InputStream fileInputStream, @FormDataParam( "stock" ) String stockUuid,
			@FormDataParam( "lang" ) String language) {
		return this.importByXml( fileInputStream, stockUuid, language );
	}

	/**
	 * POST method for importing new process data set sent directly
	 * 
	 * @param fileInputStream
	 *            XML data to import
	 * @param stockUuid
	 *            UUID of the root stock to import to (or <code>null</code> for default
	 * @return response for client
	 */
	@POST
	@Produces( { MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML } )
	@Consumes( "application/xml" )
	public Response importByXml( InputStream fileInputStream, @HeaderParam( "stock" ) String stockUuid, @HeaderParam( "lang" ) String language) {
		try {
			T ds = this.importXml( "POST import", fileInputStream, stockUuid );

			return Response.ok( this.getStreamingOutput( ds.getRootDataStock() ) ).type( MediaType.APPLICATION_XML_TYPE ).build();
		}
		catch ( IllegalAccessException e ) {
			return Response.status( Status.PRECONDITION_FAILED ).type( MediaType.TEXT_PLAIN_TYPE ).entity( e.getMessage() ).build();
		}
		catch ( IllegalArgumentException e ) {
			return Response.status( Status.BAD_REQUEST ).type( MediaType.TEXT_PLAIN_TYPE ).entity( e.getMessage() ).build();
		}
		catch ( AuthorizationException e ) {
			return Response.status( Status.FORBIDDEN ).type( MediaType.TEXT_PLAIN_TYPE ).entity( e.getMessage() ).build();
		}
		catch ( IOException e ) {
			return Response.status( Status.INTERNAL_SERVER_ERROR ).type( MediaType.TEXT_PLAIN_TYPE ).entity( e.getMessage() ).build();
		}
	}

	/**
	 * Process uploaded XML as input stream
	 * 
	 * @param inputStream
	 *            stream with XML
	 * @param stockUuid
	 *            UUID of the root stock to import to (or <code>null</code> for default
	 * @return response for client
	 */

	/**
	 * Get streaming output for data stock
	 * 
	 * @param ads
	 *            data stock instance
	 * @return streaming output
	 */
	protected StreamingOutput getStreamingOutput( AbstractDataStock ads ) {
		final DataStockVO dsVo = DataStockListAdapter.getServiceApiVo( ads );
		final StreamingOutput sout = new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO sDao = new ServiceDAO();
				try {
					sDao.marshal( dsVo, out );
				}
				catch ( JAXBException e ) {
					if ( e.getCause().getCause() instanceof SocketException ) {
						AbstractDataSetResource.LOGGER.warn( "exception occurred during marshalling - " + e );
					}
					else {
						AbstractDataSetResource.LOGGER.error( "error marshalling data", e );
					}
				}
			}
		};

		return sout;
	}

	/**
	 * Process uploaded XML as input stream
	 * 
	 * @param desc
	 *            description (mainly for logging)
	 * @param inputStream
	 *            stream with XML
	 * @param stockUuid
	 *            UUID of the root stock to import to (or <code>null</code> for default
	 * @return created data set
	 * @throws IllegalAccessException
	 *             on illegal access of resources
	 * @throws IOException
	 *             on I/O errors
	 */
	protected T importXml( String desc, InputStream inputStream, String stockUuid ) throws IllegalAccessException, IOException {
		final CommonDataStockDao dao = new CommonDataStockDao();
		AbstractDataStock ads;
		if ( StringUtils.isBlank( stockUuid ) ) {
			ads = dao.getById( IDataStockMetaData.DEFAULT_DATASTOCK_ID );
		}
		else {
			ads = dao.getDataStockByUuid( stockUuid );
			if ( !(ads instanceof RootDataStock) ) {
				throw new IllegalAccessException( "Data sets can only be imported into root data stocks!" );
			}
		}
		if ( ads == null ) {
			throw new IllegalArgumentException( "Invalid root data stock UUID specified" );
		}
		SecurityUtil.assertCanImport( ads );

		return this.importByInputStream( desc, this.modelType, inputStream, (RootDataStock) ads, null );
	}

	/**
	 * Get the category systems
	 * 
	 * @return list of category systems
	 */
	@GET
	@Path( "categorysystems" )
	@Produces( "application/xml" )
	public StreamingOutput getCategorySystems() {
		ClassificationDao cDao = new ClassificationDao();

		final CategoryList cats = new CategoryList();
		for ( String name : cDao.getCategorySystemNames() ) {
			cats.addCategory( new ClassType( name ) );
		}
		return this.getCategoriesStreamingOutput( cats );
	}

	/**
	 * Get the top categories
	 * 
	 * @param stockName
	 *            name of data stock
	 * @param catSystem
	 *            name of category system
	 * @param sort
	 *            sort field
	 * @return list of categories
	 */
	@GET
	@Path( "categories" )
	@Produces( "application/xml" )
	public Response getCategories( @PathParam( "stockIdentifier" ) String stockIdentifier, @QueryParam( "catSystem" ) String catSystem,
			@QueryParam( "sort" ) final String sort, @QueryParam( "lang" ) String language) {

		if ( catSystem == null )
			catSystem = ConfigurationService.INSTANCE.getDefaultClassificationSystem();

		if ( language == null )
			language = ConfigurationService.INSTANCE.getDefaultLanguage();

		IDataStockMetaData stock = null;
		if ( stockIdentifier != null ) {
			stock = this.getStockByIdentifier( stockIdentifier );
			if ( stock == null ) {
				return Response.status( Response.Status.FORBIDDEN ).type( "text/plain" ).build();
			}
		}

		IDataStockMetaData[] stocksToCheck = stock != null ? new IDataStockMetaData[] { stock } : this.getAvailableDataStocks();

		DataSetDao<T, ?, ?> daoObject = this.getFreshDaoInstance();

		final CategoryList catList = new CategoryList();

		List<ClClass> cats = daoObject.getTopClasses( catSystem, stocksToCheck );

		if ( sort != null && sort.equalsIgnoreCase( SORT_ID ) ) {
			Collections.sort( cats, new ClClassIdComparator() );
		}

		for ( ClClass clazz : cats ) {
			catList.addCategory( new ClClassAdapter( clazz ) );
		}

		if ( ConfigurationService.INSTANCE.isTranslateClassification() && !StringUtils.equalsIgnoreCase( language, ConfigurationService.INSTANCE
				.getDefaultLanguage() ) ) {
			CategoryTranslator t = new CategoryTranslator( this.modelType, catSystem );
			for ( ClassType cat : catList.getCategories() ) {
				cat.setValue( t.translateTo( cat.getClassId(), language ) );
			}
		}

		return Response.ok( this.getCategoriesStreamingOutput( catList ) ).build();
	}

	/**
	 * Get data sets in category or sub-categories
	 * 
	 * @param stockName
	 *            name of data stock
	 * @param category
	 *            list of categories
	 * @param catSystem
	 *            category system
	 * @param sort
	 *            sort field
	 * @param startIndex
	 *            start index (only valid for data set query)
	 * @param pageSize
	 *            page size (only valid for data set query)
	 * @param format
	 *            format for output
	 * @param callback
	 *            JSONP callback
	 * @return data sets in category or sub-categories
	 */
	@GET
	@Path( "categories/{category:.+}" )
	@Produces( "application/xml" )
	public Response getDatasetsInCategoryOrSubcategories( @PathParam( "stockIdentifier" ) String stockIdentifier,
			@PathParam( "category" ) @Encoded String category, @QueryParam( "catSystem" ) String catSystem, @QueryParam( "sort" ) final String sort,
			@DefaultValue( "0" ) @QueryParam( "startIndex" ) final int startIndex, @DefaultValue( "500" ) @QueryParam( "pageSize" ) final int pageSize,
			@QueryParam( AbstractDataSetResource.PARAM_FORMAT ) String format, @DefaultValue( "fn" ) @QueryParam( "callback" ) String callback,
			@QueryParam( "lang" ) String language, @DefaultValue( "false" ) @QueryParam( "allVersions" ) final boolean allVersions) {

		if ( catSystem == null )
			catSystem = ConfigurationService.INSTANCE.getDefaultClassificationSystem();

		if ( language == null )
			language = ConfigurationService.INSTANCE.getDefaultLanguage();

		IDataStockMetaData stock = null;
		if ( stockIdentifier != null ) {
			stock = this.getStockByIdentifier( stockIdentifier );
			if ( stock == null ) {
				return Response.status( Response.Status.FORBIDDEN ).type( "text/plain" ).build();
			}
		}

		IDataStockMetaData[] stocksToCheck = stock != null ? new IDataStockMetaData[] { stock } : this.getAvailableDataStocks();

		StringTokenizer tokenizer = new StringTokenizer( category, "/" );

		List<String> categories = new ArrayList<String>();

		while ( tokenizer.hasMoreTokens() ) {
			categories.add( SodaUtil.decode( tokenizer.nextToken() ) );
		}

		if ( categories.get( categories.size() - 1 ).equals( "subcategories" ) ) {
			categories.remove( categories.size() - 1 );
			return Response.ok( this.getSubCategories( categories, catSystem, (!allVersions), sort, language, stocksToCheck ), MediaType.APPLICATION_XML_TYPE )
					.build();
		}
		else {
			return this.getDatasetsInSubcategory( startIndex, pageSize, categories, catSystem, format, callback, language, !allVersions, stocksToCheck );
		}
	}

	/**
	 * Get the sub categories
	 * 
	 * @param categories
	 *            categories path
	 * @param catSystem
	 *            category system name
	 * @param mostRecentVersionOnly
	 * @return sub categories
	 */
	private StreamingOutput getSubCategories( List<String> categories, String catSystem, boolean mostRecentVersionOnly, String sort, String language,
			IDataStockMetaData... stocks ) {
		DataSetDao<T, ?, ?> daoObject = this.getFreshDaoInstance();

		stocks = stocks != null ? stocks : this.getAvailableDataStocks();

		// translate incoming
		categories = translateCategories( categories, catSystem, language );

		final CategoryList cats = new CategoryList();
		List<ClClass> classes = daoObject.getSubClasses( catSystem, categories, mostRecentVersionOnly, stocks );

		if ( sort != null && sort.equalsIgnoreCase( SORT_ID ) ) {
			Collections.sort( classes, new ClClassIdComparator() );
		}

		for ( ClClass clazz : classes ) {
			cats.addCategory( new ClClassAdapter( clazz ) );
		}

		// translate outgoing
		if ( ConfigurationService.INSTANCE.isTranslateClassification() && !StringUtils.equalsIgnoreCase( language, ConfigurationService.INSTANCE
				.getDefaultLanguage() ) ) {
			CategoryTranslator t = new CategoryTranslator( this.modelType, catSystem );
			for ( ClassType cat : cats.getCategories() ) {
				cat.setValue( t.translateTo( cat.getClassId(), language ) );
			}
		}

		return this.getCategoriesStreamingOutput( cats );
	}

	private List<String> translateCategories( List<String> categories, String catSystem, String fromLanguage ) {
		if ( ConfigurationService.INSTANCE.isTranslateClassification() && !StringUtils.equalsIgnoreCase( fromLanguage, ConfigurationService.INSTANCE
				.getDefaultLanguage() ) ) {
			CategoryTranslator t = new CategoryTranslator( this.modelType, catSystem );
			List<String> translatedCategories = new ArrayList<String>();
			for ( String cat : categories ) {
				translatedCategories.add( t.translateFrom( cat, fromLanguage ) );
			}
			categories = translatedCategories;
		}
		return categories;
	}

	/**
	 * Get all data sets in specified sub category path
	 * 
	 * @param startIndex
	 *            start index
	 * @param pageSize
	 *            page size
	 * @param categories
	 *            categories path
	 * @param catSystem
	 *            name of category system
	 * @param language
	 *            the language to filter for
	 * @param allVersions
	 * @return all data sets in specified sub category path
	 * @see #getDatasetsInCategoryOrSubcateogories(String, String, int, int)
	 */
	private Response getDatasetsInSubcategory( int startIndex, int pageSize, List<String> categories, String catSystem, String format, String callback,
			String language, boolean mostRecentVersionOnly, IDataStockMetaData... stocks ) {
		stocks = stocks != null ? stocks : this.getAvailableDataStocks();

		DataSetDao<T, ?, ?> daoObject = this.getFreshDaoInstance();
		List<T> dataSets = new ArrayList<T>();

		// translate incoming
		categories = translateCategories( categories, catSystem, language );

		int count = (int) daoObject.getNumberByClass( catSystem, categories, stocks, language, mostRecentVersionOnly );
		dataSets = daoObject.getByClass( catSystem, categories, stocks, language, mostRecentVersionOnly, startIndex, pageSize );

		return this.getResponseForDatasetListVOList( dataSets, count, startIndex, pageSize, format, callback, language );
	}

	/**
	 * Get streaming output for category list
	 * 
	 * @param cats
	 *            category list
	 * @return streaming output for category list
	 */
	private StreamingOutput getCategoriesStreamingOutput( final CategoryList cats ) {
		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO dao = new ServiceDAO();
				try {
					dao.marshal( cats, out );
				}
				catch ( JAXBException e ) {

					LOGGER.error( "error marshalling data", e );
				}
			}
		};
	}

	/**
	 * Get current request
	 * 
	 * @return current request
	 */
	protected HttpServletRequest getRequest() {
		return this.request;
	}

	/**
	 * Get all available data stocks
	 * 
	 * @return available data stocks
	 */
	protected IDataStockMetaData[] getAvailableDataStocks() {
		AvailableStockHandler availableStocksHandler = new AvailableStockHandler();
		availableStocksHandler.setDirty( new DirtyFlagBean() );

		return availableStocksHandler.getAllStocksMeta().toArray( new IDataStockMetaData[0] );
	}

	/**
	 * Get data stock by identifier (name or UUID)
	 * 
	 * @param stockIdentifier
	 *            identifier
	 * 
	 * @return available data stock or <code>null</code>
	 */
	protected IDataStockMetaData getStockByIdentifier( String stockIdentifier ) {
		AvailableStockHandler availableStocksHandler = new AvailableStockHandler();
		availableStocksHandler.setDirty( new DirtyFlagBean() );

		return availableStocksHandler.getStockByIdentifier( stockIdentifier );
	}

	/**
	 * Get the context / URI information
	 * 
	 * @return context / URI information
	 */
	protected UriInfo getContext() {
		return this.context;
	}

	/**
	 * Import data set from input stream
	 * 
	 * @param desc
	 *            description (mainly for logging)
	 * @param type
	 *            type of data set
	 * @param inStream
	 *            the input stream
	 * @param rds
	 *            the root data stock to import to
	 * @param digitFileProvider
	 *            digital file provider, may be <code>null</code> for all non-{@link Source} data sets
	 * @return created data set
	 * @throws IOException
	 *             on I/O errors
	 */
	@SuppressWarnings( "unchecked" )
	protected T importByInputStream( String desc, DataSetType type, InputStream inStream, RootDataStock rds, AbstractDigitalFileProvider digitFileProvider )
			throws IOException {

		DataSetImporter importer = new DataSetImporter();
		StringWriter writer = new StringWriter();
		PrintWriter out = new PrintWriter( writer );

		try {
			T foo = (T) importer.importDataSet( type, desc, inStream, out, rds, digitFileProvider );
			if ( foo != null ) {
				LOGGER.info( "data set successfully imported." );
				LOGGER.info( "{}", writer.getBuffer() );
				out.println( "data set successfully imported." );
				out.flush();
				return foo;
			}
			else {
				LOGGER.error( "Cannot import data set" );
				LOGGER.error( "output is: {}", writer.getBuffer() );
				throw new IllegalStateException( writer.getBuffer().toString() );
			}
		}
		catch ( Exception e ) {
			LOGGER.error( "cannot import data set" );
			LOGGER.error( "exception is: ", e );
			throw new IllegalArgumentException( writer.getBuffer().toString() );
		}
	}
}