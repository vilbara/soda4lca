package de.iai.ilcd.services.primeui;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.fzk.iai.ilcd.api.binding.generated.categories.DataSetType;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.fzk.iai.ilcd.service.model.IFlowListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodListVO;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.ISourceListVO;
import de.fzk.iai.ilcd.service.model.ISourceVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.PublicationTypeValue;
import de.fzk.iai.ilcd.service.model.flowproperty.IUnitGroupType;
import de.iai.ilcd.model.flow.ElementaryFlow;
import de.iai.ilcd.model.flow.Flow;
import de.iai.ilcd.model.flow.FlowPropertyDescription;
import de.iai.ilcd.model.unitgroup.Unit;
import de.iai.ilcd.model.unitgroup.UnitGroup;

/**
 * <p>
 * Serializer for PrimeUI lists. Register in a {@link GsonBuilder} via {@link #register(GsonBuilder)}.
 * </p>
 * <p>
 * The generated JSON has the fields: <code>startIndex, pageSize, totalCount, data</code>, where <code>data</code> is an
 * array of data sets (ready for PrimeUI data table).
 * </p>
 * <p>
 * JSON fields set for the various data set types are:
 * <ul>
 * <li>Process: <code>dsType, name, classific, geo, refYear, validUntil</code></li>
 * <li>Flow: <code>dsType, name, classific, refProp, refPropUnit</code></li>
 * <li>Flow Property: <code>dsType, name, classific, defUnitGrp, defUnit</code></li>
 * <li>LCIA method: <code>dsType, name, type, refYear, dur</code></li>
 * <li>Unit group: <code>dsType, name, classific, defUnit</code></li>
 * <li>Source: <code>dsType, name, classific, pubType</code></li>
 * <li>Contact: <code>dsType, name, classific, email, www</code></li>
 * </ul>
 * </p>
 * <p>
 * Example for a Flow:
 * <pre>
 * { "startIndex":0,
 *   "pageSize":1,
 *   "totalCount":210,
 *   "data":[
 *   	{"name":"Demo Name 1","type":"Product flow","classific":"My / Classification / Path","refProp":"Mass","refPropUnit":"kg","dsType":"Flow"},
 *      {"name":"Demo Name 2","type":"Product flow","classific":"My / Classification / Path","refProp":"Mass","refPropUnit":"kg","dsType":"Flow"}
 *   ]
 * }
 * </pre>
 * <p>
 */
public class PUIDatasetListSerializer implements JsonSerializer<PUIDatasetList> {

	/**
	 * Private constructor, use {@link #register(GsonBuilder)}
	 */
	private PUIDatasetListSerializer() {
	}

	/**
	 * Register serializer in provided builder. Serialization of a list
	 * must be performed by passing an instance of the wrapper class {@link PUIDatasetList} to {@link Gson}.
	 * 
	 * @param gsonBuilder
	 *            GSON builder
	 */
	public static void register( GsonBuilder gsonBuilder ) {
		gsonBuilder.registerTypeAdapter( PUIDatasetList.class, new PUIDatasetListSerializer() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JsonElement serialize( PUIDatasetList src, Type type, JsonSerializationContext context ) {
		JsonObject list = new JsonObject();

		list.addProperty( "startIndex", src.getStartIndex() );
		list.addProperty( "pageSize", src.getPageSize() );
		list.addProperty( "totalCount", src.getTotalCount() );

		JsonArray data = new JsonArray();
		for ( IDataSetListVO listvo : src.getDataSets() ) {
			JsonObject obj = new JsonObject();
			DataSetType dsType;
			if ( listvo instanceof IProcessListVO ) {
				dsType = this.serialize( obj, (IProcessListVO) listvo );
			}
			else if ( listvo instanceof IFlowListVO ) {
				dsType = this.serialize( obj, (IFlowListVO) listvo );
			}
			else if ( listvo instanceof IFlowPropertyListVO ) {
				dsType = this.serialize( obj, (IFlowPropertyListVO) listvo );
			}
			else if ( listvo instanceof ILCIAMethodListVO ) {
				dsType = this.serialize( obj, (ILCIAMethodListVO) listvo );
			}
			else if ( listvo instanceof IUnitGroupListVO ) {
				dsType = this.serialize( obj, (IUnitGroupListVO) listvo );
			}
			else if ( listvo instanceof ISourceListVO ) {
				dsType = this.serialize( obj, (ISourceListVO) listvo );
			}
			else if ( listvo instanceof IContactListVO ) {
				dsType = this.serialize( obj, (IContactListVO) listvo );
			}
			else {
				// prevent adding of empty object
				continue;
			}
			obj.addProperty( "dsType", dsType.getValue() );
			data.add( obj );
		}

		list.add( "data", data );

		return list;
	}

	/**
	 * Serialize a process list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, IProcessListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "geo", subject.getLocation() );
		obj.addProperty( "classific", subject.getClassification().getClassHierarchyAsString() );
		de.fzk.iai.ilcd.service.model.process.ITimeInformation ti = subject.getTimeInformation();
		if ( ti != null ) {
			obj.addProperty( "refYear", ti.getReferenceYear() );
			obj.addProperty( "validUntil", ti.getValidUntil() );
		}
		return DataSetType.PROCESS;
	}

	/**
	 * Serialize a flow list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, IFlowListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "type", subject.getType().getValue() );

		if ( subject instanceof Flow ) {
			final String classific;
			if ( subject instanceof ElementaryFlow ) {
				classific = ((ElementaryFlow) subject).getCategorization().getClassHierarchyAsString();
			}
			else {
				classific = subject.getClassification().getClassHierarchyAsString();
			}
			obj.addProperty( "classific", classific );

			FlowPropertyDescription fp = ((Flow) subject).getReferencePropertyDescription();
			if ( fp != null ) {
				this.addProperty( obj, "refProp", fp.getName() );
				obj.addProperty( "refPropUnit", fp.getFlowPropertyUnit() );
			}
		}
		return DataSetType.FLOW;
	}

	/**
	 * Serialize a flow property list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, IFlowPropertyListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "classific", subject.getClassification().getClassHierarchyAsString() );
		IUnitGroupType ugDetails = subject.getUnitGroupDetails();
		if ( ugDetails != null ) {
			this.addProperty( obj, "defUnitGrp", ugDetails.getName() );
			obj.addProperty( "defUnit", ugDetails.getDefaultUnit() );
		}
		return DataSetType.FLOW_PROPERTY;
	}

	/**
	 * Serialize a LCIA method list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, ILCIAMethodListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "type", subject.getType().getValue() );
		de.fzk.iai.ilcd.service.model.lciamethod.ITimeInformation ti = subject.getTimeInformation();
		if ( ti != null ) {
			obj.addProperty( "refYear", ti.getReferenceYear().getValue() );
			obj.addProperty( "dur", ti.getDuration().getValue() );
		}
		return DataSetType.LCIA_METHOD;
	}

	/**
	 * Serialize an unit group list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, IUnitGroupListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "classific", subject.getClassification().getClassHierarchyAsString() );
		if ( subject instanceof UnitGroup ) {
			Unit refUnit = ((UnitGroup) subject).getReferenceUnit();
			if ( refUnit != null ) {
				obj.addProperty( "defUnit", refUnit.getName() );
			}
		}
		return DataSetType.UNIT_GROUP;
	}

	/**
	 * Serialize a source list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, ISourceListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "classific", subject.getClassification().getClassHierarchyAsString() );
		if ( subject instanceof ISourceVO ) {
			PublicationTypeValue pType = ((ISourceVO) subject).getPublicationType();
			if ( pType != null ) {
				obj.addProperty( "pubType", pType.getValue() );
			}
		}
		return DataSetType.SOURCE;
	}

	/**
	 * Serialize a contact list value object to JSON
	 * 
	 * @param obj
	 *            object to add properties
	 * @param subject
	 *            source object
	 * @return subject data set type
	 */
	private DataSetType serialize( JsonObject obj, IContactListVO subject ) {
		this.addProperty( obj, "name", subject.getName() );
		obj.addProperty( "classific", subject.getClassification().getClassHierarchyAsString() );
		obj.addProperty( "email", subject.getEmail() );
		obj.addProperty( "www", subject.getWww() );
		return DataSetType.CONTACT;
	}

	/**
	 * Add multilang string property
	 * 
	 * @param obj
	 *            object to add to
	 * @param name
	 *            name of property
	 * @param mlStr
	 *            multilang string instance
	 */
	private void addProperty( JsonObject obj, String name, IMultiLangString mlStr ) {
		if ( mlStr != null ) {
			obj.addProperty( name, mlStr.getValue() );
		}
	}
}
