package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of flow property resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/flowproperties" )
public class FlowPropertyStockAwareResource extends FlowPropertyResource {

}
