package de.iai.ilcd.services;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.api.binding.generated.common.ExchangeDirectionValues;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.client.impl.ServiceDAO;
import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.IntegerList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.fzk.iai.ilcd.service.model.IFlowListVO;
import de.fzk.iai.ilcd.service.model.common.ILString;
import de.fzk.iai.ilcd.service.model.enums.TypeOfFlowValue;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.adapter.DataSetListAdapter;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.ExchangeDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.process.Exchange;
import de.iai.ilcd.model.process.Process;

/**
 * REST web service for Processes
 */
@Component
@Path( "processes" )
public class ProcessResource extends AbstractDataSetResource<Process> {

	private static final Logger LOGGER = LoggerFactory.getLogger( ProcessResource.class );

	public ProcessResource() {
		super( DataSetType.PROCESS, ILCDTypes.PROCESS );
	}

	/**
	 * Get exchanges for process
	 * 
	 * @param uuid
	 *            the UUID of the process
	 * @param direction
	 *            the direction, mapping is: &quot;in&quot; &rArr; {@link ExchangeDirection#INPUT}, &quot;out&quot;
	 *            &rArr; {@link ExchangeDirection#OUTPUT}. <code>null</code> is permitted (both directions matched)
	 * @param type
	 *            the type of the flow, mapped via {@link TypeOfFlowValue#valueOf(String)}. <code>null</code> is
	 *            permitted (all types matched)
	 * @return the list of the matched exchanges
	 */
	@GET
	@Path( "{uuid}/exchanges" )
	@Produces( "application/xml" )
	public StreamingOutput getExchanges( @PathParam( "uuid" ) String uuid, @QueryParam( "direction" ) final String direction,
			@QueryParam( "type" ) final String type, @QueryParam( "lang" ) String language ) {

		ExchangeDirectionValues eDir = null;
		if ( "in".equals( direction ) ) {
			eDir = ExchangeDirectionValues.INPUT;
		}
		else if ( "out".equals( direction ) ) {
			eDir = ExchangeDirectionValues.OUTPUT;
		}

		TypeOfFlowValue fType = null;
		try {
			if ( type != null ) {
				fType = TypeOfFlowValue.valueOf( type );
			}
		}
		catch ( Exception e ) {
			// Nothing to do, null is already set before try
		}

		ExchangeDao eDao = new ExchangeDao();
		List<Exchange> exchanges = eDao.getExchangesForProcess( uuid, null, eDir, fType );
		List<IDataSetListVO> dataSets = new ArrayList<IDataSetListVO>();
		final String baseFlowUrl = ConfigurationService.INSTANCE.getNodeInfo().getBaseURL() + DatasetTypes.FLOWS.getValue() + "/";
		for ( Exchange e : exchanges ) {
			IFlowListVO f = e.getFlow();
			if ( f == null ) {
				final GlobalReference ref = e.getFlowReference();
				f = new FlowDataSetVO();
				for ( ILString lStr : ref.getShortDescription().getLStrings() ) {
					f.getName().setValue( lStr.getLang(), lStr.getValue() );
				}
				f.setHref( ref.getHref() );
			}
			else {
				f.setHref( baseFlowUrl + f.getUuidAsString() );
			}
			dataSets.add( f );
		}

		final DataSetList dsList = new DataSetListAdapter( dataSets, language );

		dsList.setTotalSize( dataSets.size() );
		dsList.setPageSize( dataSets.size() );
		dsList.setStartIndex( 0 );

		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				DatasetVODAO dao = new DatasetVODAO();
				try {
					dao.marshal( dsList, out );
				}
				catch ( JAXBException e ) {
					if ( e.getCause().getCause() instanceof SocketException ) {
						LOGGER.warn( "exception occurred during marshalling - " + e );
					}
					else {
						LOGGER.error( "error marshalling data", e );
					}
				}
			}
		};
	}

	/**
	 * Get the reference years
	 * 
	 * @return reference years
	 */
	@GET
	@Path( "referenceyears" )
	@Produces( "application/xml" )
	public StreamingOutput getReferenceYears() {
		ProcessDao pDao = new ProcessDao();

		final IntegerList il = new IntegerList();
		il.getIntegers().addAll( pDao.getReferenceYears() );

		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO dao = new ServiceDAO();
				try {
					dao.marshal( il, out );
				}
				catch ( JAXBException e ) {
					if ( e.getCause().getCause() instanceof SocketException ) {
						LOGGER.warn( "exception occurred during marshalling - " + e );
					}
					else {
						LOGGER.error( "error marshalling data", e );
					}
				}
			}
		};
	}

	/**
	 * Get the validUntil years
	 * 
	 * @return validUntil years
	 */
	@GET
	@Path( "validuntilyears" )
	@Produces( "application/xml" )
	public StreamingOutput getValidUntilYears() {
		ProcessDao pDao = new ProcessDao();

		final IntegerList il = new IntegerList();
		il.getIntegers().addAll( pDao.getValidUntilYears() );

		return new StreamingOutput() {

			@Override
			public void write( OutputStream out ) throws IOException, WebApplicationException {
				ServiceDAO dao = new ServiceDAO();
				try {
					dao.marshal( il, out );
				}
				catch ( JAXBException e ) {
					if ( e.getCause().getCause() instanceof SocketException ) {
						LOGGER.warn( "exception occurred during marshalling - " + e );
					}
					else {
						LOGGER.error( "error marshalling data", e );
					}
				}
			}
		};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataSetDao<Process, ?, ?> getFreshDaoInstance() {
		return new ProcessDao();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getXMLTemplatePath() {
		return "/xml/process.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getHTMLDatasetDetailTemplatePath() {
		return "/html/process.vm";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDataSetTypeName() {
		return "process";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean userRequiresDatasetDetailRights() {
		return true;
	}

}
