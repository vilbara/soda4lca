package de.iai.ilcd.services;

import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

/**
 * Stock aware version of flow resource
 */
@Component
@Path( "datastocks/{stockIdentifier}/flows" )
public class FlowStockAwareResource extends FlowResource {

}
