package de.iai.ilcd.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 * 
 * @author clemens.duepmeier
 */
public class UserLoginActions {

	public void login( String userName, String password ) {
		Subject currentUser = SecurityUtils.getSubject();
		// if (currentUser.isAuthenticated())
		// return;
		UsernamePasswordToken token = new UsernamePasswordToken( userName, password );
		currentUser.login( token );
	}

	public void logout() {
		Subject currentUser = SecurityUtils.getSubject();
		// IlcdSecurityRealm realm= new IlcdSecurityRealm();
		// realm.clearAuthorizationInfo(currentUser.getPrincipals());
		if ( currentUser.isAuthenticated() ) {
			currentUser.logout();
		}
	}

	public boolean isLoggedIn() {
		Subject currentUser = SecurityUtils.getSubject();
		if ( currentUser.isAuthenticated() ) {
			return true;
		}
		else {
			return false;
		}
	}

}
