package de.iai.ilcd.util;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.dao.ClassificationDao;

public class CategoryTranslator {

	private ClassLoader loader = null;

	private String bundleBaseName = null;

	private DataSetType datasetType = null;

	ClassificationDao cDao = new ClassificationDao();

	private final Logger logger = LoggerFactory.getLogger( CategoryTranslator.class );

	public CategoryTranslator( DataSetType datasetType, String catSystem ) {
		if ( logger.isDebugEnabled() ) {
			logger.debug( "instantiating CategoryTranslator for dataset type " + datasetType + " and " + catSystem );
			logger.debug( "reading translations from " + ConfigurationService.INSTANCE.getTranslateClassifications().get( catSystem.toUpperCase() ) );
		}
		this.datasetType = datasetType;
		try {
			this.bundleBaseName = StringUtils.replaceEach( catSystem, new String[] { ".", "-" }, new String[] { "", "" } );
			this.bundleBaseName = this.bundleBaseName.toUpperCase();
			File file = new File( ConfigurationService.INSTANCE.getTranslateClassifications().get( catSystem.toUpperCase() ) );
			URL[] urls = { file.toURI().toURL() };
			this.loader = new URLClassLoader( urls );
		}
		catch ( Exception e ) {
			logger.error( "error resolving resource bundle", e );
			if ( logger.isDebugEnabled() )
				e.printStackTrace();
		}
	}

	public String translateTo( String key, String toLanguage ) {
		ResourceBundle bundle;
		try {
			bundle = ResourceBundle.getBundle( bundleBaseName, new Locale( toLanguage ), loader );
		}
		catch ( Exception e ) {
			logger.error( "resource bundle '" + bundleBaseName + "' cannot be found" );
			return key;
		}
		return bundle.getString( key );
	}

	public String translateFrom( String value, String fromLanguage ) {
		ResourceBundle bundle;
		try {
			bundle = ResourceBundle.getBundle( bundleBaseName, new Locale( fromLanguage ), loader );
		}
		catch ( Exception e ) {
			logger.error( "resource bundle '" + bundleBaseName + "' cannot be found" );
			return value;
		}

		for ( String key : bundle.keySet() ) {
			String keyValue = bundle.getString( key );
			if ( value.startsWith( keyValue ) ) {
				return cDao.getNameByClId( datasetType, key );
			}
		}
		return null;
	}
}
