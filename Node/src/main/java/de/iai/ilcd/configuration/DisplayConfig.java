package de.iai.ilcd.configuration;

public class DisplayConfig {

	/**
	 * Flag to indicate if the additional link for alternative rendering method of dataset detail should be shown in
	 * data set
	 * overview page
	 */
	private boolean showAlternativeHtmlLink = false;

	/**
	 * Flag to indicate if the back links back links on dataset detail view page should be shown
	 */
	private boolean showBackLinks = false;

	/**
	 * Flag to indicate if the login link should be shown
	 */
	private boolean showLoginLink = false;

	/**
	 * Flag to indicate if the node id should be shown as link
	 * search results
	 */
	private boolean showNodeIdAsLink = false;

	/* Flags for identification of visibility of the columns in the public tables with contacts */
	private boolean showContactCategory = true;

	private boolean showContactEmail = true;

	private boolean showContactHomePage = true;

	private boolean showContactName = true;

	/* Flags for identification of visibility of the columns in the public tables with elementary flows */
	private boolean showElementaryFlowCategory = true;

	private boolean showElementaryFlowName = true;

	private boolean showElementaryFlowRefProperty = true;

	private boolean showElementaryFlowRefPropertyUnit = true;

	/* Flags for identification of visibility of the columns in the public tables with flow properties */
	private boolean showFlowPropertyCategory = true;

	private boolean showFlowPropertyDefaultUnit = true;

	private boolean showFlowPropertyDefaultUnitGroup = true;

	private boolean showFlowPropertyName = true;

	/* Flags for identification of visibility of the columns in the public tables with LCIA methods */
	private boolean showMethodDuration = true;

	private boolean showMethodName = true;

	private boolean showMethodReferenceYear = true;

	private boolean showMethodType = true;

	/* Flags for identification of visibility of the columns in the public tables with processes */
	private boolean showProcessClassification = true;

	private boolean showProcessLocation = true;

	private boolean showProcessType = true;

	private boolean showProcessSubType = false;

	private boolean showProcessName = true;

	private boolean showProcessNodeId = true;
	
	private boolean showProcessReferenceYear = true;
	
	private boolean showProcessValidUntil = true;

	private boolean showManageProcessType = true;

	private boolean showManageProcessSubType = false;

	private boolean showManageProcessLocation = true;

	private boolean showManageProcessReferenceYear = true;

	private boolean showManageProcessValidUntil = true;

	/* Flags for identification of visibility of the columns in the public tables with product flows */
	private boolean showProductFlowCategory = true;

	private boolean showProductFlowName = true;

	private boolean showProductFlowRefProperty = true;

	private boolean showProductFlowRefPropertyUnit = true;

	private boolean showProductFlowType = true;

	/* Flags for identification of visibility of the columns in the public tables with sources */
	private boolean showSourceCategory = true;

	private boolean showSourceName = true;

	private boolean showSourceType = true;

	/* Flags for identification of visibility of the columns in the public tables with unit groups */
	private boolean showUnitGroupCategory = true;

	private boolean showUnitGroupDefaultUnit = true;

	private boolean showUnitGroupName = true;

	private boolean showManageDatasetImportDate = true;

	private boolean showManageDatasetRootStock = true;

	private boolean showManageDatasetContainedIn = true;

	private boolean showManageDatasetVersion = true;

	private boolean showManageDatasetMostRecentVersion = true;

	private boolean showManageDatasetClassification = true;

	private boolean showManageProcessClassificationId = false;

	private boolean showDatasetDetailProcessType = true;

	public boolean isShowDatasetDetailProcessType() {
		return showDatasetDetailProcessType;
	}

	public void setShowDatasetDetailProcessType( boolean showDatasetDetailProcessType ) {
		this.showDatasetDetailProcessType = showDatasetDetailProcessType;
	}

	/**
	 * Indicates if the additional link for alternative rendering method of dataset detail should be shown in data set
	 * overview page
	 * 
	 * @return <code>true</code> if the additional link for alternative rendering method of dataset detail should be
	 *         shown in
	 *         data set overview page, else <code>false</code>
	 */
	public boolean isShowAlternativeHtmlLink() {
		return this.showAlternativeHtmlLink;
	}

	/**
	 * Indicates if the back links on dataset detail view page should be shown
	 * 
	 * @return <code>true</code> if the back links on dataset detail view page should be shown, else <code>false</code>
	 */
	public boolean isShowBackLinks() {
		return this.showBackLinks;
	}

	public boolean isShowContactCategory() {
		return showContactCategory;
	}

	public boolean isShowContactEmail() {
		return showContactEmail;
	}

	public boolean isShowContactHomePage() {
		return showContactHomePage;
	}

	public boolean isShowContactName() {
		return showContactName;
	}

	public boolean isShowElementaryFlowCategory() {
		return showElementaryFlowCategory;
	}

	public boolean isShowElementaryFlowName() {
		return showElementaryFlowName;
	}

	public boolean isShowElementaryFlowRefProperty() {
		return showElementaryFlowRefProperty;
	}

	public boolean isShowElementaryFlowRefPropertyUnit() {
		return showElementaryFlowRefPropertyUnit;
	}

	public boolean isShowFlowPropertyCategory() {
		return showFlowPropertyCategory;
	}

	public boolean isShowFlowPropertyDefaultUnit() {
		return showFlowPropertyDefaultUnit;
	}

	public boolean isShowFlowPropertyDefaultUnitGroup() {
		return showFlowPropertyDefaultUnitGroup;
	}

	public boolean isShowFlowPropertyName() {
		return showFlowPropertyName;
	}

	public boolean isShowLoginLink() {
		return showLoginLink;
	}

	public boolean isShowMethodDuration() {
		return showMethodDuration;
	}

	public boolean isShowMethodName() {
		return showMethodName;
	}

	public boolean isShowMethodReferenceYear() {
		return showMethodReferenceYear;
	}

	public boolean isShowMethodType() {
		return showMethodType;
	}

	public boolean isShowNodeIdAsLink() {
		return showNodeIdAsLink;
	}

	public boolean isShowProcessClassification() {
		return showProcessClassification;
	}

	public boolean isShowProcessLocation() {
		return showProcessLocation;
	}

	public boolean isShowProcessName() {
		return showProcessName;
	}

	public boolean isShowProcessNodeId() {
		return showProcessNodeId;
	}

	public boolean isShowProcessReferenceYear() {
		return showProcessReferenceYear;
	}

	public boolean isShowProcessValidUntil() {
		return showProcessValidUntil;
	}

	public boolean isShowProductFlowCategory() {
		return showProductFlowCategory;
	}

	public boolean isShowProductFlowName() {
		return showProductFlowName;
	}

	public boolean isShowProductFlowRefProperty() {
		return showProductFlowRefProperty;
	}

	public boolean isShowProductFlowRefPropertyUnit() {
		return showProductFlowRefPropertyUnit;
	}

	public boolean isShowProductFlowType() {
		return showProductFlowType;
	}

	public boolean isShowSourceCategory() {
		return showSourceCategory;
	}

	public boolean isShowSourceName() {
		return showSourceName;
	}

	public boolean isShowSourceType() {
		return showSourceType;
	}

	public boolean isShowUnitGroupCategory() {
		return showUnitGroupCategory;
	}

	public boolean isShowUnitGroupDefaultUnit() {
		return showUnitGroupDefaultUnit;
	}

	public boolean isShowUnitGroupName() {
		return showUnitGroupName;
	}

	/**
	 * Set if the additional link for alternative rendering method of dataset detail should be shown in data set
	 * overview page
	 * 
	 * @param showAlternativeHtmlLink
	 *            the additional link for alternative rendering method of dataset detail to be set
	 */
	public void setShowAlternativeHtmlLink( boolean showAlternativeHtmlLink ) {
		this.showAlternativeHtmlLink = showAlternativeHtmlLink;
	}

	/**
	 * Set if the back links on dataset detail view page should be shown
	 * 
	 * @param showBackLinks
	 *            the back links on dataset detail view page should be shown to be set
	 */
	public void setShowBackLinks( boolean showBackLinks ) {
		this.showBackLinks = showBackLinks;
	}

	public void setShowContactCategory( boolean showContactCategory ) {
		this.showContactCategory = showContactCategory;
	}

	public void setShowContactEmail( boolean showContactEmail ) {
		this.showContactEmail = showContactEmail;
	}

	public void setShowContactHomePage( boolean showContactHomePage ) {
		this.showContactHomePage = showContactHomePage;
	}

	public void setShowContactName( boolean showContactName ) {
		this.showContactName = showContactName;
	}

	public void setShowElementaryFlowCategory( boolean showElementaryFlowCategory ) {
		this.showElementaryFlowCategory = showElementaryFlowCategory;
	}

	public void setShowElementaryFlowName( boolean showElementaryFlowName ) {
		this.showElementaryFlowName = showElementaryFlowName;
	}

	public void setShowElementaryFlowRefProperty( boolean showElementaryFlowRefProperty ) {
		this.showElementaryFlowRefProperty = showElementaryFlowRefProperty;
	}

	public void setShowElementaryFlowRefPropertyUnit( boolean showElementaryFlowRefPropertyUnit ) {
		this.showElementaryFlowRefPropertyUnit = showElementaryFlowRefPropertyUnit;
	}

	public void setShowFlowPropertyCategory( boolean showFlowPropertyCategory ) {
		this.showFlowPropertyCategory = showFlowPropertyCategory;
	}

	public void setShowFlowPropertyDefaultUnit( boolean showFlowPropertyDefaultUnit ) {
		this.showFlowPropertyDefaultUnit = showFlowPropertyDefaultUnit;
	}

	public void setShowFlowPropertyDefaultUnitGroup( boolean showFlowPropertyDefaultUnitGroup ) {
		this.showFlowPropertyDefaultUnitGroup = showFlowPropertyDefaultUnitGroup;
	}

	public void setShowFlowPropertyName( boolean showFlowPropertyName ) {
		this.showFlowPropertyName = showFlowPropertyName;
	}

	public void setShowLoginLink( boolean showLoginLink ) {
		this.showLoginLink = showLoginLink;
	}

	public void setShowMethodDuration( boolean showMethodDuration ) {
		this.showMethodDuration = showMethodDuration;
	}

	public void setShowMethodName( boolean showMethodName ) {
		this.showMethodName = showMethodName;
	}

	public void setShowMethodReferenceYear( boolean showMethodReferenceYear ) {
		this.showMethodReferenceYear = showMethodReferenceYear;
	}

	public void setShowMethodType( boolean showMethodType ) {
		this.showMethodType = showMethodType;
	}

	public void setShowNodeIdAsLink( boolean showNodeIdAsLink ) {
		this.showNodeIdAsLink = showNodeIdAsLink;
	}

	public void setShowProcessClassification( boolean showProcessClassification ) {
		this.showProcessClassification = showProcessClassification;
	}

	public void setShowProcessLocation( boolean showProcessLocation ) {
		this.showProcessLocation = showProcessLocation;
	}

	public void setShowProcessName( boolean showProcessName ) {
		this.showProcessName = showProcessName;
	}

	public void setShowProcessNodeId( boolean showProcessNodeId ) {
		this.showProcessNodeId = showProcessNodeId;
	}

	public void setShowProcessReferenceYear( boolean showProcessReferenceYear ) {
		this.showProcessReferenceYear = showProcessReferenceYear;
	}

	public void setShowProcessValidUntil( boolean showProcessValidUntil ) {
		this.showProcessValidUntil = showProcessValidUntil;
	}

	public void setShowProductFlowCategory( boolean showProductFlowCategory ) {
		this.showProductFlowCategory = showProductFlowCategory;
	}

	public void setShowProductFlowName( boolean showProductFlowName ) {
		this.showProductFlowName = showProductFlowName;
	}

	public void setShowProductFlowRefProperty( boolean showProductFlowRefProperty ) {
		this.showProductFlowRefProperty = showProductFlowRefProperty;
	}

	public void setShowProductFlowRefPropertyUnit( boolean showProductFlowRefPropertyUnit ) {
		this.showProductFlowRefPropertyUnit = showProductFlowRefPropertyUnit;
	}

	public void setShowProductFlowType( boolean showProductFlowType ) {
		this.showProductFlowType = showProductFlowType;
	}

	public void setShowSourceCategory( boolean showSourceCategory ) {
		this.showSourceCategory = showSourceCategory;
	}

	public void setShowSourceName( boolean showSourceName ) {
		this.showSourceName = showSourceName;
	}

	public void setShowSourceType( boolean showSourceType ) {
		this.showSourceType = showSourceType;
	}

	public void setShowUnitGroupCategory( boolean showUnitGroupCategory ) {
		this.showUnitGroupCategory = showUnitGroupCategory;
	}

	public void setShowUnitGroupDefaultUnit( boolean showUnitGroupDefaultUnit ) {
		this.showUnitGroupDefaultUnit = showUnitGroupDefaultUnit;
	}

	public void setShowUnitGroupName( boolean showUnitGroupName ) {
		this.showUnitGroupName = showUnitGroupName;
	}

	public void configure( ConfigurationService configurationService ) {
		this.showAlternativeHtmlLink = configurationService.fileConfig.getBoolean( "showAlternativeHtmlLink", false );
		this.showBackLinks = configurationService.fileConfig.getBoolean( "showBackLinks", true );
		this.showLoginLink = configurationService.fileConfig.getBoolean( "showLoginLink", true );
		this.showNodeIdAsLink = configurationService.fileConfig.getBoolean( "results.showNodeIdAsLink", true );
		this.showDatasetDetailProcessType = configurationService.fileConfig.getBoolean( "processes.detail.type", true );
	}

	public void configureColumns( ConfigurationService configurationService ) {
		this.showProcessName = configurationService.fileConfig.getBoolean( "processes.name", true );
		this.showProcessLocation = configurationService.fileConfig.getBoolean( "processes.location", true );
		this.showProcessType = configurationService.fileConfig.getBoolean( "processes.type", true );
		this.showProcessSubType = configurationService.fileConfig.getBoolean( "processes.subtype", false );
		this.showProcessClassification = configurationService.fileConfig.getBoolean( "processes.classification", true );
		this.showProcessReferenceYear = configurationService.fileConfig.getBoolean( "processes.referenceYear", true );
		this.showProcessValidUntil = configurationService.fileConfig.getBoolean( "processes.validUntil", true );
		this.showProcessNodeId = configurationService.fileConfig.getBoolean( "processes.nodeId", true );
		this.showMethodName = configurationService.fileConfig.getBoolean( "methods.name", true );
		this.showMethodType = configurationService.fileConfig.getBoolean( "methods.type", true );
		this.showMethodReferenceYear = configurationService.fileConfig.getBoolean( "methods.referenceYear", true );
		this.showMethodDuration = configurationService.fileConfig.getBoolean( "methods.duration", true );
		this.showElementaryFlowName = configurationService.fileConfig.getBoolean( "elementaryFlows.name", true );
		this.showElementaryFlowCategory = configurationService.fileConfig.getBoolean( "elementaryFlows.category", true );
		this.showElementaryFlowRefProperty = configurationService.fileConfig.getBoolean( "elementaryFlows.referenceProperty", true );
		this.showElementaryFlowRefPropertyUnit = configurationService.fileConfig.getBoolean( "elementaryFlows.referencePropertyUnit", true );
		this.showProductFlowName = configurationService.fileConfig.getBoolean( "productFlows.name", true );
		this.showProductFlowType = configurationService.fileConfig.getBoolean( "productFlows.type", true );
		this.showProductFlowCategory = configurationService.fileConfig.getBoolean( "productFlows.category", true );
		this.showProductFlowRefProperty = configurationService.fileConfig.getBoolean( "productFlows.referenceProperty", true );
		this.showProductFlowRefPropertyUnit = configurationService.fileConfig.getBoolean( "productFlows.referencePropertyUnit", true );
		this.showFlowPropertyName = configurationService.fileConfig.getBoolean( "flowProperties.name", true );
		this.showFlowPropertyCategory = configurationService.fileConfig.getBoolean( "flowProperties.category", true );
		this.showFlowPropertyDefaultUnit = configurationService.fileConfig.getBoolean( "flowProperties.defaultUnit", true );
		this.showFlowPropertyDefaultUnitGroup = configurationService.fileConfig.getBoolean( "flowProperties.defaultUnitGroup", true );
		this.showUnitGroupName = configurationService.fileConfig.getBoolean( "unitGroups.name", true );
		this.showUnitGroupCategory = configurationService.fileConfig.getBoolean( "unitGroups.category", true );
		this.showUnitGroupDefaultUnit = configurationService.fileConfig.getBoolean( "unitGroups.defaultUnit", true );
		this.showSourceName = configurationService.fileConfig.getBoolean( "sources.name", true );
		this.showSourceCategory = configurationService.fileConfig.getBoolean( "sources.category", true );
		this.showSourceType = configurationService.fileConfig.getBoolean( "sources.type", true );
		this.showContactName = configurationService.fileConfig.getBoolean( "contacts.name", true );
		this.showContactCategory = configurationService.fileConfig.getBoolean( "contacts.category", true );
		this.showContactEmail = configurationService.fileConfig.getBoolean( "contacts.email", true );
		this.showContactHomePage = configurationService.fileConfig.getBoolean( "contacts.homePage", true );
		this.showManageDatasetImportDate = configurationService.fileConfig.getBoolean( "datasets.manage.importdate", true );
		this.showManageDatasetRootStock = configurationService.fileConfig.getBoolean( "datasets.manage.rootdatastock", true );
		this.showManageDatasetContainedIn = configurationService.fileConfig.getBoolean( "datasets.manage.containedin", true );
		this.showManageDatasetVersion = configurationService.fileConfig.getBoolean( "datasets.manage.version", true );
		this.showManageDatasetMostRecentVersion = configurationService.fileConfig.getBoolean( "datasets.manage.mostrecent", true );
		this.showManageDatasetClassification = configurationService.fileConfig.getBoolean( "datasets.manage.classification", true );
		this.showManageProcessType = configurationService.fileConfig.getBoolean( "processes.manage.type", true );
		this.showManageProcessSubType = configurationService.fileConfig.getBoolean( "processes.manage.subtype", false );
		this.showManageProcessLocation = configurationService.fileConfig.getBoolean( "processes.manage.location", true );
		this.showManageProcessReferenceYear = configurationService.fileConfig.getBoolean( "processes.manage.referenceyear", true );
		this.showManageProcessValidUntil = configurationService.fileConfig.getBoolean( "processes.manage.validuntil", true );
		this.showManageProcessClassificationId = configurationService.fileConfig.getBoolean( "processes.manage.classificationid", false );
	}

	public boolean isShowProcessType() {
		return showProcessType;
	}

	public void setShowProcessType( boolean showProcessType ) {
		this.showProcessType = showProcessType;
	}

	public boolean isShowManageDatasetImportDate() {
		return showManageDatasetImportDate;
	}

	public void setShowManageDatasetImportDate( boolean showManageDatasetImportDate ) {
		this.showManageDatasetImportDate = showManageDatasetImportDate;
	}

	public boolean isShowManageDatasetRootStock() {
		return showManageDatasetRootStock;
	}

	public void setShowManageDatasetRootStock( boolean showManageDatasetRootStock ) {
		this.showManageDatasetRootStock = showManageDatasetRootStock;
	}

	public boolean isShowManageDatasetContainedIn() {
		return showManageDatasetContainedIn;
	}

	public void setShowManageDatasetContainedIn( boolean showManageDatasetContainedIn ) {
		this.showManageDatasetContainedIn = showManageDatasetContainedIn;
	}

	public boolean isShowManageDatasetVersion() {
		return showManageDatasetVersion;
	}

	public void setShowManageDatasetVersion( boolean showManageDatasetVersion ) {
		this.showManageDatasetVersion = showManageDatasetVersion;
	}

	public boolean isShowManageDatasetMostRecentVersion() {
		return showManageDatasetMostRecentVersion;
	}

	public void setShowManageDatasetMostRecentVersion( boolean showManageDatasetMostRecentVersion ) {
		this.showManageDatasetMostRecentVersion = showManageDatasetMostRecentVersion;
	}

	public boolean isShowManageDatasetClassification() {
		return showManageDatasetClassification;
	}

	public void setShowManageDatasetClassification( boolean showManageDatasetClassification ) {
		this.showManageDatasetClassification = showManageDatasetClassification;
	}

	public boolean isShowManageProcessType() {
		return showManageProcessType;
	}

	public void setShowManageProcessType( boolean showManageProcessType ) {
		this.showManageProcessType = showManageProcessType;
	}

	public boolean isShowManageProcessLocation() {
		return showManageProcessLocation;
	}

	public void setShowManageProcessLocation( boolean showManageProcessLocation ) {
		this.showManageProcessLocation = showManageProcessLocation;
	}

	public boolean isShowManageProcessReferenceYear() {
		return showManageProcessReferenceYear;
	}

	public void setShowManageProcessReferenceYear( boolean showManageProcessReferenceYear ) {
		this.showManageProcessReferenceYear = showManageProcessReferenceYear;
	}

	public boolean isShowManageProcessValidUntil() {
		return showManageProcessValidUntil;
	}

	public void setShowManageProcessValidUntil( boolean showManageProcessValidUntil ) {
		this.showManageProcessValidUntil = showManageProcessValidUntil;
	}

	public boolean isShowManageProcessSubType() {
		return showManageProcessSubType;
	}

	public void setShowManageProcessSubType( boolean showManageProcessSubType ) {
		this.showManageProcessSubType = showManageProcessSubType;
	}

	public boolean isShowProcessSubType() {
		return showProcessSubType;
	}

	public void setShowProcessSubType( boolean showProcessSubType ) {
		this.showProcessSubType = showProcessSubType;
	}

	public boolean isShowManageProcessClassificationId() {
		return showManageProcessClassificationId;
	}

	public void setShowManageProcessClassificationId( boolean showClassificationId ) {
		this.showManageProcessClassificationId = showClassificationId;
	}

}
