package de.iai.ilcd.webgui.controller.admin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.zip.ILCDManifest;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DigitalFile;
import de.iai.ilcd.model.dao.CommonDataStockDao;
import de.iai.ilcd.model.dao.ContactDao;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.ElementaryFlowDao;
import de.iai.ilcd.model.dao.FlowDao;
import de.iai.ilcd.model.dao.FlowPropertyDao;
import de.iai.ilcd.model.dao.LCIAMethodDao;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.dao.ProductFlowDao;
import de.iai.ilcd.model.dao.SourceDao;
import de.iai.ilcd.model.dao.UnitGroupDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.model.source.Source;
import de.iai.ilcd.persistence.PersistenceUtil;
import net.java.truevfs.access.TConfig;
import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileOutputStream;
import net.java.truevfs.access.TFileWriter;
import net.java.truevfs.access.TVFS;
import net.java.truevfs.kernel.spec.FsAccessOption;

@ManagedBean
@ViewScoped
public class DataExportController implements Serializable {

	private static final long serialVersionUID = 3800026862547967419L;

	private final Logger logger = LoggerFactory.getLogger( DataExportController.class );

	private StreamedContent file;

	private IDataStockMetaData stock;

	private boolean exportAllVersions = false;

	public DataExportController() {

	}

	public TFile export( IDataStockMetaData stock ) throws IOException {

		// the new archive
		TFile zip = new TFile( ConfigurationService.INSTANCE.getZipFileDirectory() + File.separator + Long.toString( System.currentTimeMillis() ) + ".zip" );

		TConfig config = TConfig.open();
		// Set FsAccessOption.GROW for appending-to for better performance
		config.setAccessPreference( FsAccessOption.GROW, true );

		this.logger.trace( zip.getAbsolutePath() );

		ILCDManifest manifest = new ILCDManifest( Constants.FORMAT_VERSION, "soda4LCA " + ConfigurationService.INSTANCE.getVersionTag() );
		TFileOutputStream tfos = new TFileOutputStream( new TFile( zip, "META-INF/MANIFEST.MF" ) );
		manifest.write( tfos );
		tfos.close();

		 
		long start = System.currentTimeMillis();

		this.logger.info( "exporting flow props" );
		this.exportDatasets( new FlowPropertyDao(), stock, zip );

		this.logger.info( "exporting flows" );
		this.exportDatasets( new ElementaryFlowDao(), stock, zip );

		this.exportDatasets( new ProductFlowDao(), stock, zip );

		this.logger.info( "exporting processes" );
		this.exportDatasets( new ProcessDao(), stock, zip );

		this.logger.info( "exporting LCIA methods" );
		this.exportDatasets( new LCIAMethodDao(), stock, zip );

		this.logger.info( "exporting sources" );
		this.exportDatasets( new SourceDao(), stock, zip );

		this.logger.info( "exporting contacts" );
		this.exportDatasets( new ContactDao(), stock, zip );

		this.logger.info( "exporting unit groups" );
		this.exportDatasets( new UnitGroupDao(), stock, zip );

		try {
			this.logger.debug( "unmounting ZIP" );
			// since we're using GROW, we need to compact
			zip.compact();
			TVFS.umount( zip );
		}
		catch ( Exception e ) {
			this.logger.error( "error writing archive", e );
			return null;
		}
		long stop = System.currentTimeMillis();

		this.logger.info( "done. export took " + (stop - start) / 1000 + " seconds" );

		if ( !exportAllVersions ) {
			// store export tag, only if allversions=false
			storeExportTag( zip, stock );
		}
		return zip;
	}

	private void storeExportTag( TFile zip, IDataStockMetaData stock ) {
		CommonDataStockDao dao = new CommonDataStockDao();
		AbstractDataStock actualStock = dao.getById( stock.getId() );
		actualStock.getExportTag().update( zip.getAbsolutePath() );
		try {
			dao.persist( actualStock );
		}
		catch ( PersistException e ) {
			this.logger.warn( "error persisting export tag", e );
			e.printStackTrace();
		}
	}

	private <T extends DataSet> void exportDatasets( DataSetDao<T, ?, ?> dao, IDataStockMetaData stock, TFile zip ) {

		long dataSetCount;

		if ( stock != null ) {
			dataSetCount = dao.getCount( stock );
		}
		else {
			dataSetCount = dao.getAllCount();
		}

		final int pageSize = 100;

		long pages = dataSetCount / pageSize;
		long remainder = dataSetCount % pageSize;
		if ( remainder > 0 ) {
			pages++;
		}

		if ( this.logger.isTraceEnabled() ) {
			this.logger.trace( dataSetCount + " datasets, pagesize: " + pageSize + ", " + pages + " pages" );
		}

		File dir = null;

		if ( dao instanceof ProcessDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.PROCESSES.getValue() );
		}
		else if ( dao instanceof LCIAMethodDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.LCIAMETHODS.getValue() );
		}
		else if ( dao instanceof FlowDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.FLOWS.getValue() );
		}
		else if ( dao instanceof FlowPropertyDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.FLOWPROPERTIES.getValue() );
		}
		else if ( dao instanceof UnitGroupDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.UNITGROUPS.getValue() );
		}
		else if ( dao instanceof SourceDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.SOURCES.getValue() );
			TFile externalDocsDir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + "external_docs" );
			externalDocsDir.mkdir();
		}
		else if ( dao instanceof ContactDao ) {
			dir = new TFile( zip.getAbsolutePath() + File.separator + "ILCD" + File.separator + DatasetTypes.CONTACTS.getValue() );
		}

		if ( dir != null ) {
			dir.mkdir();
		}
		else {
			throw new IllegalArgumentException( "Invalid DAO object" );
		}

		BufferedWriter writer = null;

		for ( int currentPage = 0; currentPage < pages; currentPage++ ) {
			if ( this.logger.isTraceEnabled() ) {
				this.logger.trace( "exporting page " + (currentPage + 1) + " of " + pages );
			}
			this.writeDatasets( dao, stock, currentPage, pageSize, zip, dir, writer );
		}

	}

	private <T extends DataSet> void writeDatasets( DataSetDao<T, ?, ?> dao, IDataStockMetaData stock, int page, int pageSize, TFile zip, File dir,
			Writer writer ) {

		PersistenceUtil.closeEntityManager();
		List<T> dataSetList;

		if ( stock != null ) {
			dataSetList = dao.get( stock, (page * pageSize), pageSize );
		}
		else {
			dataSetList = dao.get( (page * pageSize), pageSize );
		}

		if ( this.logger.isTraceEnabled() ) {
			this.logger.trace( "  exporting datasets " + (page * pageSize) + " through " + ((page * pageSize) + pageSize - 1) );
		}

		if ( dataSetList.equals( Collections.EMPTY_LIST ) ) {
			this.logger.trace( "(no datasets found)" );
			return;
		}

		StringBuilder externalDocsPathPrefix = new StringBuilder().append( zip.getAbsolutePath() ).append( File.separator ).append( "ILCD" ).append(
				File.separator ).append( "external_docs" ).append( File.separator );

		for ( T dataset : dataSetList ) {

			try {
				String path;
				if ( exportAllVersions ) {
					path = new StringBuilder().append( dir.getAbsolutePath() ).append( File.separator ).append( dataset.getUuidAsString() ).append( "." )
							.append( dataset.getVersion().toString() ).append( ".xml" ).toString();
				}
				else {
					path = new StringBuilder().append( dir.getAbsolutePath() ).append( File.separator ).append( dataset.getUuidAsString() ).append( ".xml" )
							.toString();
				}

				writer = new TFileWriter( new TFile( path ), false, StandardCharsets.UTF_8 );
				writer.write( dataset.getXmlFile().getContent() );
				writer.close();

				if ( dataset instanceof Source ) {
					for ( DigitalFile file : ((Source) dataset).getFiles() ) {
						TFile digitalFile = new TFile( file.getAbsoluteFileName() );
						if ( digitalFile.exists() ) {
							if ( this.logger.isTraceEnabled() )
								this.logger.trace( "writing digital file " + digitalFile );

							String dst = new StringBuilder( externalDocsPathPrefix ).append( digitalFile.getName() ).toString();
							TFile.cp( digitalFile, new TFile( dst ) );
						}
					}
				}

				dataset = null;
			}
			catch ( Exception e ) {
				this.logger.error( "error writing to archive", e );
			}
			finally {
				try {
					if ( writer != null ) {
						writer.close();
					}
				}
				catch ( Exception e ) {
					this.logger.error( "error writing to archive", e );
				}
			}
		}
	}

	public String getZipFileName() {
		if ( this.stock == null ) {
			this.logger.debug( "no datastock selected, exporting entire database" );
			return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + ".zip";
		}
		else {
			this.logger.info( "starting export of datastock " + this.stock.getName() );
			return this.stock.getName() + ".zip";
		}
	}


	public StreamedContent getFile() {

		TFile zip;
		try {
			zip = this.export( this.stock );

			if ( zip != null ) {
				FileInputStream stream = new FileInputStream( zip );
				this.file = new DefaultStreamedContent( stream, "application/zip", this.getZipFileName() );
			}
		}
		catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return this.file;
	}

	public IDataStockMetaData getStock() {
		return this.stock;
	}

	public void setStock( IDataStockMetaData stock ) {
		this.stock = stock;
	}

	public boolean isExportAllVersions() {
		return exportAllVersions;
	}

	public void setExportAllVersions( boolean exportAllVersions ) {
		this.exportAllVersions = exportAllVersions;
	}

}