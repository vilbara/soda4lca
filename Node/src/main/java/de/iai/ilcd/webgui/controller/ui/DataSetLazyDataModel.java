package de.iai.ilcd.webgui.controller.ui;

import java.util.List;
import java.util.Map;

import org.apache.velocity.tools.generic.ValueParser;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.model.utils.DistributedSearchLog;

/**
 * Lazy data model for JSF for all data set types
 * 
 * @param <T>
 *            type of data set
 */
public class DataSetLazyDataModel<T extends DataSet> extends LazyDataModel<T> {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 651369617999373671L;

	/**
	 * Type of data set
	 */
	private final Class<T> type;

	/**
	 * Load most recent version only
	 */
	private final boolean mostRecentOnly;

	/**
	 * DAO for data set type
	 */
	private final DataSetDao<T, ?, ?> daoObject;

	/**
	 * the parameter value parser
	 */
	private ValueParser params;

	/**
	 * the stocks
	 */
	private final IDataStockMetaData[] stocks;

	/**
	 * Log for distributed search
	 */
	private DistributedSearchLog log;

	/**
	 * Initialize lazy data model, package wide visibility for the handler classes
	 * 
	 * @param type
	 *            type of data set
	 * @param daoObject
	 *            DAO for data set
	 * @param mostRecentOnly
	 *            Load most recent version only
	 * @param stocks
	 *            stocks
	 */
	DataSetLazyDataModel( Class<T> type, DataSetDao<T, ?, ?> daoObject, boolean mostRecentOnly, IDataStockMetaData[] stocks ) {
		if ( type == null ) {
			throw new IllegalArgumentException( "Type for lazy data model must not be null" );
		}
		if ( daoObject == null ) {
			throw new IllegalArgumentException( "Dao object for lazy data model must not be null" );
		}
		this.stocks = stocks;
		this.mostRecentOnly = mostRecentOnly;
		this.params = new ValueParser();
		this.type = type;
		this.daoObject = daoObject;
		this.setRowCount( (int) this.daoObject.searchResultCount( this.params, this.mostRecentOnly, stocks ) );
	}

	/**
	 * Get the parameters
	 * 
	 * @return the parameters
	 */
	public ValueParser getParams() {
		return this.params;
	}

	/**
	 * Get the current DAO instance
	 * 
	 * @return current DAO instance
	 */
	protected DataSetDao<T, ?, ?> getDaoObject() {
		return this.daoObject;
	}

	/**
	 * Get the stocks
	 * 
	 * @return stocks
	 */
	protected IDataStockMetaData[] getStocks() {
		return this.stocks;
	}

	/**
	 * Get the type of the data set
	 * 
	 * @return type of the data set
	 */
	protected Class<T> getType() {
		return this.type;
	}


	/**
	 * Determine if only most recent versions shall be loaded
	 * 
	 * @return <code>true</code> if only most recent versions shall be loaded, <code>false</code> otherwise
	 */
	protected boolean isMostRecentOnly() {
		return this.mostRecentOnly;
	}

	/**
	 * Set the parameters
	 * 
	 * @param params
	 *            the parameters to set
	 */
	public void setParams( ValueParser params ) {
		this.params = params;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<T> load( int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {
		this.setRowCount( (int) this.daoObject.searchResultCount( this.params, this.mostRecentOnly, this.stocks ) );

		// SortCriteria sortCriteria = SortCriteria.fromValue(sortField);

		// boolean sortOrder: true = asc / false = desc (old, Primefaces 2.x)
		// enum SortOrder: ASCENDING, DESCENDING, UNSORTED (new, Primefaces 3.x)
		return this.daoObject.lsearch( this.params, first, pageSize, sortField, !SortOrder.DESCENDING.equals( sortOrder ), this.isMostRecentOnly(), this.stocks, null );
	}

	/**
	 * Get the log for distributed search
	 * 
	 * @return log for distributed search
	 */
	public DistributedSearchLog getLog() {
		return this.log;
	}

	/**
	 * Set the log for distributed search
	 * 
	 * @param log
	 *            log for distributed search to set
	 */
	public void setLog( DistributedSearchLog log ) {
		this.log = log;
	}

	@Override
	public T getRowData( String rowKey ) {

		@SuppressWarnings( "unchecked" )
		List<T> l = (List<T>) getWrappedData();

		for ( T o : l ) {
			if ( o.getId().toString().equals( rowKey ) )
					return (T) o;
			}

		return null;
	}

	@Override
	public Object getRowKey( T t ) {
		return t.getId();
	}

}
