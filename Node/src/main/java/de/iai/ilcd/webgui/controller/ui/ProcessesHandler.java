package de.iai.ilcd.webgui.controller.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.SortOrder;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.common.ClClass;
import de.iai.ilcd.model.dao.NetworkNodeDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.model.process.Process;

/**
 * Backing bean for process list view
 */
@ManagedBean
@ViewScoped
public class ProcessesHandler extends AbstractDataSetsHandler<Process, ProcessDao> implements Serializable {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = -3347083803227668593L;

	/**
	 * Key for name filter
	 */
	private final static String NAME_FILTER_KEY = "name";

	/**
	 * Filter key for classes.
	 */
	private final static String CLASSES_FILTER_KEY = "classes";

	/**
	 * 
	 */
	private List<SelectItem> all2ndLevelClasses = null;

	/**
	 * Available stocks bean
	 */
	@ManagedProperty( value = "#{availableStocks}" )
	private AvailableStockHandler availableStocks;

	/**
	 * Initialize handler
	 */
	public ProcessesHandler() {
		// TODO remove 3rd arg in constructor call once primefaces fixes datatable issue
		super( Process.class, new ProcessDao(), "tableForm:processTable" );
	}

	/**
	 * Get the selected classes
	 * 
	 * @return selected classes
	 */
	public List<String> getSelectedClasses() {
		String[] val = super.getFilterStringArr( ProcessesHandler.CLASSES_FILTER_KEY );
		if ( val != null ) {
			return new ArrayList<String>( Arrays.asList( val ) );
		}
		else {
			return null;
		}
	}

	/**
	 * Set the selected classes
	 * 
	 * @param selected
	 *            selected classes
	 */
	public void setSelectedClasses( List<String> selected ) {
		String[] val;
		if ( selected != null && selected.size() > 0 ) {
			val = selected.toArray( new String[0] );
		}
		else {
			val = null;
		}
		super.setFilter( ProcessesHandler.CLASSES_FILTER_KEY, val );
	}

	/**
	 * Get the current value of name filter
	 * 
	 * @return current value of name filter
	 */
	public String getNameFilter() {
		return super.getFilter( ProcessesHandler.NAME_FILTER_KEY );
	}

	/**
	 * Set value for name filter
	 * 
	 * @param nameFilter
	 *            name filter value to set
	 */
	public void setNameFilter( String nameFilter ) {
		super.setFilter( ProcessesHandler.NAME_FILTER_KEY, nameFilter );
	}

	/**
	 * Get all 2nd level classification classes
	 * 
	 * @return 2nd level classification classes
	 */
	public List<SelectItem> getAll2ndLevelClasses() {
		if ( this.all2ndLevelClasses == null ) {
			this.all2ndLevelClasses = new ArrayList<SelectItem>();

			// Load all classes for pick list
			for ( ClClass topClass : super.getDaoInstance().getTopClasses( this.availableStocks.getAllStocksMeta().toArray( new IDataStockMetaData[0] ) ) ) {
				for ( ClClass subClass : super.getDaoInstance().getSubClasses( topClass.getName(), "1", true, this.availableStocks.getAllStocksMeta().toArray( new IDataStockMetaData[0] ) ) ) {
					final String topAndSubClassName = topClass.getName() + " / " + subClass.getName();
					this.all2ndLevelClasses.add( new SelectItem( topAndSubClassName, topAndSubClassName ) );
				}
			}
		}
		return this.all2ndLevelClasses;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataSetLazyDataModel<Process> createLazyDataModel() {
		@SuppressWarnings( "serial" )
		DataSetLazyDataModel<Process> tmp = new DataSetLazyDataModel<Process>( this.getType(), this.getDaoInstance(), true, this.getStockSelection().getCurrentStockAsArray() ) {

			@Override
			@SuppressWarnings( "unchecked" )
			public List<Process> load( int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {
				this.setRowCount( (int) this.getDaoObject().searchResultCount( this.getParams(), this.isMostRecentOnly(), this.getStocks() ) );

				// TODO make that work, this will break the moment a distributed search is being performed
				// (and returns results..)!
				// If there is a type conflict ignoring generics is NOT A SOLUTION!
				List result = this.getDaoObject().searchDist( this.getType(), this.getParams(), first, pageSize, sortField, sortOrder, this.isMostRecentOnly(), this.getStocks(), null, this.getLog() );

				// TODO: this solution is lacking some elegance
				if ( result != null ) {
					int counter = 0;
					for ( Object entity : result ) {
						if ( entity instanceof ProcessDataSetVO ) {
							if ( !((ProcessDataSetVO) entity).getSourceId().equals( ConfigurationService.INSTANCE.getNodeId() ) ) {
								counter++;
							}
						}
					}
					this.setRowCount( this.getRowCount() + counter );
				}
				return result;
			}
		};
		return tmp;
	}

	/**
	 * Get node base URL by registry ID and node ID
	 * 
	 * @param registryName
	 *            registry name (optional, may be null in p2p mode)
	 * @param nodeId
	 *            node ID
	 * @return node base URL or <code>null</code> on errors (e.g. {@link NumberFormatException} with
	 *         <code>registryId</code>)
	 */
	public String getNodeBaseUrl( String registryName, String nodeId ) {
		NetworkNodeDao nnd = new NetworkNodeDao();
		try {
			if ( !StringUtils.isBlank( registryName ) ) {
				// long registryIdLng = Long.parseLong( registryId );
				return nnd.getNetworkNode( nodeId, registryName ).getBaseUrl();
			}
			else {
				return nnd.getNetworkNode( nodeId ).getBaseUrl();
			}
		}
		catch ( Exception ex ) {
			return null;
		}
	}

	/**
	 * Get node id by node ID
	 * 
	 * @param nodeId
	 *            node ID
	 * @return node id or <code>null</code> on errors (e.g. {@link NumberFormatException})
	 */
	public Long getNodeId( String nodeId ) {
		NetworkNodeDao nnd = new NetworkNodeDao();
		try {
			return nnd.getNetworkNode( nodeId ).getId();
		}
		catch ( Exception ex ) {
			return null;
		}
	}

	/**
	 * Set available stocks bean
	 * 
	 * @param availableStocks
	 *            available stocks bean to set
	 */
	public void setAvailableStocks( AvailableStockHandler availableStocks ) {
		this.availableStocks = availableStocks;
	}

	/**
	 * Get the available stocks bean
	 * 
	 * @return available stocks bean
	 */
	public AvailableStockHandler getAvailableStocks() {
		return this.availableStocks;
	}
}
