package de.iai.ilcd.webgui.controller.ui;

import javax.faces.bean.ManagedBean;

import de.fzk.iai.ilcd.api.binding.generated.lciamethod.LCIAMethodDataSetType;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.iai.ilcd.model.dao.LCIAMethodDao;
import de.iai.ilcd.model.lciamethod.LCIAMethod;

/**
 * Backing bean for source LCIA method view
 */
@ManagedBean( name = "lciamethodHandler" )
public class LCIAMethodHandler extends AbstractDataSetHandler<ILCIAMethodVO, LCIAMethod, LCIAMethodDao, LCIAMethodDataSetType> {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 8501172387748377828L;

	/**
	 * Initialize handler
	 */
	public LCIAMethodHandler() {
		super( new LCIAMethodDao(), DatasetTypes.LCIAMETHODS.getValue(), ILCDTypes.LCIAMETHOD );
	}

	/**
	 * Convenience method, delegates to {@link #getDataSet()}
	 * 
	 * @return represented LCIA method instance
	 */
	public ILCIAMethodVO getLciamethod() {
		return this.getDataSet();
	}

}
