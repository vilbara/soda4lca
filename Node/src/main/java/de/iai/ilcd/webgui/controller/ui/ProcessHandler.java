package de.iai.ilcd.webgui.controller.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import de.fzk.iai.ilcd.api.binding.generated.common.ClassType;
import de.fzk.iai.ilcd.api.binding.generated.common.ExchangeDirectionValues;
import de.fzk.iai.ilcd.api.binding.generated.process.ProcessDataSetType;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.enums.LCIMethodApproachesValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfFlowValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.delegate.DataSetRestServiceBD;
import de.iai.ilcd.delegate.ValidationResult;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.flow.Flow;
import de.iai.ilcd.model.process.Exchange;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.model.registry.DataSetRegistrationData;
import de.iai.ilcd.model.registry.Registry;
import de.iai.ilcd.security.UserAccessBean;
import de.iai.ilcd.services.DataSetDeregistrationService;
import de.iai.ilcd.services.DataSetRegistrationService;
import de.iai.ilcd.services.RegistryService;
import de.iai.ilcd.util.CategoryTranslator;
import de.iai.ilcd.util.SodaUtil;
import eu.europa.ec.jrc.lca.commons.service.exceptions.AuthenticationException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.RestWSUnknownException;
import eu.europa.ec.jrc.lca.commons.view.util.Messages;
import eu.europa.ec.jrc.lca.registry.domain.DataSet;

/**
 * Backing bean for process detail view
 */
@ManagedBean
@ViewScoped
public class ProcessHandler extends AbstractDataSetHandler<IProcessVO, Process, ProcessDao, ProcessDataSetType> {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 6907352892423743765L;

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( ProcessHandler.class );

	/**
	 * List with input product flows
	 */
	private List<Flow> inputProducts;

	/**
	 * List with co product flows
	 */
	private List<Flow> coProducts;

	private final DataSetRegistrationService dataSetRegistrationService;

	private final DataSetDeregistrationService dataSetDeregistrationService;

	private String reason;

	private DataSetRegistrationData selectedDataSetRegistrationData;

	private final RegistryService registryService;

	/**
	 * Flag to indicate if EPD type
	 */
	private boolean epd;

	/**
	 * Initialize the handler
	 */
	public ProcessHandler() {
		super( new ProcessDao(), DatasetTypes.PROCESSES.getValue(), ILCDTypes.PROCESS );
		WebApplicationContext ctx = FacesContextUtils.getWebApplicationContext( FacesContext.getCurrentInstance() );
		this.dataSetRegistrationService = ctx.getBean( DataSetRegistrationService.class );
		this.dataSetDeregistrationService = ctx.getBean( DataSetDeregistrationService.class );
		this.registryService = ctx.getBean( RegistryService.class );
	}

	/**
	 * Convenience method, delegates to {@link #getDataSet()}
	 * 
	 * @return process
	 */
	public IProcessVO getProcess() {
		return this.getDataSet();
	}

	/**
	 * We need this as workaround because the Service API delivers a set but we need a List in the template
	 * 
	 * @return
	 */
	public List<LCIMethodApproachesValue> getApproaches() {
		List<LCIMethodApproachesValue> approaches = new ArrayList<LCIMethodApproachesValue>();

		if ( this.getDataSet() != null ) {
			approaches = new ArrayList<LCIMethodApproachesValue>( this.getDataSet().getLCIMethodInformation().getApproaches() );
		}

		return approaches;
	}

	/**
	 * We need this too as workaround to geth the ComplianceSystems as List
	 * 
	 * @return
	 */
	public List<IComplianceSystem> getComplianceSystems() {
		List<IComplianceSystem> complianceSystems = new ArrayList<IComplianceSystem>();

		if ( this.getDataSet() != null ) {
			complianceSystems = new ArrayList<IComplianceSystem>( this.getDataSet().getComplianceSystems() );
		}

		return complianceSystems;
	}

	/**
	 * Get the input product flows
	 * 
	 * @return input product flows
	 */
	public List<Flow> getInputProducts() {
		return this.inputProducts;
	}

	/**
	 * Get the co-product flows
	 * 
	 * @return co-product flows
	 */
	public List<Flow> getCoProducts() {
		return this.coProducts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void datasetLoaded( Process p ) {
		// Load input products
		List<Exchange> tmp = p.getExchanges( ExchangeDirectionValues.INPUT.name() );
		this.inputProducts = new ArrayList<Flow>();
		for ( Exchange e : tmp ) {
			if ( e.getFlow() != null && TypeOfFlowValue.PRODUCT_FLOW.equals( e.getFlow().getType() ) ) {
				this.inputProducts.add( e.getFlow() );
			}
		}

		// Load co-products
		tmp = p.getExchanges( ExchangeDirectionValues.OUTPUT.name() );
		this.coProducts = new ArrayList<Flow>();
		// named loop (due to continue from inner loop)
		coProdLoop: for ( Exchange e : tmp ) {
			if ( e.getFlow() != null && TypeOfFlowValue.PRODUCT_FLOW.equals( e.getFlow().getType() ) ) {
				// check if this exchange flow is contained in the reference exchange list
				Long id = e.getFlow().getId();
				for ( Exchange refEx : p.getReferenceExchanges() ) {
					if ( refEx.getFlow() != null && id.equals( refEx.getFlow().getId() ) ) {
						// trigger next loop from !! outer !! loop
						continue coProdLoop;
					}
				}
				// flow was not in reference exchange list
				this.coProducts.add( e.getFlow() );
			}
		}
		// set EPD flag
		this.epd = TypeOfProcessValue.EPD.equals( p.getType() );
	}

	public String deregisterSelected() {
		UserAccessBean user = new UserAccessBean();
		try {
			if ( user.hasAdminAreaAccessRight() ) {
				this.dataSetDeregistrationService.deregisterDatasets( Collections.singletonList( this.selectedDataSetRegistrationData ), this.reason,
						this.selectedDataSetRegistrationData.getRegistry() );
			}
		}
		catch ( RestWSUnknownException e ) {
			FacesMessage message = Messages.getMessage( "resources.lang", "admin.deregisterDataSets.restWSUnknownException", null );
			message.setSeverity( FacesMessage.SEVERITY_ERROR );
			FacesContext.getCurrentInstance().addMessage( null, message );
		}
		catch ( AuthenticationException e ) {
			FacesMessage message = Messages.getMessage( "resources.lang", "authenticationException_errorMessage", null );
			message.setSeverity( FacesMessage.SEVERITY_ERROR );
			FacesContext.getCurrentInstance().addMessage( null, message );
		}
		return null;
	}

	public List<DataSetRegistrationData> getRegistrations() {
		return this.dataSetRegistrationService.getListOfRegistrations( (Process) this.getDataSet() );
	}

	public DataSetRegistrationData getSelectedDataSetRegistrationData() {
		return this.selectedDataSetRegistrationData;
	}

	public void setSelectedDataSetRegistrationData( DataSetRegistrationData selectedDataSetRegistrationData ) {
		this.selectedDataSetRegistrationData = selectedDataSetRegistrationData;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason( String reason ) {
		this.reason = reason;
	}

	public ValidationResult getDatasetValidationResult() {
		if ( this.getRegistryUUID() != null && !this.getRegistryUUID().trim().equals( "" ) ) {
			Registry reg = this.registryService.findByUUID( this.getRegistryUUID() );
			DataSet ds = this.getDataSetFromProcess();
			return DataSetRestServiceBD.getInstance( reg ).verify( ds );
		}
		else {
			return ValidationResult.CANT_VALIDATE_NOT_REGISTERED;
		}
	}

	private DataSet getDataSetFromProcess() {
		Process p = (Process) this.getDataSet();
		DataSet ds = new DataSet();
		ds.setUuid( p.getUuid().getUuid() );
		ds.setVersion( p.getVersion().getVersionString() );
		ds.setHash( p.getXmlFile().getContentHash() );
		return ds;
	}

	/**
	 * Determines if process is of type EPD
	 * 
	 * @return <code>true</code> if process is of type EPD, <code>false</code> otherwise
	 */
	public boolean isEPD() {
		return this.epd;
	}

	/**
	 * @see SodaUtil#replace(String, String, String)
	 */
	public String replace( String text, String searchString, String replacement ) {
		return SodaUtil.replace( text, searchString, replacement );
	}

	/**
	 * @see SodaUtil#groupHint(String, java.util.ResourceBundle)
	 */
	public String groupHint( String group ) {
		return SodaUtil.groupHint( group, this.getI18n() );
	}

	/**
	 * Helper method returns a String containing slash separated inline list of class types as a workaround
	 * 
	 * @param clazz
	 *            class type as list
	 * @return String containing slash separated inline list of class types
	 */
	public String getClassTypeListInline( List<ClassType> clazz, String catSystem, String language ) {
		boolean translate = (ConfigurationService.INSTANCE.isTranslateClassification() && !StringUtils.equalsIgnoreCase( language, ConfigurationService.INSTANCE
				.getDefaultLanguage() ));

		CategoryTranslator t = null;

		if ( translate )
			try {
				t = new CategoryTranslator( DataSetType.PROCESS, catSystem );
			}
			catch ( Exception e ) {
				LOGGER.warn( "Could not instantiate CategoryTranslator", e );
				translate = false;
			}

		if ( CollectionUtils.isNotEmpty( clazz ) ) {
			StringBuilder classListSb = new StringBuilder();
			for ( Iterator<ClassType> classIterator = clazz.iterator(); classIterator.hasNext(); ) {
				ClassType classType = classIterator.next();
				if ( !translate )
					classListSb.append( classType.getValue() );
				else
					classListSb.append( t.translateTo( classType.getClassId(), language ) );

				if ( classIterator.hasNext() ) {
					classListSb.append( " / " );
				}
			}
			return classListSb.toString();
		}
		return null;
	}

}
