package de.iai.ilcd.model.common;


/**
 * 
 * @author clemens.duepmeier
 */
public enum DataSetType {
	PROCESS( "process data set" ), FLOW( "flow data set" ), FLOWPROPERTY( "flowproperty data set" ), SOURCE( "source data set" ), UNITGROUP(
			"unitgroup data set" ), CONTACT( "contact data set" ), LCIAMETHOD( "lcia method data set" );

	private String value;

	DataSetType( String value ) {
		this.value = value;
	}

	public static DataSetType fromValue( String value ) {
		for ( DataSetType enumValue : DataSetType.values() ) {
			if ( enumValue.getValue().equals( value ) ) {
				return enumValue;
			}
		}
		return null;
	}

	public String getValue() {
		return this.value;
	}
}
