package de.iai.ilcd.model.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.ValueParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.api.binding.generated.common.ExchangeDirectionValues;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.enums.TypeOfProcessValue;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.GeographicalArea;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.model.flow.Flow;
import de.iai.ilcd.model.process.Exchange;
import de.iai.ilcd.model.process.LciaResult;
import de.iai.ilcd.model.process.LciaResultClClassStats;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.persistence.PersistenceUtil;

/**
 * Data access object for {@link Process processes}
 */
public class ProcessDao extends DataSetDao<Process, IProcessListVO, IProcessVO> {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger( ProcessDao.class );

	/**
	 * Create the data access object for {@link Process processes}
	 */
	public ProcessDao() {
		super( "Process", Process.class, IProcessListVO.class, IProcessVO.class, DataSetType.PROCESS );
	}

	@Override
	protected String getDataStockField() {
		return DatasetTypes.PROCESSES.getValue();
	}

	public Process getFullProcess( String uuid ) {
		Process process = this.getByUuid( uuid );
		if ( process != null ) {
			this.addFlowsToExchanges( process );
		}
		return process;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( Process dataSet ) {
		this.addFlowsToExchanges( dataSet );

		this.updateTimestampForLciaResultCacheObjects( dataSet );
	}

	/**
	 * Get the list of processes which have the provided direction and flow as in- or output exchange flow
	 * 
	 * @param flowUuid
	 *            uuid of flow
	 * @param direction
	 *            direction of flow
	 * @param firstResult
	 *            start index
	 * @param maxResults
	 *            maximum result items
	 * @return list of processes which have the provided direction and flow as in- or output exchange flow
	 */
	@SuppressWarnings( "unchecked" )
	public List<Process> getProcessesForExchangeFlow( String flowUuid, ExchangeDirectionValues direction, int firstResult, int maxResults ) {
		Query q = this.getProcessesForExchangeFlowQuery( flowUuid, direction, false );
		q.setFirstResult( firstResult );
		q.setMaxResults( maxResults );
		return q.getResultList();
	}

	/**
	 * Get count of processes for provided exchange flow and direction
	 * 
	 * @param flowUuid
	 *            uuid of flow
	 * @param direction
	 *            direction of flow
	 * @return count of processes for provided exchange flow and direction
	 */
	public long getProcessesForExchangeFlowCount( String flowUuid, ExchangeDirectionValues direction ) {
		Query q = this.getProcessesForExchangeFlowQuery( flowUuid, direction, true );
		return (Long) q.getSingleResult();
	}

	/**
	 * Get query for list or count of processes which have provided flow as in- or output exchange flow
	 * 
	 * @param flow
	 *            flow to get processes for
	 * @param direction
	 *            direction of process
	 * @param count
	 *            flag to indicate if count query shall be created
	 * @return query for list or count of processes which have provided flow as in- or output exchange flow
	 */
	private Query getProcessesForExchangeFlowQuery( String flowUuid, ExchangeDirectionValues direction, boolean count ) {
		if ( flowUuid == null ) {
			throw new IllegalArgumentException( "Flow must not be null!" );
		}
		if ( direction == null ) {
			throw new IllegalArgumentException( "Direction must not be null!" );
		}
		EntityManager em = PersistenceUtil.getEntityManager();

		StringBuilder sb = new StringBuilder();
		sb.append( "SELECT " );

		if ( count ) {
			sb.append( "COUNT(DISTINCT p)" );
		}
		else {
			sb.append( "DISTINCT p" );
		}

		sb.append( " FROM Process p LEFT JOIN p.exchanges e WHERE e.flow.uuid.uuid=:uuid AND e.exchangeDirection=:dir" );

		Query q = em.createQuery( sb.toString() );

		q.setParameter( "uuid", flowUuid );
		q.setParameter( "dir", direction );

		return q;

	}

	@SuppressWarnings( "unchecked" )
	public List<GeographicalArea> getUsedLocations() {
		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery( "select distinct area from Process p, GeographicalArea area where p.geography.location=area.areaCode order by area.name" )
				.getResultList();
	}

	@SuppressWarnings( "unchecked" )
	public List<GeographicalArea> getAllLocations() {
		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery( "select distinct area from GeographicalArea area order by area.name" ).getResultList();
	}

	@SuppressWarnings( "unchecked" )
	public List<Integer> getReferenceYears() {
		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery( "select distinct p.timeInformation.referenceYear from Process p order by p.timeInformation.referenceYear asc" ).getResultList();
	}

	@SuppressWarnings( "unchecked" )
	public List<Integer> getValidUntilYears() {
		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery( "select distinct p.timeInformation.validUntil from Process p order by p.timeInformation.validUntil asc" ).getResultList();
	}

	/**
	 * Get the reference years
	 * 
	 * @param stocks
	 *            stocks to get reference years for
	 * @return loaded years. <b>Please note:</b> no stocks (<code>null</code> or empty array) will return an empty list!
	 */
	@SuppressWarnings( "unchecked" )
	public List<Integer> getReferenceYears( IDataStockMetaData... stocks ) {
		if ( stocks == null || stocks.length == 0 ) {
			return new ArrayList<Integer>();
		}

		List<String> lstRdsIds = new ArrayList<String>();
		List<String> lstDsIds = new ArrayList<String>();
		for ( IDataStockMetaData m : stocks ) {
			if ( m.isRoot() ) {
				lstRdsIds.add( Long.toString( m.getId() ) );
			}
			else {
				lstDsIds.add( Long.toString( m.getId() ) );
			}
		}

		String join = "";
		final List<String> whereSmtnts = new ArrayList<String>();

		if ( !lstRdsIds.isEmpty() ) {
			whereSmtnts.add( "p.rootDataStock.id in (" + this.join( lstRdsIds, "," ) + ")" );
		}
		if ( !lstDsIds.isEmpty() ) {
			join = " LEFT JOIN p.containingDataStocks ds";
			whereSmtnts.add( "ds.id in (" + this.join( lstDsIds, "," ) + ")" );
		}

		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery(
				"select distinct p.timeInformation.referenceYear from Process p" + join + " WHERE " + StringUtils.join( whereSmtnts, " OR " )
				+ " order by p.timeInformation.referenceYear asc" ).getResultList();
	}

	/**
	 * Get the validUntil years
	 * 
	 * @param stocks
	 *            stocks to get validUntil years for
	 * @return loaded years. <b>Please note:</b> no stocks (<code>null</code> or empty array) will return an empty list!
	 */
	@SuppressWarnings( "unchecked" )
	public List<Integer> getValidUntilYears( IDataStockMetaData... stocks ) {
		if ( stocks == null || stocks.length == 0 ) {
			return new ArrayList<Integer>();
		}

		List<String> lstRdsIds = new ArrayList<String>();
		List<String> lstDsIds = new ArrayList<String>();
		for ( IDataStockMetaData m : stocks ) {
			if ( m.isRoot() ) {
				lstRdsIds.add( Long.toString( m.getId() ) );
			}
			else {
				lstDsIds.add( Long.toString( m.getId() ) );
			}
		}

		String join = "";
		final List<String> whereSmtnts = new ArrayList<String>();

		if ( !lstRdsIds.isEmpty() ) {
			whereSmtnts.add( "p.rootDataStock.id in (" + this.join( lstRdsIds, "," ) + ")" );
		}
		if ( !lstDsIds.isEmpty() ) {
			join = " LEFT JOIN p.containingDataStocks ds";
			whereSmtnts.add( "ds.id in (" + this.join( lstDsIds, "," ) + ")" );
		}

		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery(
				"select distinct p.timeInformation.validUntil from Process p" + join + " WHERE " + StringUtils.join( whereSmtnts, " OR " )
				+ " order by p.timeInformation.validUntil asc" ).getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString ) {
		if ( "type.value".equals( sortString ) ) {
			return typeAlias + ".type";
		}
		else if ( "location".equals( sortString ) ) {
			return typeAlias + ".geography.location";
		}
		else if ( "timeInformation.referenceYear".equals( sortString ) ) {
			return typeAlias + ".timeInformation.referenceYear";
		}
		else if ( "timeInformation.validUntil".equals( sortString ) ) {
			return typeAlias + ".timeInformation.validUntil";
		}
		else if ( "LCIMethodInformation.methodPrinciple.value".equals( sortString ) ) {
			return typeAlias + ".lCIMethodInformation.methodPrinciple";
		}
		else if ( "complianceSystems".equals( sortString ) ) {
			return typeAlias + ".complianceSystemCache";
		}
		else if ( "subType".equals( sortString ) ) {
			return typeAlias + ".subType";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringJoinPart( ValueParser params, String typeAlias ) {
		boolean exchangeFlowQuery = !StringUtils.isBlank( params.getString( "exchangeFlow" ) );
		if ( exchangeFlowQuery ) {
			return "LEFT JOIN " + typeAlias + ".exchanges " + typeAlias + "Ex";
		}

		boolean hasNameParam = !StringUtils.isBlank( params.getString( "name" ) );
		boolean hasDescParam = !StringUtils.isBlank( params.getString( "description" ) );

		if ( hasNameParam || hasDescParam ) {
			StringBuilder buf = new StringBuilder();
			if ( hasNameParam ) {
				buf.append( "LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".baseName bn LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".nameRoute nr LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".nameLocation nl LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".nameUnit nu LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".synonyms syn " );
			}
			if ( hasDescParam ) {
				buf.append( "LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".description des LEFT JOIN " );
				buf.append( typeAlias );
				buf.append( ".useAdvice ua " );
			}
			return buf.toString();
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// time information stuff
		Integer validFrom = this.parseInteger( params.getString( "referenceYear" ) );
		Integer validUntil = this.parseInteger( params.getString( "validUntil" ) );

		if ( validFrom != null || validUntil != null ) {
			validFrom = validFrom != null ? validFrom : validUntil;
			validUntil = validUntil != null ? validUntil : validFrom;

			whereClauses
			.add( "(" + typeAlias + ".timeInformation.referenceYear <= :validFrom AND " + typeAlias + ".timeInformation.validUntil >= :validUntil)" );
			whereParamValues.put( "validFrom", validFrom );
			whereParamValues.put( "validUntil", validUntil );
		}

		// parameterized
		if ( params.getString( "parameterized" ) != null ) {
			whereClauses.add( typeAlias + ".parameterized=:parameterized" );
			whereParamValues.put( "parameterized", Boolean.TRUE );
		}

		// exchange flows
		final boolean exchangeFlowQuery = params.getString( "exchangeFlow" ) != null;
		final String exAlias = typeAlias + "Ex";

		if ( exchangeFlowQuery ) {
			whereClauses.add( exAlias + ".flow.uuid.uuid=:exFlowUuid" );
			whereClauses.add( exAlias + ".exchangeDirection=:exFlowDir" );

			whereParamValues.put( "exFlowUuid", params.getString( "exchangeFlow" ) );
			whereParamValues.put( "exFlowDir", params.get( "exchangeFlowDirection" ) );
		}

		// classification
		String[] categories = params.getStrings( "classes" );
		if ( categories != null && categories.length > 0 ) {
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			int paramNo = 0;
			for ( String cat : categories ) {
				if ( first ) {
					first = false;
				}
				else {
					sb.append( " OR " );
				}
				String catParam = "classParam" + Integer.toString( paramNo++ );
				sb.append( typeAlias + ".classificationCache LIKE :" + catParam );
				whereParamValues.put( catParam, "%" + cat + "%" );
			}
			whereClauses.add( sb.toString() );
		}

		// type
		String type = params.getString( "type" );
		if ( type != null && (type.length() > 3) && (!type.equals( "select option" )) ) {
			TypeOfProcessValue typeValue = null;
			try {
				typeValue = TypeOfProcessValue.valueOf( type );
			}
			catch ( Exception e ) {
				// ignore it as we do not have a parsable value
			}
			if ( typeValue != null ) {
				whereClauses.add( typeAlias + ".type=:typeOfProc" );
				whereParamValues.put( "typeOfProc", typeValue );
			}
		}

		// Locations
		String[] locations = params.getStrings( "location" );
		if ( locations != null && locations.length > 0 ) {
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			int paramNo = 0;
			for ( String loc : locations ) {
				if ( first ) {
					first = false;
				}
				else {
					sb.append( " OR " );
				}
				String locParam = "locParam" + Integer.toString( paramNo++ );
				sb.append( typeAlias + ".geography.location=:" + locParam );
				whereParamValues.put( locParam, loc );
			}
			whereClauses.add( sb.toString() );
		}

		// registries
		String registeredIn = params.getString( "registeredIn" );
		if ( registeredIn != null ) {
			whereClauses.add( "EXISTS (SELECT dsrd FROM DataSetRegistrationData dsrd WHERE dsrd.registry.uuid = :registeredIn " + "AND dsrd.uuid = "
					+ typeAlias + ".uuid.uuid " + "AND dsrd.version = " + typeAlias + ".version "
					+ "AND dsrd.status =  de.iai.ilcd.model.registry.DataSetRegistrationDataStatus.ACCEPTED" + ")" );
			whereParamValues.put( "registeredIn", registeredIn );
		}

	}

	/**
	 * Override default behavior for name and description filter in order to use {@link Process#getProcessName()}
	 * instead of {@link Process#getName()}.
	 */
	@Override
	protected void addNameDescWhereClauseAndNamedParamForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {

		// name and description
		final String namePhrase = params.getString( "name" );
		final String descriptionPhrase = params.getString( "description" );

		boolean hasNameParam = !StringUtils.isBlank( namePhrase );
		boolean hasDescParam = !StringUtils.isBlank( descriptionPhrase ) && descriptionPhrase.length() > 2;
		if ( hasNameParam || hasDescParam ) {
			StringBuilder sb = new StringBuilder( "(" );
			if ( hasNameParam ) {
				sb.append( "bn LIKE :namePhrase OR " );
				sb.append( "nr LIKE :namePhrase OR " );
				sb.append( "nl LIKE :namePhrase OR " );
				sb.append( "nu LIKE :namePhrase OR " );
				sb.append( "syn LIKE :namePhrase" );
				whereParamValues.put( "namePhrase", "%" + namePhrase + "%" );
			}
			if ( hasDescParam ) {
				if ( hasNameParam ) {
					sb.append( " OR " );
				}
				sb.append( "des LIKE :descriptionPhrase OR " );
				sb.append( "ua LIKE :descriptionPhrase" );
				whereParamValues.put( "descriptionPhrase", "%" + descriptionPhrase + "%" );
			}
			sb.append( ")" );
			whereClauses.add( sb.toString() );
		}

	}

	/**
	 * Parse integer and return <code>null</code> on arbitrary error
	 * 
	 * @param s
	 *            string to parse
	 * @return parsed integer or <code>null</code>
	 */
	private Integer parseInteger( String s ) {
		if ( StringUtils.isBlank( s ) ) {
			return null;
		}
		try {
			return new Integer( s );
		}
		catch ( Exception e ) {
			return null;
		}
	}

	/**
	 * Get a foreign process
	 * 
	 * @param nodeShortName
	 *            short name of the node
	 * @param uuid
	 *            UUID of process
	 * @return loaded process
	 */
	public IProcessVO getForeignProcess( String nodeShortName, String uuid, Long registryId ) {
		ForeignDataSetsHelper foreignHelper = new ForeignDataSetsHelper();
		return foreignHelper.getForeignDataSet( IProcessVO.class, nodeShortName, uuid, registryId );
	}

	/**
	 * Add flows references to the exchanges. This means that references to {@link Exchange#getFlow() data base flows}
	 * shall be established where only {@link Exchange#getFlowReference() global references} are set.
	 * 
	 * @param process
	 *            process to find flows for
	 */
	private void addFlowsToExchanges( Process process ) {
		ElementaryFlowDao eFlowDao = new ElementaryFlowDao();
		ProductFlowDao pFlowDao = new ProductFlowDao();

		for ( Exchange exchange : process.getExchanges() ) {

			if ( exchange.getFlow() != null ) {
				continue; // flow already associated
			}
			if ( exchange.getFlowReference() == null ) {
				continue;
			}
			GlobalReference flowReference = exchange.getFlowReference();
			String flowUuid = null;
			if ( flowReference.getUuid() == null ) {
				// crap, no uuid attribute in flow reference
				// but we can try to get it from the uri attribute if there
				String uri = flowReference.getUri();
				if ( uri == null ) {
					continue; // sorry we can't get uuid
				}
				String[] splittedUri = uri.split( "_" );
				if ( splittedUri.length <= 2 ) {
					continue; // uri does not contain enough parts: so no uuid
				}
				flowUuid = splittedUri[splittedUri.length - 2];
			}
			else {
				flowUuid = exchange.getFlowReference().getUuid().getUuid();
			}
			Flow flow = null;
			try {
				flow = pFlowDao.getByUuid( flowUuid );
				if ( flow != null ) {
					exchange.setFlow( flow );
				}
				else {
					exchange.setFlow( eFlowDao.getByUuid( flowUuid ) );
				}
			}
			catch ( NoResultException ex ) {
				// OK, we have no associated flow, so do nothing
			}
		}
	}

	/**
	 * Update the time stamps for the {@link LciaResultClClassStats} cache objects
	 * 
	 * @param p
	 *            the process to update {@link LciaResultClClassStats} for
	 */
	private void updateTimestampForLciaResultCacheObjects( Process p ) {
		if ( p != null && CollectionUtils.isNotEmpty( p.getLciaResults() ) ) {
			List<String> ugUuids = new ArrayList<String>();
			for ( LciaResult r : p.getLciaResults() ) {
				String ugUuid = r.getUnitGroupReference().getUuid().getUuid();
				if ( !ugUuids.contains( ugUuid ) ) {
					ugUuids.add( ugUuid );
				}
			}

			if ( CollectionUtils.isNotEmpty( ugUuids ) ) {
				LciaResultClClassStatsDao statsDao = new LciaResultClClassStatsDao();
				long now = System.currentTimeMillis();
				try {
					for ( String ugUuid : ugUuids ) {
						List<LciaResultClClassStats> statsList = statsDao.getNotDirty( ugUuid );
						for ( LciaResultClClassStats statsObj : statsList ) {
							statsObj.setTsLastChange( now );
						}
						statsDao.merge( statsList );
					}
				}
				catch ( Exception e ) {
					LOGGER.error( "Error while updating time stamps of statistics cache objects", e );
				}
			}

		}
	}

	/**
	 * {@inheritDoc} <br />
	 * Plus: update time stamps for LCIA result cache objects
	 */
	@Override
	public Collection<Process> remove( Collection<Process> objs ) throws Exception {
		Collection<Process> tmp = super.remove( objs );
		for ( Process p : tmp ) {
			this.updateTimestampForLciaResultCacheObjects( p );
		}
		return tmp;
	}

	/**
	 * {@inheritDoc} <br />
	 * Plus: update time stamps for LCIA result cache objects
	 */
	@Override
	public Process remove( Process dataSet ) throws DeleteDataSetException {
		Process p = super.remove( dataSet );
		this.updateTimestampForLciaResultCacheObjects( dataSet );
		return p;
	}

	/**
	 * {@inheritDoc} <br />
	 * Plus: update time stamps for LCIA result cache objects
	 */
	@Override
	public Process merge( Process obj ) throws MergeException {
		Process p = super.merge( obj );
		this.updateTimestampForLciaResultCacheObjects( obj );
		return p;
	}

	/**
	 * {@inheritDoc} <br />
	 * Plus: update time stamps for LCIA result cache objects
	 */
	@Override
	public Collection<Process> merge( Collection<Process> objs ) throws MergeException {
		Collection<Process> tmp = super.merge( objs );
		for ( Process p : tmp ) {
			this.updateTimestampForLciaResultCacheObjects( p );
		}
		return tmp;
	}

}
