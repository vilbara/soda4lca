package de.iai.ilcd.model.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.process.ITimeInformation;
import de.iai.ilcd.util.lstring.IStringMapProvider;
import de.iai.ilcd.util.lstring.MultiLangStringMapAdapter;

/**
 * 
 * @author clemens.duepmeier
 */
@Entity( name = "process_timeinformation" )
@Table( name = "process_timeinformation" )
public class TimeInformation implements Serializable, ITimeInformation {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 3919984048290994958L;

	/**
	 * ID of Time information
	 */
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	/**
	 * Get the ID of Time information
	 * 
	 * @return the ID of Time information
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Set the ID of Time information
	 * 
	 * @param id
	 *            the ID of Time information to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	protected Integer referenceYear = null;

	protected Integer validUntil = null;

	@ElementCollection
	@Column( name = "value", columnDefinition = "TEXT" )
	@CollectionTable( name = "process_timedescription", joinColumns = @JoinColumn( name = "process_timeinformation_id" ) )
	@MapKeyColumn( name = "lang" )
	protected final Map<String, String> description = new HashMap<String, String>();

	/**
	 * Adapter for API backwards compatibility.
	 */
	@Transient
	private final MultiLangStringMapAdapter descriptionAdapter = new MultiLangStringMapAdapter( new IStringMapProvider() {

		@Override
		public Map<String, String> getMap() {
			return TimeInformation.this.description;
		}
	} );

	public IMultiLangString getDescription() {
		return this.descriptionAdapter;
	}

	public void setDescription( IMultiLangString description ) {
		this.descriptionAdapter.overrideValues( description );
	}

	@Override
	public Integer getReferenceYear() {
		return this.referenceYear;
	}

	public void setReferenceYear( Integer referenceYear ) {
		this.referenceYear = referenceYear;
	}

	@Override
	public Integer getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil( Integer validUntil ) {
		this.validUntil = validUntil;
	}
}
