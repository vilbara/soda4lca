package de.iai.ilcd.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupVO;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.unitgroup.UnitGroup;

/**
 * Data access object for {@link UnitGroup unit groups}
 */
public class UnitGroupDao extends DataSetDao<UnitGroup, IUnitGroupListVO, IUnitGroupVO> {

	/**
	 * Create the data access object for {@link UnitGroup unit groups}
	 */
	public UnitGroupDao() {
		super( "UnitGroup", UnitGroup.class, IUnitGroupListVO.class, IUnitGroupVO.class, DataSetType.UNITGROUP );
	}

	@Override
	protected String getDataStockField() {
		return "unitGroups";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( UnitGroup dataSet ) {
		// Nothing to to :)
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString ) {
		if ( "referenceUnit.name".equals( sortString ) ) {
			return typeAlias + ".referenceUnit.name";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString );
		}
	}

}
