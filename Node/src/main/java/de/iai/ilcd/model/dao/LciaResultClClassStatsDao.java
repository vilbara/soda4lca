package de.iai.ilcd.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import de.iai.ilcd.model.process.LciaResultClClassStats;
import de.iai.ilcd.persistence.PersistenceUtil;

/**
 * DAO for {@link LciaResultClClassStats}
 */
public class LciaResultClClassStatsDao extends AbstractLongIdObjectDao<LciaResultClClassStats> {

	/**
	 * Create DAO
	 */
	public LciaResultClClassStatsDao() {
		super( LciaResultClClassStats.class.getSimpleName(), LciaResultClClassStats.class );
	}

	/**
	 * Get statistics objects for UUID of unit group that are currently not in dirty state (dirty means
	 * {@link LciaResultClClassStats#getTsCalculated() tsCalculated} &lt;
	 * {@link LciaResultClClassStats#getTsLastChange() tsChanged})
	 * 
	 * @param ugUuid
	 *            UUID of unit group
	 * @return list of statistic objects
	 */
	@SuppressWarnings( "unchecked" )
	public List<LciaResultClClassStats> getNotDirty( String ugUuid ) {
		EntityManager em = PersistenceUtil.getEntityManager();

		Query q = em.createQuery( "SELECT a FROM " + this.getJpaName() + " a WHERE a.tsCalculated >= a.tsLastChange AND a.ugUuid=:ugUuid" );
		q.setParameter( "ugUuid", ugUuid );

		return q.getResultList();
	}

	/**
	 * Get the statistics object
	 * 
	 * @param clid
	 *            class id
	 * @param ugUuid
	 *            unit group uuid
	 * @param module
	 *            module
	 * @return statistics object
	 */
	public LciaResultClClassStats get( String clid, String ugUuid, String module ) {
		EntityManager em = PersistenceUtil.getEntityManager();

		LciaResultClClassStats foo = null;

		try {
			try {
				Query q = em.createQuery( "SELECT a FROM " + this.getJpaName() + " a WHERE a.clid=:clid AND a.ugUuid=:ugUuid AND a.module=:module" );
				q.setParameter( "clid", clid );
				q.setParameter( "ugUuid", ugUuid );
				q.setParameter( "module", module );

				foo = (LciaResultClClassStats) q.getSingleResult();

				if ( foo.getTsCalculated() < foo.getTsLastChange() ) {
					return this.recalculateLciaResClClassStats( foo );
				}
				else {
					return foo;
				}
			}
			catch ( NoResultException nre ) {
				return this.createNotExisting( clid, ugUuid, module );
			}
		}
		catch ( PersistException pex ) {
			return null;
		}
		catch ( MergeException mex ) {
			return null;
		}
	}

	/**
	 * Create statistics object for combination of <code>clid</code>, <code>ugUuid</code> and <code>module</code> that
	 * does not yet exist
	 * 
	 * @param clid
	 *            class id
	 * @param ugUuid
	 *            unit group uuid
	 * @param module
	 *            module
	 * @return statistics object
	 * @throws PersistException
	 *             on persist errors
	 */
	private LciaResultClClassStats createNotExisting( String clid, String ugUuid, String module ) throws PersistException {
		LciaResultClClassStats foo = this.calculateLciaResClClassStats( null, clid, ugUuid, module );
		this.persist( foo );
		return foo;
	}

	/**
	 * Re-calculate statistics for (already managed) object
	 * 
	 * @param foo
	 *            statistics object to re-calculate
	 * @return updated statistics object
	 * @throws MergeException
	 *             on merge errors
	 */
	private LciaResultClClassStats recalculateLciaResClClassStats( LciaResultClClassStats foo ) throws MergeException {
		foo = this.calculateLciaResClClassStats( foo, foo.getClid(), foo.getUgUuid(), foo.getModule() );
		return this.merge( foo );
	}

	/**
	 * Calculate statistics
	 * 
	 * @param objToFill
	 *            existing object to set values for, might be <code>null</code> to create fresh object
	 * @param clid
	 *            class id
	 * @param ugUuid
	 *            unit group uuid
	 * @param module
	 *            module
	 * @return statistics object
	 */
	private LciaResultClClassStats calculateLciaResClClassStats( LciaResultClClassStats objToFill, String clid, String ugUuid, String module ) {
		String sql = "SELECT g.`UUID`, la.`module`, MIN(`value`) AS `min`, MAX(`value`) AS `max`, AVG(`value`) AS `avg`  FROM `lciaresult` l LEFT JOIN `process_lciaresult` pl ON (l.ID=pl.lciaResults_ID) LEFT JOIN `lciaresult_amounts` la ON (l.ID=la.lciaresult_id) LEFT JOIN `globalreference` g ON (g.ID=l.unitgroup_reference) WHERE pl.Process_ID IN( SELECT DISTINCT(p.ID) FROM `process` p LEFT JOIN `process_classifications` pc ON (p.ID=pc.Process_ID) LEFT JOIN `classification_clclass` ccc ON (pc.classifications_ID=ccc.Classification_ID) LEFT JOIN `clclass`cc ON(cc.ID=ccc.classes_ID) WHERE cc.CLID=? ) AND g.`UUID`=? AND la.`module`=? GROUP BY g.`UUID`, la.`module`";

		EntityManager em = PersistenceUtil.getEntityManager();

		Query q = em.createNativeQuery( sql );
		q.setParameter( 1, clid );
		q.setParameter( 2, ugUuid );
		q.setParameter( 3, module );

		try {
			Object[] res = (Object[]) q.getSingleResult();

			LciaResultClClassStats foo = objToFill != null ? objToFill : new LciaResultClClassStats();

			// set values and calc time always
			foo.setMin( ((Number) res[2]).floatValue() );
			foo.setMax( ((Number) res[3]).floatValue() );
			foo.setMean( ((Number) res[4]).floatValue() );

			final long now = System.currentTimeMillis();
			foo.setTsCalculated( now );

			// rest of meta data only for new objects
			if ( objToFill == null ) {
				foo.setClid( clid );
				foo.setUgUuid( (String) res[0] );
				foo.setModule( (String) res[1] );
				foo.setTsLastChange( now );
			}
			return foo;
		} catch ( NoResultException nre ){
			return objToFill;
		}

	}

}
