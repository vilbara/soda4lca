package de.iai.ilcd.model.adapter;

import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ContactDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.FlowPropertyDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.LCIAMethodDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.SourceDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.UnitGroupDataSetVO;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.fzk.iai.ilcd.service.model.IFlowListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodListVO;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.ISourceListVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.iai.ilcd.model.adapter.dataset.ContactVOAdapter;
import de.iai.ilcd.model.adapter.dataset.FlowPropertyVOAdapter;
import de.iai.ilcd.model.adapter.dataset.FlowVOAdapter;
import de.iai.ilcd.model.adapter.dataset.LCIAMethodVOAdapter;
import de.iai.ilcd.model.adapter.dataset.ProcessVOAdapter;
import de.iai.ilcd.model.adapter.dataset.SourceVOAdapter;
import de.iai.ilcd.model.adapter.dataset.UnitGroupVOAdapter;

public class DataSetListAdapter extends DataSetList {

	public DataSetListAdapter( List<? extends IDataSetListVO> list ) {
		this(list, null);
	}

	public DataSetListAdapter( List<? extends IDataSetListVO> list, String language ) {

		List<DataSetVO> lst = this.getDataSet();

		for ( IDataSetListVO dataset : list ) {
			if ( dataset instanceof IProcessListVO ) {
				ProcessDataSetVO d = new ProcessVOAdapter( (IProcessListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof ILCIAMethodListVO ) {
				LCIAMethodDataSetVO d = new LCIAMethodVOAdapter( (ILCIAMethodListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof IFlowListVO ) {
				FlowDataSetVO d = new FlowVOAdapter( (IFlowListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof IFlowPropertyListVO ) {
				FlowPropertyDataSetVO d = new FlowPropertyVOAdapter( (IFlowPropertyListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof IUnitGroupListVO ) {
				UnitGroupDataSetVO d = new UnitGroupVOAdapter( (IUnitGroupListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof ISourceListVO ) {
				SourceDataSetVO d = new SourceVOAdapter( (ISourceListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
			else if ( dataset instanceof IContactListVO ) {
				ContactDataSetVO d = new ContactVOAdapter( (IContactListVO) dataset, language ).getDataSet();
				if ( language != null && d.getName().getValue( language ) != null )
					lst.add( d );
				else if ( language == null )
					lst.add( d );
			}
		}
	}
}
