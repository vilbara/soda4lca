package de.iai.ilcd.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IContactVO;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.contact.Contact;

/**
 * Data access object for {@link Contact contacts}
 */
public class ContactDao extends DataSetDao<Contact, IContactListVO, IContactVO> {

	/**
	 * Create the data access object for {@link Contact contacts}
	 */
	public ContactDao() {
		super( "Contact", Contact.class, IContactListVO.class, IContactVO.class, DataSetType.CONTACT );
	}

	@Override
	protected String getDataStockField() {
		return DatasetTypes.CONTACTS.getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( Contact dataSet ) {
		// Nothing to do for contacts :)
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString ) {
		if ( "email".equals( sortString ) ) {
			return typeAlias + ".email";
		}
		else if ( "www".equals( sortString ) ) {
			return typeAlias + ".www";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

}
