package de.iai.ilcd.model.process;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Transient;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.iai.ilcd.util.lstring.IStringMapProvider;
import de.iai.ilcd.util.lstring.MultiLangStringMapAdapter;

/**
 * Safety margins
 */
@Embeddable
public class SafetyMargins {

	/**
	 * Margins value
	 */
	@Basic
	@Column( name = "margins" )
	private int margins;

	/**
	 * Description
	 */
	@ElementCollection
	@Column( name = "value", columnDefinition = "TEXT" )
	@CollectionTable( name = "process_safetymargins_description", joinColumns = @JoinColumn( name = "process_id" ) )
	@MapKeyColumn( name = "lang" )
	protected final Map<String, String> description = new HashMap<String, String>();

	/**
	 * Adapter for API backwards compatibility.
	 */
	@Transient
	private final MultiLangStringMapAdapter descriptionAdapter = new MultiLangStringMapAdapter( new IStringMapProvider() {

		@Override
		public Map<String, String> getMap() {
			return SafetyMargins.this.description;
		}
	} );

	/**
	 * Get the margins
	 * 
	 * @return margins
	 */
	public int getMargins() {
		return this.margins;
	}

	/**
	 * Set the margins
	 * 
	 * @param margins
	 *            margins to set
	 */
	public void setMargins( int margins ) {
		this.margins = margins;
	}

	/**
	 * Get the description
	 * 
	 * @return description
	 */
	public IMultiLangString getDescription() {
		return this.descriptionAdapter;
	}

	/**
	 * Set the description
	 * 
	 * @param mls
	 *            description to set
	 */
	public void setDescription( IMultiLangString mls ) {
		this.descriptionAdapter.overrideValues( mls );
	}

}
