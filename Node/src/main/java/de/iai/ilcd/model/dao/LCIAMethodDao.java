package de.iai.ilcd.model.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.service.model.ILCIAMethodListVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.common.Uuid;
import de.iai.ilcd.model.flow.ProductFlow;
import de.iai.ilcd.model.lciamethod.LCIAMethod;
import de.iai.ilcd.model.lciamethod.LCIAMethodCharacterisationFactor;

/**
 * Data access object for {@link LCIAMethod LCIA methods}
 */
public class LCIAMethodDao extends DataSetDao<LCIAMethod, ILCIAMethodListVO, ILCIAMethodVO> {

	/**
	 * Create the data access object for {@link LCIAMethod LCIA methods}
	 */
	public LCIAMethodDao() {
		super( "LCIAMethod", LCIAMethod.class, ILCIAMethodListVO.class, ILCIAMethodVO.class, DataSetType.LCIAMETHOD );
	}

	@Override
	protected String getDataStockField() {
		return "lciaMethods";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString ) {
		if ( "type.value".equals( sortString ) ) {
			return typeAlias + ".type";
		}
		else if ( "timeInformation.referenceYear.value".equals( sortString ) ) {
			return typeAlias + ".timeInformation.referenceYear.defaultValue";
		}
		else if ( "timeInformation.duration.value".equals( sortString ) ) {
			return typeAlias + ".timeInformation.duration.defaultValue";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString );
		}
	}

	/**
	 * Link the flow instances in the database with the flow global references of this LCIAMethod
	 * 
	 * @param method
	 *            {@link LCIAMethod}
	 */
	@Override
	protected void preCheckAndPersist( LCIAMethod method ) {
		for ( LCIAMethodCharacterisationFactor cf : method.getCharactarisationFactors() ) {

			if ( cf.getReferencedFlowInstance() != null ) {
				continue; // flow already associated, nothing to do
			}

			GlobalReference flowGlobalReference = cf.getFlowGlobalReference();
			if ( flowGlobalReference == null ) {
				continue; // no global reference available, so no mapping with UUID in this characterisation factor can
				// be tried --> nothing to do
			}

			String flowUuidStr = null;
			Uuid flowUuid = flowGlobalReference.getUuid();
			if ( flowGlobalReference.getUuid() == null ) {
				// sadly no UUID attribute in the global reference
				// but we can try our luck with the URI
				String uriStr = flowGlobalReference.getUri();
				if ( uriStr == null ) {
					continue; // bad luck: no URL :-(
				}
				String[] splittedUri = uriStr.split( "_" );
				if ( splittedUri.length <= 2 ) {
					continue; // URI has not enough parts in order to contain an UUID: bad luck again :-(
				}
				flowUuidStr = splittedUri[splittedUri.length - 2];
			}
			else {
				flowUuidStr = flowUuid.getUuid();
			}
			// all right ... we found ourselves an UUID string, let's try to find the flow for it in the DB
			ProductFlow flowInstance = null;
			try {
				ProductFlowDao flowDao = new ProductFlowDao();
				flowInstance = flowDao.getByUuid( flowUuidStr );

				// if no exception was thrown: congratulations, we found the flow!
				cf.setReferencedFlowInstance( flowInstance );
			}
			catch ( NoResultException ex ) {
				// close, but not close enough. UUID string was found, but no flow
				// with this UUID in the database: bad luck after all :-(
			}
		}
	}

}
