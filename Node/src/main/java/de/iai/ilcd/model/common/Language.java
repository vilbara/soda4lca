package de.iai.ilcd.model.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author clemens.duepmeier
 */
@Entity
@Table( name = "languages" )
public class Language implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	String languageCode;

	String name;

	public Long getId() {
		return this.id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getLanguageCode() {
		return this.languageCode;
	}

	public void setLanguageCode( String languageCode ) {
		this.languageCode = languageCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.languageCode == null) ? 0 : this.languageCode.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( !(obj instanceof Language) ) {
			return false;
		}
		Language other = (Language) obj;
		if ( this.id == null ) {
			if ( other.id != null ) {
				return false;
			}
		}
		else if ( !this.id.equals( other.id ) ) {
			return false;
		}
		if ( this.languageCode == null ) {
			if ( other.languageCode != null ) {
				return false;
			}
		}
		else if ( !this.languageCode.equals( other.languageCode ) ) {
			return false;
		}
		if ( this.name == null ) {
			if ( other.name != null ) {
				return false;
			}
		}
		else if ( !this.name.equals( other.name ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "de.iai.ilcd.model.common.Language[languageCode=" + this.languageCode + "]";
	}

}
