package de.iai.ilcd.model.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.iai.ilcd.model.common.exception.FormatException;
import de.iai.ilcd.util.lstring.IStringMapProvider;
import de.iai.ilcd.util.lstring.MultiLangStringMapAdapter;

/**
 * 
 * @author clemens.duepmeier
 */
@Entity
@Table( name = "globalreference" )
public class GlobalReference implements Serializable, IGlobalReference {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@ElementCollection
	@Column( name = "value", columnDefinition = "TEXT" )
	@CollectionTable( name = "globalreference_shortdescription", joinColumns = @JoinColumn( name = "globalreference_id" ) )
	@MapKeyColumn( name = "lang" )
	protected final Map<String, String> shortDescription = new HashMap<String, String>();

	/**
	 * Adapter for API backwards compatibility.
	 */
	@Transient
	private final MultiLangStringMapAdapter shortDescriptionAdapter = new MultiLangStringMapAdapter( new IStringMapProvider() {

		@Override
		public Map<String, String> getMap() {
			return GlobalReference.this.shortDescription;
		}
	} );

	// private MultiLanguageText shortDescription = new MultiLanguageText();

	@ElementCollection
	@CollectionTable( name = "globalreference_subreferences", joinColumns = @JoinColumn( name = "globalreference_id" ) )
	private List<String> subReferences = new LinkedList<String>();

	@Enumerated( EnumType.STRING )
	private GlobalReferenceTypeValue type;

	@Embedded
	private Uuid uuid;

	DataSetVersion version;

	String uri;

	// URL to objects will be generated automatically
	@Transient
	String href = null;

	public Long getId() {
		return this.id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	@Override
	public GlobalReferenceTypeValue getType() {
		return this.type;
	}

	@Override
	public void setType( GlobalReferenceTypeValue refType ) {
		this.type = refType;
	}

	@Override
	public IMultiLangString getShortDescription() {
		return this.shortDescriptionAdapter;
	}

	public void setShortDescription( IMultiLangString shortDescription ) {
		this.shortDescriptionAdapter.overrideValues( shortDescription );
	}

	public List<String> getSubReferences() {
		return this.subReferences;
	}

	protected void setSubReferences( List<String> subReferences ) {
		this.subReferences = subReferences;
	}

	public void addSubReference( String subReference ) {
		this.subReferences.add( subReference );
	}

	@Override
	public String getUri() {
		return this.uri;
	}

	@Override
	public void setUri( String uri ) {
		this.uri = uri;
	}

	public Uuid getUuid() {
		if ( this.uuid != null ) {
			return this.uuid;
		}
		Uuid uuidFromUri = this.getUuidFromUri();
		if ( uuidFromUri != null ) {
			return uuidFromUri;
		}
		return null;
	}

	public void setUuid( Uuid uuid ) {
		this.uuid = uuid;
	}

	@Override
	public String getRefObjectId() {
		if ( this.uuid != null ) {
			return this.uuid.getUuid();
		}
		Uuid uuidFromUri = this.getUuidFromUri();
		if ( uuidFromUri != null ) {
			return uuidFromUri.getUuid();
		}
		return null;
	}

	private Uuid getUuidFromUri() {
		Uuid uuidFromUri = null;
		if ( this.uri == null ) {
			return null;
		}
		try {
			GlobalRefUriAnalyzer analyzer = new GlobalRefUriAnalyzer( this.uri );
			return analyzer.getUuid();
		}
		catch ( Exception e ) {
			// we do nothing here
		}
		return null;
	}

	@Override
	public void setRefObjectId( String value ) {
		this.uuid = new Uuid( value );
	}

	public DataSetVersion getVersion() {
		return this.version;
	}

	public void setVersion( DataSetVersion version ) {
		this.version = version;
	}

	@Override
	public String getVersionAsString() {
		if ( this.version != null ) {
			return this.version.getVersionString();
		}
		return null;
	}

	@Override
	public void setVersion( String versionString ) {
		DataSetVersion newVersion = null;
		try {
			newVersion = DataSetVersion.parse( versionString );
		}
		catch ( FormatException ex ) {
			// we do nothing here
		}
		if ( newVersion != null ) {
			this.version = newVersion;
		}
	}

	@Override
	public String getHref() {
		// @TODO: include HREF generation code here later
		return this.href;
	}

	public void setHref( String href ) {
		this.href = href;
	}

	@Override
	public String toString() {
		return "de.iai.ilcd.model.common.GlobalReference[id=" + this.id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.uri == null) ? 0 : this.uri.hashCode());
		result = prime * result + ((this.uuid == null) ? 0 : this.uuid.hashCode());
		result = prime * result + ((this.version == null) ? 0 : this.version.hashCode());
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( !(obj instanceof GlobalReference) ) {
			return false;
		}
		GlobalReference other = (GlobalReference) obj;
		if ( this.id == null ) {
			if ( other.id != null ) {
				return false;
			}
		}
		else if ( !this.id.equals( other.id ) ) {
			return false;
		}
		if ( this.uri == null ) {
			if ( other.uri != null ) {
				return false;
			}
		}
		else if ( !this.uri.equals( other.uri ) ) {
			return false;
		}
		if ( this.uuid == null ) {
			if ( other.uuid != null ) {
				return false;
			}
		}
		else if ( !this.uuid.equals( other.uuid ) ) {
			return false;
		}
		if ( this.version == null ) {
			if ( other.version != null ) {
				return false;
			}
		}
		else if ( !this.version.equals( other.version ) ) {
			return false;
		}
		return true;
	}

}
