package de.iai.ilcd.model.process;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Index;

/**
 * Statistics for a classification
 */
@Entity
@Table( name = "process_lciaresult_clclass_statistics" )
public class LciaResultClClassStats {

	/**
	 * ID
	 */
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id" )
	private Long id;

	/**
	 * Classification
	 */
	@Basic
	@Index
	@Column( name = "clid" )
	private String clid;

	/**
	 * UUID of the indicators unit group
	 */
	@Basic
	@Index
	@Column( name = "unitgroup_uuid" )
	private String ugUuid;

	/**
	 * Module
	 */
	@Basic
	@Index
	@Column( name = "module" )
	private String module;

	/**
	 * Minimum value
	 */
	@Basic
	@Column( name = "val_min" )
	private float min;

	/**
	 * Mean value
	 */
	@Basic
	@Column( name = "val_mean" )
	private float mean;

	/**
	 * Maximum value
	 */
	@Basic
	@Column( name = "val_max" )
	private float max;

	/**
	 * Timestamp of calculation
	 */
	@Basic
	@Column( name = "ts_calculated" )
	private long tsCalculated;

	/**
	 * Timestamp of calculation
	 */
	@Basic
	@Column( name = "ts_last_change" )
	private long tsLastChange;

	/**
	 * Get the ID
	 * 
	 * @return ID
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Set the ID
	 * 
	 * @param id
	 *            ID to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * Get the min value
	 * 
	 * @return min value
	 */
	public float getMin() {
		return this.min;
	}

	/**
	 * Set the min value
	 * 
	 * @param min
	 *            value to set
	 */
	public void setMin( float min ) {
		this.min = min;
	}

	/**
	 * Get the mean value
	 * 
	 * @return mean value
	 */
	public float getMean() {
		return this.mean;
	}

	/**
	 * Set the mean value
	 * 
	 * @param mean
	 *            value to set
	 */
	public void setMean( float mean ) {
		this.mean = mean;
	}

	/**
	 * Get the max value
	 * 
	 * @return max value
	 */
	public float getMax() {
		return this.max;
	}

	/**
	 * Set the max value
	 * 
	 * @param max
	 *            value to set
	 */
	public void setMax( float max ) {
		this.max = max;
	}

	/**
	 * Get the time stamp of value calculation
	 * 
	 * @return time stamp of value calculation
	 */
	public long getTsCalculated() {
		return this.tsCalculated;
	}

	/**
	 * Set the time stamp of value calculation
	 * 
	 * @param tsCalculated
	 *            time stamp of value calculation to set
	 */
	public void setTsCalculated( long tsCalculated ) {
		this.tsCalculated = tsCalculated;
	}

	/**
	 * Get the time stamp of last value change
	 * 
	 * @return time stamp of last value change
	 */
	public long getTsLastChange() {
		return this.tsLastChange;
	}

	/**
	 * Set the time stamp of last value change
	 * 
	 * @param tsLastChange
	 *            time stamp of last value change to set
	 */
	public void setTsLastChange( long tsLastChange ) {
		this.tsLastChange = tsLastChange;
	}

	/**
	 * Set the CLID of the stats entry
	 * 
	 * @return CLID of the stats entry
	 */
	public String getClid() {
		return this.clid;
	}

	/**
	 * Set the CLID of the stats entry
	 * 
	 * @param clid
	 *            CLID of the stats entry to set
	 */
	public void setClid( String clid ) {
		this.clid = clid;
	}

	/**
	 * Get the unit group UUID of the stats entry
	 * 
	 * @return unit group UUID of the stats entry
	 */
	public String getUgUuid() {
		return this.ugUuid;
	}

	/**
	 * Set the unit group UUID of the stats entry
	 * 
	 * @param ugUuid
	 *            unit group UUID of the stats entry to set
	 */
	public void setUgUuid( String ugUuid ) {
		this.ugUuid = ugUuid;
	}

	/**
	 * Get the module of the stats entry
	 * 
	 * @return module of the stats entry
	 */
	public String getModule() {
		return this.module;
	}

	/**
	 * Set the module of the stats entry
	 * 
	 * @param module
	 *            module of the stats entry to set
	 */
	public void setModule( String module ) {
		this.module = module;
	}

}
