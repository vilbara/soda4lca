package de.iai.ilcd.model.adapter;

import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;

/**
 * Adapter for global reference
 */
public class GlobalReferenceAdapter extends GlobalReferenceType {

	/**
	 * Create the adapter.
	 * 
	 * @param adaptee
	 *            instance to adapt
	 */
	public GlobalReferenceAdapter( IGlobalReference reference, String language ) {
		if ( reference != null ) {
			this.setHref( reference.getHref() );
			this.setType( reference.getType() );
			this.setUri( reference.getUri() );
			this.setVersion( reference.getVersionAsString() );

			this.setShortDescription( new LStringAdapter( reference.getShortDescription(), language ).getLStrings() );
		}
	}

	/**
	 * Copy global references via adapter
	 * 
	 * @param src
	 *            source list
	 * @param dst
	 *            destination list
	 */
	public static void copyGlobalReferences( List<IGlobalReference> src, List<IGlobalReference> dst, String language ) {
		if ( src != null && dst != null ) {
			for ( IGlobalReference srcItem : src ) {
				dst.add( new GlobalReferenceAdapter( srcItem, language ) );
			}
		}
	}
}
