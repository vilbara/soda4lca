package de.iai.ilcd.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.flowproperty.FlowProperty;
import de.iai.ilcd.model.unitgroup.UnitGroup;

/**
 * Data access object for {@link FlowProperty flow properties}
 */
public class FlowPropertyDao extends DataSetDao<FlowProperty, IFlowPropertyListVO, IFlowPropertyVO> {

	/**
	 * Create the data access object for {@link FlowProperty flow properties}
	 */
	public FlowPropertyDao() {
		super( "FlowProperty", FlowProperty.class, IFlowPropertyListVO.class, IFlowPropertyVO.class, DataSetType.FLOWPROPERTY );
	}

	@Override
	protected String getDataStockField() {
		return "flowProperties";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( FlowProperty dataSet ) {
		this.attachUnitGroup( dataSet );
	}

	/**
	 * Get flow property by UUID. Overrides {@link DataSetDao} implementation because
	 * {@link #attachUnitGroup(FlowProperty)} is called after loading.
	 */
	@Override
	public FlowProperty getByUuid( String uuid ) {
		FlowProperty fp = super.getByUuid( uuid );
		if ( fp != null ) {
			this.attachUnitGroup( fp );
		}
		return fp;
	}

	private void attachUnitGroup( FlowProperty flowprop ) {
		UnitGroupDao unitGroupDao = new UnitGroupDao();
		if ( flowprop != null && flowprop.getReferenceToUnitGroup() != null ) {
			String uuid = flowprop.getReferenceToUnitGroup().getUuid().getUuid();
			UnitGroup unitGroup = unitGroupDao.getByUuid( uuid );
			flowprop.setUnitGroup( unitGroup );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString ) {
		if ( "unitGroupName.value".equals( sortString ) ) {
			return typeAlias + ".unitGroup.name.value";
		}
		else if ( "defaultUnit".equals( sortString ) ) {
			return typeAlias + ".unitGroup.referenceUnit.name";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

}
