package de.iai.ilcd.model.process;

/**
 * Sub type for processes
 */
public enum ProcessSubType {

	/**
	 * generic data set
	 */
	GENERIC_DATASET( "generic dataset" ),

	/**
	 * representative data set
	 */
	REPRESENTATIVE_DATASET( "representative dataset" ),

	/**
	 * average data set
	 */
	AVARAGE_DATASET( "average dataset" ),

	/**
	 * specific data set
	 */
	SPECIFIC_DATASET( "specific dataset" );

	/**
	 * Value
	 */
	private final String value;

	/**
	 * Create process sub type
	 * 
	 * @param value
	 *            value of type
	 */
	private ProcessSubType( String value ) {
		this.value = value;
	}

	/**
	 * Get the value
	 * 
	 * @return value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Get the sub type instance from value
	 * 
	 * @param val
	 *            value
	 * @return sub type instance from value (or <code>null</code> if unmatched)
	 */
	public static ProcessSubType fromValue( String val ) {
		for ( ProcessSubType tmp : ProcessSubType.values() ) {
			if ( tmp.value.equals( val ) ) {
				return tmp;
			}
		}
		return null;
	}

}
