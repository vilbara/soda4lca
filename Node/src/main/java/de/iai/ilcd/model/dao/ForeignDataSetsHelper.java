package de.iai.ilcd.model.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.velocity.tools.generic.ValueParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import de.fzk.iai.ilcd.service.client.DatasetNotFoundException;
import de.fzk.iai.ilcd.service.client.FailedAuthenticationException;
import de.fzk.iai.ilcd.service.client.FailedConnectionException;
import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.fzk.iai.ilcd.service.model.IDataSetListVO;
import de.fzk.iai.ilcd.service.model.IDataSetVO;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.nodes.NetworkNode;
import de.iai.ilcd.model.utils.DistributedSearchLog;

/**
 * 
 * @author clemens.duepmeier
 */
public class ForeignDataSetsHelper {

	private static final String REST_SERVLET_PREFIX = "resource/";

	private static final Logger logger = LoggerFactory.getLogger( ForeignDataSetsHelper.class );

	public <T extends IDataSetListVO> List<T> foreignSearch( Class<T> searchResultClassType, ValueParser params, DistributedSearchLog log ) {
		logger.debug( "searching foreign nodes" );

		List<T> results = new ArrayList<T>();

		MultivaluedMap<String, String> paramMap = new MultivaluedMapImpl();
		for ( Entry<String, Object> entry : params.entrySet() ) {
			String key = entry.getKey();
			if ( key.equals( "distributed" ) ) {
				continue;
			}
			String[] values = params.getStrings( key );
			if ( values != null && values.length > 0 ) {
				List<String> valueList = new ArrayList<String>();
				for ( String value : values ) {
					valueList.add( value );
				}
				paramMap.put( key, valueList );
			}
			else {
				String singleValue = params.getString( key );
				if ( singleValue != null && !singleValue.isEmpty() ) {
					paramMap.putSingle( key, singleValue );
				}
			}

		}

		NetworkNodeDao nodeDao = new NetworkNodeDao();
		List<NetworkNode> nodes = null;
		if ( params.getBoolean( "virtual" ) == null || Boolean.FALSE.equals( params.getBoolean( "virtual" ) ) ) {
			nodes = nodeDao.getRemoteNetworkNodesFromRegistry( params.getString( "registeredIn" ) );
		}
		else {
			nodes = nodeDao.getRemoteNetworkNodes();
		}

		logger.info( "querying {} foreign nodes", nodes.size() );

		logger.debug( "param  keys:  {}", paramMap.keySet() );
		logger.debug( "param values: {}", paramMap.values() );

		ForeignDataSetsQueryController<T> threadControl = new ForeignDataSetsQueryController<T>();
		threadControl.setResults( results );
		threadControl.setLog( log );
		threadControl.setSearchResultClassType( searchResultClassType );
		threadControl.setParamMap( paramMap );

		for ( NetworkNode node : nodes ) {

			logger.debug( "querying node {}", node.getBaseUrl() );

			String baseUrl = node.getBaseUrl();
			if ( !node.getBaseUrl().endsWith( "/" ) ) {
				baseUrl = baseUrl + "/";
			}
			baseUrl += REST_SERVLET_PREFIX;

			threadControl.registerThread( node, baseUrl, searchResultClassType, paramMap );
		}

		threadControl.runThreads();
		threadControl.doWait( ConfigurationService.INSTANCE.getSearchDistTimeout() );

		return results;
	}

	public <T extends IDataSetVO> T getForeignDataSet( Class<T> dataSetClassType, String nodeShortName, String uuid, Long registryId ) {

		// we need node name and uuid
		if ( nodeShortName == null || uuid == null ) {
			return null;
		}
		logger.info( "get foreign process from node {} with uuid {}", nodeShortName, uuid );
		NetworkNodeDao nodeDao = new NetworkNodeDao();
		NetworkNode node = nodeDao.getNetworkNode( nodeShortName, registryId );
		if ( node == null ) {
			return null;
		}

		String baseUrl = node.getBaseUrl();
		if ( !node.getBaseUrl().endsWith( "/" ) ) {
			baseUrl = baseUrl + "/";
		}
		baseUrl += REST_SERVLET_PREFIX;

		T dataSet = null;

		try {
			ILCDNetworkClient targetConnection = new ILCDNetworkClient( baseUrl );
			dataSet = (T) targetConnection.getDataSetVO( dataSetClassType, uuid );
			// attach node information
			// if (dataSet.getSourceId() == null)
			dataSet.setSourceId( node.getNodeId() );
			dataSet.setHref( baseUrl );
			// process = new Process(poProcess);
		}
		catch ( FailedConnectionException ex ) {
			logger.error( "Connection to {} failed", baseUrl, ex );
			return null;
		}
		catch ( FailedAuthenticationException ex ) {
			logger.error( "Authentication to {} failed", baseUrl, ex );
			return null;
		}
		catch ( IOException ex ) {
			logger.error( "There were some I/O-errors accessing a dataset from {}", baseUrl, ex );
		}
		catch ( DatasetNotFoundException ex ) {
			logger.error( "Dataset with uuid {} not found", uuid, ex );
			return null;
		}
		catch ( ILCDServiceClientException e ) {
			logger.error( "other error", e );
		}

		return dataSet;
	}
}
