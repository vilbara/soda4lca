package de.iai.ilcd.model.adapter.dataset;

import java.util.Set;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ProcessDataSetVO;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.AccessInformationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.LCIMethodInformationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.process.TimeInformationType;
import de.fzk.iai.ilcd.service.model.IProcessListVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.fzk.iai.ilcd.service.model.process.ILCIMethodInformation;
import de.iai.ilcd.model.adapter.ComplianceSystemAdapter;
import de.iai.ilcd.model.adapter.GlobalReferenceAdapter;
import de.iai.ilcd.model.adapter.LStringAdapter;
import de.iai.ilcd.model.adapter.QuantitativeReferenceAdapter;
import de.iai.ilcd.model.adapter.ReviewAdapter;

/**
 * Adapter for Processes
 */
public class ProcessVOAdapter extends AbstractDatasetAdapter<ProcessDataSetVO, IProcessListVO, IProcessVO> {

	/**
	 * Create adapter for {@link IProcessListVO process list value object}
	 * 
	 * @param adaptee
	 *            list value object to adapt
	 */
	public ProcessVOAdapter( IProcessListVO adaptee ) {
		super( new ProcessDataSetVO(), adaptee );
	}

	/**
	 * Create adapter for the {@link IProcessVO value object}
	 * 
	 * @param adaptee
	 *            value object to adapt
	 */
	public ProcessVOAdapter( IProcessVO adaptee ) {
		super( new ProcessDataSetVO(), adaptee );
	}

	/**
	 * Create adapter for {@link IProcessListVO process list value object} with language filtering
	 * 
	 * @param adaptee
	 *            list value object to adapt
	 */
	public ProcessVOAdapter( IProcessListVO adaptee, String language ) {
		super( new ProcessDataSetVO(), adaptee, language );
	}

	/**
	 * Create adapter for the {@link IProcessVO value object} with language filtering
	 * 
	 * @param adaptee
	 *            value object to adapt
	 */
	public ProcessVOAdapter( IProcessVO adaptee, String language ) {
		super( new ProcessDataSetVO(), adaptee, language );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void copyValues( IProcessListVO src, ProcessDataSetVO dst ) {
		AccessInformationType accessInformation = new AccessInformationType();
		LStringAdapter.copyLStrings( src.getAccessInformation().getUseRestrictions(), accessInformation.getUseRestrictions(), this.language );
		accessInformation.setLicenseType( src.getAccessInformation().getLicenseType() );
		dst.setAccessInformation( accessInformation );
		dst.setContainsProductModel( src.getContainsProductModel() );

		TimeInformationType timeInformation = new TimeInformationType();
		timeInformation.setReferenceYear( src.getTimeInformation().getReferenceYear() );
		timeInformation.setValidUntil( src.getTimeInformation().getValidUntil() );
		dst.setTimeInformation( timeInformation );

		dst.setType( src.getType() );

		ILCIMethodInformation i = src.getLCIMethodInformation();
		if ( i != null ) {
			LCIMethodInformationType lciMethodInformation = new LCIMethodInformationType();
			lciMethodInformation.setMethodPrinciple( i.getMethodPrinciple() );
			lciMethodInformation.getApproaches().addAll( i.getApproaches() );
			dst.setLCIMethodInformation( lciMethodInformation );
		}

		dst.setLocation( src.getLocation() );

		Set<IComplianceSystem> srcComplSystems = src.getComplianceSystems();
		if ( srcComplSystems != null ) {
			Set<IComplianceSystem> destComplSystems = dst.getComplianceSystems();
			for ( IComplianceSystem compSys : srcComplSystems ) {
				destComplSystems.add( new ComplianceSystemAdapter( compSys, this.language ) );
			}
		}

		dst.setOverallQuality( src.getOverallQuality() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void copyValues( IProcessVO src, ProcessDataSetVO dst ) {
		this.copyValues( (IProcessListVO) src, dst );

		dst.setApprovedBy( new GlobalReferenceAdapter( src.getApprovedBy(), this.language ) );

		LStringAdapter.copyLStrings( src.getBaseName(), dst.getBaseName(), this.language );

		dst.setCompletenessProductModel( src.getCompletenessProductModel() );

		dst.setFormat( src.getFormat() );

		dst.setOwnerReference( new GlobalReferenceAdapter( src.getOwnerReference(), this.language ) );

		dst.setQuantitativeReference( new QuantitativeReferenceAdapter( src.getQuantitativeReference(), this.language ) );

		ReviewAdapter.copyReviews( src.getReviews(), dst.getReviews(), this.language );

		LStringAdapter.copyLStrings( src.getSynonyms(), dst.getSynonyms(), this.language );
		LStringAdapter.copyLStrings( src.getTechnicalPurpose(), dst.getTechnicalPurpose(), this.language );
		LStringAdapter.copyLStrings( src.getUseAdvice(), dst.getUseAdvice(), this.language );
	}

}
