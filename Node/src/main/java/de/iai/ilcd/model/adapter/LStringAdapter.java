package de.iai.ilcd.model.adapter;

import java.util.ArrayList;
import java.util.List;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.model.common.ILString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

/**
 * Adapter for localized strings
 */
public class LStringAdapter {

	/**
	 * The localized strings
	 */
	protected List<LString> lStrings = new ArrayList<LString>();

	/**
	 * Create the adapter
	 * 
	 * @param mls
	 *            instance to adapt
	 */
	// public LStringAdapter( IMultiLangString mls ) {
	// if ( mls != null ) {
	// for ( ILString s : mls.getLStrings() ) {
	// lStrings.add( new LString( s.getLang(), s.getValue() ) );
	// }
	// }
	// }

	/**
	 * Create the adapter with language filtering
	 * 
	 * @param mls
	 *            instance to adapt
	 */
	public LStringAdapter( IMultiLangString mls, String language ) {
		if ( mls != null ) {
			for ( ILString s : mls.getLStrings() ) {
				if ( s.getLang().equals( language ) )
					lStrings.add( new LString( s.getLang(), s.getValue() ) );
			}
		}
	}

	/**
	 * Get the localized strings
	 * 
	 * @return localized strings
	 */
	public List<LString> getLStrings() {
		return lStrings;
	}

	/**
	 * Copy all localized strings
	 * 
	 * @param src
	 *            source {@link IMultiLangString} instance
	 * @param dst
	 *            destination {@link IMultiLangString} instance
	 */
	// public static void copyLStrings( IMultiLangString src, IMultiLangString dst ) {
	// copyLStrings( src, dst, null );
	// }

	/**
	 * Copy all localized strings, filtering for language
	 * 
	 * @param src
	 *            source {@link IMultiLangString} instance
	 * @param dst
	 *            destination {@link IMultiLangString} instance
	 */
	public static void copyLStrings( IMultiLangString src, IMultiLangString dst, String language ) {
		if ( src != null && dst != null ) {
			for ( ILString s : src.getLStrings() ) {
				if ( language == null )
					dst.setValue( s.getLang(), s.getValue() );
				else if ( s.getLang().equals( language ) )
					dst.setValue( s.getLang(), s.getValue() );
			}
		}
	}
}
