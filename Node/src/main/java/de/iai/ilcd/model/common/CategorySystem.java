package de.iai.ilcd.model.common;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author clemens.duepmeier
 */
@Entity
@Table( name = "categorysystem" )
public class CategorySystem implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@Column( name = "csname" )
	private String name;

	@OneToMany( cascade = CascadeType.ALL )
	private Set<Category> categories = new HashSet<Category>();

	protected CategorySystem() {
	}

	public CategorySystem( String name ) {
		this.name = name;
	}

	public Long getId() {
		return this.id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Set<Category> getCategories() {
		return this.categories;
	}

	protected void setCategories( Set<Category> categories ) {
		this.categories = categories;
	}

	public void addCategory( Category cat ) {
		if ( !this.categories.contains( cat ) ) {
			this.categories.add( cat );
		}
	}

	public void removeCategory( Category cat ) {
		if ( this.categories.contains( cat ) ) {
			this.categories.remove( cat );
		}
	}

	public String getName() {
		return this.name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( !(obj instanceof CategorySystem) ) {
			return false;
		}
		CategorySystem other = (CategorySystem) obj;
		if ( this.id == null ) {
			if ( other.id != null ) {
				return false;
			}
		}
		else if ( !this.id.equals( other.id ) ) {
			return false;
		}
		if ( this.name == null ) {
			if ( other.name != null ) {
				return false;
			}
		}
		else if ( !this.name.equals( other.name ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "de.iai.ilcd.model.common.ClassificationSystem[name=" + this.name + "]";
	}

}
