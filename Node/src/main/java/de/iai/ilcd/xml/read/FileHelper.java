package de.iai.ilcd.xml.read;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;

/**
 * 
 * @author clemens.duepmeier
 */
public class FileHelper {

	public static String convertFileToString( String fileName ) throws FileNotFoundException, IOException {
		TFileInputStream inputStream = new TFileInputStream( fileName );
		return convertStreamToString( inputStream );
	}

	public static String convertFileToString( TFile file ) throws FileNotFoundException, IOException {
		TFileInputStream inputStream = new TFileInputStream( file );
		return convertStreamToString( inputStream );
	}

	public static String convertStreamToString( InputStream is ) throws IOException {
		BufferedReader reader = new BufferedReader( new InputStreamReader( is ) );
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ( (line = reader.readLine()) != null ) {
			sb.append( line ).append( "\n" );
		}
		is.close();
		return sb.toString();
	}

}
