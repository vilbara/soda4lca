package edu.kit.soda4lca.test.ui.main;

import java.io.File;
import java.util.Properties;

import org.junit.After;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.AfterTest;

public class TestContext {

	// variables for the program
	public static String site = "http://localhost:8080/Node/";

	public static String siteReg = "http://localhost:8080/Registry/";

	public static String datafilepath = "/sample_data.zip";

	public static String language = "en"; // en for english, de for deutsch

	public static int timeout = 10; // maximum seconds to wait for a site to load

	public static int wait = 100; // millis to wait between entering 2 text

	// open language properties
	public static Properties lang = TestFunctions.propOpen( language );

	// start firefox
	private FirefoxDriver ff = null;

	private static TestContext _instance = null;

	public static TestContext getInstance() {
		if ( _instance == null )
			_instance = new TestContext();
		return _instance;
	}

	public TestContext() {
		FirefoxProfile profile = new FirefoxProfile();
		// set the language
		profile.setPreference( "intl.accept_languages", language );
		// for export disable download dialog
		profile.setPreference( "browser.download.folderList", 2 );
		profile.setPreference( "browser.download.manager.showWhenStarting", false );
		profile.setPreference( "browser.download.panel.shown", false );
		// get the current project folder
		String path = getClass().getClassLoader().getResource( "." ).getPath() + "tmp/";
		// if we are on a windows maschine, the first / before the C:/ should be removed
		if ( File.separator.equals( "\\" ) )
			path = path.substring( 1 );
		path = path.replace( "/", java.io.File.separator );
		profile.setPreference( "browser.download.dir", path );
		profile.setPreference( "browser.helperApps.neverAsk.saveToDisk", "application/zip" );
		this.ff = new FirefoxDriver( profile );
	}

	public FirefoxDriver getDriver() {
		return this.ff;
	}

	@AfterTest
	public void CloseFirefox() {
		TestContext.getInstance().getDriver().close();
		System.out.println( "AfterTest" );
	}

	@After
	public void CloseFirefoxJUnit() {
		TestContext.getInstance().getDriver().close();
		System.out.println( "Simply After" );
	}
}
