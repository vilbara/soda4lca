package edu.kit.soda4lca.test.ui.admin;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Test Groups menu. Add new groups, then delete one. Add users to groups
 * 
 * @author mark.szabo
 * 
 */
public class T013GroupsTest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T013GroupsTest.class );

	/**
	 * Add new group
	 * 
	 * @param name
	 *            Group name
	 * @param organization
	 *            Organization
	 * @throws InterruptedException
	 */
	public void newGroup( String name, String organization ) throws InterruptedException {
		log.trace( "Adding group " + name );
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );

		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) ) ).build().perform();
		// Mouse over the submenu 'New Group'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.group.new" ) ) ) ).click().build()
				.perform();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.group.createGroup" ) + "')]" ) ) );
		// fill in the form
		// name
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='groupName']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='groupName']" ) ).sendKeys( name );
		// organization
		// click on the list
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='org_label']" ) ).click();
		Thread.sleep( TestContext.wait );
		// search for the specified element
		int i = 1;
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='org_panel']/div/ul/li[" + i + "]" ) ).getText().contains( organization ) )
			i++;
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='org_panel']/div/ul/li[" + i + "]" ) ).click();
		Thread.sleep( TestContext.wait );
		// Click 'Create a Group'
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.group.createGroup" ) + "')]" ) ).click();
		Thread.sleep( TestContext.wait * 2 );

		// check if any warning message shows up
		TestFunctions.checkNoMessage();
		Thread.sleep( TestContext.wait * 5 );
		// check, if the new site is manageGroupList.xhtml or not
		for ( i = 0; i < TestContext.timeout * 10 && !TestContext.getInstance().getDriver().getCurrentUrl().contains( "manageGroupList.xhtml" ); i++ )
			Thread.sleep( 100 );
		if ( !TestContext.getInstance().getDriver().getCurrentUrl().contains( "manageGroupList.xhtml" ) )
			org.testng.Assert.fail( "After adding a new group, it doesn't go to manageGroupList.xhtml but goes to "
					+ TestContext.getInstance().getDriver().getCurrentUrl() );

		log.trace( "Group " + name + " added succesfull" );
	}

	/**
	 * Add users to an existing group
	 * 
	 * @param group
	 *            Group name
	 * @param users
	 *            Name of users to add - array of String
	 * @param add
	 *            boolean - if add user: true, if remove user from group: false
	 * @throws InterruptedException
	 */
	public void addUserToGroup( String group, String[] users, boolean add ) throws InterruptedException {
		log.trace( "Adding users to group " + group + " started" );

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );
		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) ) ).build().perform();
		// Mouse over and click the submenu 'Manage Group'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.group.manageList" ) ) ) ).click().build()
				.perform();
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );
		// click the group
		TestFunctions.findAndWaitOnElement( By.linkText( group ) ).click();
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );
		// select users one by one
		int addOrDelButtonPosition = 3;
		String addOrDelListXPath = ".//*[@id='generalForm']//*[preceding-sibling::div[text()='" + TestContext.lang.getProperty( "admin.picklist.targetUsers" )
				+ "']]";
		if ( add ) {
			addOrDelButtonPosition = 1;
			addOrDelListXPath = ".//*[@id='generalForm']//*[preceding-sibling::div[text()='"
					+ TestContext.lang.getProperty( "admin.picklist.sourceUsers" ) + "']]";
		}
		for ( String user : users ) {
			int i = 1;
			while ( !TestFunctions.findAndWaitOnElement( By.xpath( addOrDelListXPath + "/li[" + i + "]" ) )
					.getText().equals( user ) )
				i++;
			TestFunctions.findAndWaitOnElement( By.xpath( addOrDelListXPath + "/li[" + i + "]" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='generalForm']//button[" + addOrDelButtonPosition + "]" ) ).click();
			Thread.sleep( 2 * TestContext.wait );
		}
		Thread.sleep( 2 * TestContext.wait );
		// Click 'Change Group'
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.group.changeGroup" ) + "')]" ) ).click();
		// wait for the message
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.xpath( ".//*[@id='messages']" ) ) );
		// check the message
		if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
				TestContext.lang.getProperty( "facesMsg.group.changeSuccess" ) ) )
			org.testng.Assert.fail( "Error: " + TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText() );
		// refresh page
		TestContext.getInstance().getDriver().navigate().refresh();
		log.trace( "Adding users to group " + group + " finished succesfull" );
	}

	/**
	 * Add new groups
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 131, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T012UsersTest.manageUsers" } )
	public void newGroups() throws InterruptedException {
		// add new groups
		log.info( "'Add new groups' test started" );

		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );

		newGroup( "Group1", "Organization1" );
		newGroup( "Group2", "Organization1" );
		newGroup( "Group3", "Organization2" );
		newGroup( "Group4", "Organization1" );
		newGroup( "Test Group to Delete", "Organization1" );

		log.info( "'Add new groups' test finished" );
	}

	/**
	 * Manage groups: delete a group, add some users to other groups
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 132, dependsOnMethods = { "newGroups" } )
	public void manageGroups() throws InterruptedException {
		// manage groups
		log.info( "'Manage groups' test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );

		log.debug( "Delete a group" );
		// DELETE A GROUP
		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) ) ).build().perform();
		// Mouse over and click the submenu 'Manage Group'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.group.manageList" ) ) ) ).click().build()
				.perform();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );
		// find 'Test Group to Delete'
		int i = 1;
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='groupTable_data']/tr[" + Integer.toString( i ) + "]/td[2]/a" ) ).getText().contains(
				"Test Group to Delete" ) )
			i++;
		// select it
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='groupTable_data']/tr[" + Integer.toString( i ) + "]/td[1]/div/div[2]" ) ).click();
		// wait for the clientside script
		Thread.sleep( TestContext.wait );

		// Click 'Delete selected entries'
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='deleteBtn']" ) ).click();
		// Are you sure? - Click OK
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.ok" ) + "')]" ) ).click();
		// Check the message
		if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
				TestContext.lang.getProperty( "facesMsg.removeSuccess" ).substring( 0, 10 ) ) )
			org.testng.Assert.fail( "Wrong message: " + TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText() );
		log.debug( "Group deleted succesfull" );

		log.debug( "Add users to groups" );
		// ADD USERS TO GROUPS
		String[] users = { "Admin1", "User1", "User3" };
		addUserToGroup( "Group1", users, true );
		String[] users2 = { "Admin2", "User1", "User2", "User3" };
		addUserToGroup( "Group2", users2, true );
		// delete someone from a group
		String[] users3 = { "User3" };
		addUserToGroup( "Group2", users3, false );
		log.debug( "Users added to groups succesfull" );
		log.info( "'Manage groups' test finished" );
	}

}
