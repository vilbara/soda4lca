package edu.kit.soda4lca.test.ui.admin;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Add some users, then delete one and change an other one
 * 
 * @author mark.szabo
 * 
 */
public class T012UsersTest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T012UsersTest.class );

	/**
	 * Add user
	 * 
	 * @param user
	 *            Username
	 * @param email
	 *            email
	 * @param password
	 *            password
	 * @param title
	 *            title (Mr, Mrs, Dr)
	 * @param firstname
	 *            First name
	 * @param surname
	 *            Last name
	 * @param job
	 *            Professional Position
	 * @param gender
	 *            Gender, boolean: true-male, false-female
	 * @param admin
	 *            Super admin right, boolean
	 * @param Organization
	 *            Organization
	 * @throws InterruptedException
	 */
	public void createNewUser( String user, String email, String password, String title, String firstname, String surname, String job, boolean gender,
			boolean admin, String Organization ) throws InterruptedException {
		log.trace( "Adding user " + user );

		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) ) ).build()
				.perform();
		// Mouse over the submenu 'New User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user.new" ) ) ) ).click()
				.build().perform();

		// wait for the site to load + locate Create user button
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.user.createUser" ) + "')]" ) ) );

		// FILL IN THE FORM
		// Name
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='nameIn']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='nameIn']" ) ).sendKeys( user );
		Thread.sleep( TestContext.wait );
		// email
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='email']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='email']" ) ).sendKeys( email );
		Thread.sleep( TestContext.wait );
		// password
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='passw']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='passw']" ) ).sendKeys( password );
		Thread.sleep( TestContext.wait );
		// password again
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='repeatPassw']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='repeatPassw']" ) ).sendKeys( password );
		Thread.sleep( TestContext.wait );
		// title
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='title']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='title']" ) ).sendKeys( title );
		Thread.sleep( TestContext.wait );
		// first name
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='firstName']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='firstName']" ) ).sendKeys( firstname );
		Thread.sleep( TestContext.wait );
		// last name
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='lastName']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='lastName']" ) ).sendKeys( surname );
		Thread.sleep( TestContext.wait );
		// Professional Position
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='jobposition']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='jobposition']" ) ).sendKeys( job );
		Thread.sleep( TestContext.wait );
		// gender //true-male, false-female
		if ( gender )
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='gender']/tbody/tr/td[3]/div/div[2]" ) ).click();
		else
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='gender']/tbody/tr/td[1]/div/div[2]" ) ).click();
		Thread.sleep( TestContext.wait );
		// comments - textarea
		TestFunctions.findAndWaitOnElement( By.id( "dspurpose" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.id( "dspurpose" ) ).sendKeys( "Just for test" );
		Thread.sleep( TestContext.wait );
		// access right
		if ( admin )
			TestFunctions.findAndWaitOnCheckBox( TestContext.lang.getProperty( "admin.user.accessRight.superadmin" ) ).click();
		// Organization
		// click on the list
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='orgSelect_label']" ) ).click();
		Thread.sleep( TestContext.wait );
		// search for the specified element
		int i = 1;
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='orgSelect_panel']/div/ul/li[" + i + "]" ) ).getText().contains( Organization ) )
			i++;
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='orgSelect_panel']/div/ul/li[" + i + "]" ) ).click();
		Thread.sleep( TestContext.wait );
		// Country
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='country_label']" ) ).click();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='country_panel']/div/ul/li[99]" ) ).click();
		Thread.sleep( TestContext.wait );
		// Zip code
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='zipCode']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='zipCode']" ) ).sendKeys( "1111" );
		Thread.sleep( TestContext.wait );
		// City
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='city']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='city']" ) ).sendKeys( "Budapest" );
		Thread.sleep( TestContext.wait );
		// Street address - textarea
		TestFunctions.findAndWaitOnElement( By.id( "streetAddr" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.id( "streetAddr" ) ).sendKeys( " Irinyi u. 1-17." );
		Thread.sleep( TestContext.wait );
		// Click Create user
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.user.createUser" ) + "')]" ) ).click();
		// wait for the validator script (tests username, email etc.)
		Thread.sleep( TestContext.wait * 10 );
		// check for error message
		TestFunctions.checkNoMessage();
		// check the url, it should be manageUserList.xhtml
		if ( !TestContext.getInstance().getDriver().getCurrentUrl().contains( "manageUserList.xhtml" ) )
			org.testng.Assert.fail( "After adding a new user, it doesn't go to manageUserList.xhtml but goes to "
					+ TestContext.getInstance().getDriver().getCurrentUrl() );

		log.trace( "User " + user + " added succesfull" );
	}

	/**
	 * Add new users
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 121, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T011OrganisationsTest.manageOrganisations" } )
	public void newUsers() throws InterruptedException {
		// create some new user
		log.info( "'Create new users' test started" );

		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.linkText( TestContext.lang.getProperty( "admin.globalConfig" ) ) ) );

		// add normal users
		createNewUser( "User1", "john@iai.kit.edu", "s3cr3t", "Mr", "John", "Smith", "junior java developer", true, false, "Organization1" );
		createNewUser( "User2", "peter@iai.kit.edu", "s3cr3t", "Mr", "Peter", "Tailor", "java tester", true, false, "Organization1" );
		createNewUser( "User3", "whatever@iai.kit.edu", "s3cr3t", "Mr", "What", "Ever", "humorist", true, false, "Organization1" );
		createNewUser( "User4", "sara@iai.kit.edu", "s3cr3t", "Mrs", "Sara", "Smith", "junior java developer", false, false, "Organization1" );
		createNewUser( "User5", "alice@iai.kit.edu", "s3cr3t", "Ms", "Alice", "Regenkurt", "junior java developer", false, false, "Organization1" );
		createNewUser( "User6", "ceo@iai.kit.edu", "s3cr3t", "Dr", "Alexander", "Alexandrius", "senior java developer", true, false, "Organization2" );
		createNewUser( "User7", "noidea@iai.kit.edu", "s3cr3t", "Mr", "Idea", "No", "I've no idea what doing here", true, false, "Organization2" );
		createNewUser( "DeleteTestUser", "noidea2@iai.kit.edu", "s3cr3t", "M", "Idea", "No", "don't fire me!!!", true, false, "Organization3" );

		// add some users with admin rights
		createNewUser( "Admin1", "admin@iai.kit.edu", "s3cr3t", "Mr", "John", "Root", "admin", true, true, "Organization1" );
		createNewUser( "Admin2", "webmaster@iai.kit.edu", "s3cr3t", "Mr", "Java", "Script", "webmaster", true, true, "Organization1" );
		createNewUser( "Admin3", "designer@iai.kit.edu", "s3cr3t", "Ms", "Kathy", "Artist", "designer", true, true, "Organization2" );

		log.info( "'Create new users' test finished" );
	}

	/**
	 * Manage users. First delete one (named: DeleteTestUser), then change the zip code of an other one (named User1)
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 122, dependsOnMethods = { "newUsers" } )
	public void manageUsers() throws InterruptedException {
		// manage users
		log.info( "'Manage users - delete and change users' test started" );

		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.linkText( TestContext.lang.getProperty( "admin.globalConfig" ) ) ) );

		log.debug( "Delete user" );
		// DELETE A USER
		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) ) ).build()
				.perform();
		// Mouse over the submenu 'Manage Users'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.user.manageList" ) ) ) )
				.click().build().perform();
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.linkText( TestContext.lang.getProperty( "public.user.logout" ) ) ) );
		int i = 1;
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='userTable_data']/tr[" + Integer.toString( i ) + "]/td[2]/a" ) ).getText().contains(
				"DeleteTestUser" ) )
			i++;
		// select it
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='userTable_data']/tr[" + Integer.toString( i ) + "]/td[1]/div/div[2]" ) ).click();
		// wait for the clientside script
		Thread.sleep( 2 * TestContext.wait );
		// Click 'Delete selected entries'
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='deleteBtn']" ) ).click();
		// Are you sure? - Click OK
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.ok" ) + "')]" ) ).click();
		// Check the message
		if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
				TestContext.lang.getProperty( "facesMsg.removeSuccess" ).substring( 0, 10 ) ) )
			org.testng.Assert.fail( "Wrong message: " + TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText() );
		log.debug( "User deleted succesfull" );

		log.debug( "Change a zipCode of a user" );
		// CHANGE USER INFORMATION
		// Click on TestUser
		TestFunctions.findAndWaitOnElement( By.linkText( "User1" ) ).click();
		// Change ZIP Code
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='zipCode']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='zipCode']" ) ).sendKeys( "1234" );
		// Click Change user information
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.user.changeUserInfo" ) + "')]" ) ).click();
		// wait for a message
		Thread.sleep( TestContext.wait * 10 );
		// Check the message
		if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
				TestContext.lang.getProperty( "facesMsg.user.accountChanged" ) ) )
			org.testng.Assert.fail( "Wrong message: " + TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText() );
		log.debug( "zipCode of a user changed succesfull" );

		log.info( "'Manage users - delete and change users' test finished" );
	}

}
