package edu.kit.soda4lca.test.ui.admin;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Just looking around on the admin site, if every button is available
 * 
 * @author mark.szabo
 * 
 */
public class T010Basic_AdminTest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T010Basic_AdminTest.class );

	/**
	 * 
	 * Just looking around on the admin site, if every button is available
	 * 
	 * @throws InterruptedException
	 */
	@Test( priority = 101 )
	public void adminBasic() throws InterruptedException {
		log.info( "admin basic test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();
		// check the admin site
		log.debug( "looking for the menu and menu items" );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='adminMenuForm:adminMenu']" ) );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='admin_content']" ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.dataImport" ) ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "common.stock" ) ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.dataset.manageList" ) ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.user" ) ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.network" ) ) );
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.globalConfig" ) ) );
		log.info( "admin basic test finished" );
	}

}
