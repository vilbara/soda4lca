package edu.kit.soda4lca.test.ui.admin;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

public class T040NodesTest {

	protected final static Logger log = org.apache.log4j.Logger.getLogger( T040NodesTest.class );

	public void newNode( String www, String login, String password, boolean correct, boolean exist ) throws InterruptedException {
		log.trace( "Creating new node " + www );

		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.network" ) ) ) ).build().perform();
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.node.new" ) ) ) ).click().build()
				.perform();
		Thread.sleep( 3 * TestContext.wait );
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );
		// Fill in the form
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='baseUrl']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='baseUrl']" ) ).sendKeys( www );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='login']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='login']" ) ).sendKeys( login );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='passw']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='passw']" ) ).sendKeys( password );
		Thread.sleep( 3 * TestContext.wait );

		// Click Save
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.node.new" ) + "')]" ) ).click();
		// wait a little
		Thread.sleep( 3 * TestContext.wait );
		// check the message
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
				.xpath( ".//*[@id='messages']/div" ) ) );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']/div" ) );

		if ( correct ) {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
					TestContext.lang.getProperty( "facesMsg.node.addSuccess" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
			else {
				log.trace( "Node " + www + " created succesfull" );
			}
		}
		else if ( exist ) {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
					TestContext.lang.getProperty( "facesMsg.node.alreadyExists" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
		}
		else {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
					TestContext.lang.getProperty( "facesMsg.node.infoError" ).substring( 0, 10 ) )
					&& !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
							TestContext.lang.getProperty( "facesMsg.node.noID" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
		}
	}

	public void editNode( String nodeId, String password, boolean correct ) throws InterruptedException {
		log.trace( "Edit node " + nodeId );

		// EDIT
		log.trace( "Find node to edit" );
		int i = 1;

		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='nodeTable_data']/tr[" + Integer.toString( i ) + "]/td[2]/a" ) ).getText().contains(
				nodeId ) )
			i++;

		Thread.sleep( 3 * TestContext.wait );

		// CHANGE
		log.debug( "Change a password" );

		TestFunctions.findAndWaitOnElement( By.linkText( nodeId ) ).click();
		TestFunctions.findAndWaitOnElement( By.xpath( "//input[@id='passw']" ) ).clear();
		TestFunctions.findAndWaitOnElement( By.xpath( "//input[@id='passw']" ) ).sendKeys( password );
		Thread.sleep( 3 * TestContext.wait );

		// Click Save
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.node.updateNode" ) + "')]" ) ).click();
		// wait a little
		Thread.sleep( 3 * TestContext.wait );

		if ( correct ) {
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
					.xpath( ".//*[@id='messages']/div" ) ) );
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']/div" ) );

			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
					TestContext.lang.getProperty( "facesMsg.node.saveSuccess" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
			else {
				log.trace( "Node " + nodeId + " updated succesfull" );
			}
		}
	}

	public void deleteNode( String nodeId, boolean correct ) throws InterruptedException {
		log.trace( "Delete node " + nodeId );

		// DELETE
		log.trace( "Find node to delete" );
		int i = 1;

		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='nodeTable_data']/tr[" + Integer.toString( i ) + "]/td[2]/a" ) ).getText().contains(
				nodeId ) )
			i++;

		Thread.sleep( 3 * TestContext.wait );

		log.debug( "Delete node" );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='nodeTable_data']/tr[" + Integer.toString( i ) + "]/td[1]/div/div[2]" ) ).click();
		Thread.sleep( 3 * TestContext.wait );
		// Click 'Delete selected entries'
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='deleteBtn']" ) ).click();
		// Are you sure? - Click OK
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.ok" ) + "')]" ) ).click();
		log.trace( "Check if deleted succesfully" );

		// wait a little
		Thread.sleep( 3 * TestContext.wait );

		if ( correct ) {
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
					.xpath( ".//*[@id='messages']/div" ) ) );
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']/div" ) );

			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
					TestContext.lang.getProperty( "facesMsg.removeSuccess" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
			else {
				log.trace( "Node " + nodeId + " deleted successfully" );
			}
		}
	}

	@Test( priority = 401, dependsOnMethods = { "edu.kit.soda4lca.test.ui.basic.T022Advanced_LoginTest.Advanced_LoginTest" } )
	public void newNodes() throws InterruptedException {
		log.info( "'Create new nodes' test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		Thread.sleep( 3 * TestContext.wait );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();
		Thread.sleep( 3 * TestContext.wait );

		// add nodes
		newNode( "http://localhost:8080/Node", "", "", false, false );
		newNode( "http://141.52.46.103/netzwerk/resource/", "admin", "test", true, false );
		newNode( "http://localhost:8080/Registry", "", "", false, false );
		newNode( "http://141.52.46.103/netzwerk/resource/", "admin", "def", false, true );
		
		log.info( "'Create new nodes' test finished" );
	}

	@Test( priority = 402, dependsOnMethods = { "newNodes" } )
	public void manageNodes() throws InterruptedException {
		log.info( "'Manage nodes' test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();
		Thread.sleep( 3 * TestContext.wait );
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.network" ) ) ) ).build().perform();
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.node.manageList" ) ) ) ).click().build()
				.perform();
		Thread.sleep( 3 * TestContext.wait );
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );

		// edit node
		editNode( "NETLZD", "def", true );
		Thread.sleep( 3 * TestContext.wait );

		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.network" ) ) ) ).build().perform();
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.node.manageList" ) ) ) ).click().build()
				.perform();
		Thread.sleep( 3 * TestContext.wait );

		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );

		// delete node
		deleteNode( "NETLZD", true );

		log.info( "'Manage nodes' test finished" );

	}
}
