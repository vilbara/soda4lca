package edu.kit.soda4lca.test.ui.admin;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Delete a dataset from every type and test if it gets really deleted
 * 
 * @author mark.szabo
 * 
 */
public class T030ManageDataSetsTest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T030ManageDataSetsTest.class );

	/**
	 * Delete a dataset from every type and test if it gets really deleted. It's almost the same for every type, so this
	 * method can delete from which you want
	 * 
	 * @param menu
	 *            Language property "admin." + menu + ".manageList" contains the menu link text
	 * @param link
	 *            Table id: "[@id='" + link+ "Table_data']"
	 * @param isitUnitGroup
	 *            usually almost any dataset can be deleted. But because of dependences by unit Group we have to delete
	 *            a specified one
	 * @throws InterruptedException
	 */
	public void manageSomething( String menu, String link, boolean isitUnitGroup ) throws InterruptedException {
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
		log.info( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) + " test started" );
		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "admin.globalConfig" ) ) ) );
		// Create an action for mouse-moves
		Actions action = new Actions( TestContext.getInstance().getDriver() );
		// Mouse over the menu 'User'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.dataset.manageList" ) ) ) ).build()
				.perform();
		// Mouse over and click the submenu 'Manage ***'
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) ) ) ).click()
				.build().perform();
		log.trace( "Change datastock" );
		// Choose data stock RootStock1
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='admin_header']/div[2]//table/tbody/tr[1]/td[2]/div/div[3]/span" ) ).click();
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@data-label='RootStock1']" ) ).click();
		// TestFunctions.findandwaitanElement(By.xpath(".//*[ends-with(@id, 'selectDataStock_panel')]/div/ul/li[2]")).click();
		// wait
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.xpath( ".//*[@id='" + link
 + "Table_data']/tr[1]/td[1]/div/div[2]/span" ) ) );
		/*
		 * UnitGroups has a lot of dependences
		 * so we have to delete a specific one, which can be deleted
		 * it will be "Units of volume*length"
		 * in other cases just delete the first one
		 */
		String name;
		log.trace( "select dataset" );

		// read the name of the second element
		name = TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Table_data']/tr[2]/td[2]/a" ) ).getText();
		// select the first item
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Table_data']/tr[1]/td[1]/div/div[2]/span" ) ).click();

		// wait for Delete button to enabled
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.elementToBeClickable( By.xpath( ".//*[@id='" + link
				+ "Tablebtn']" ) ) );
		// click Delete Selected Entries
		log.trace( "Click delete" );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Tablebtn']" ) ).click();
		// Sure? Click OK
		TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.ok" ) ) ).click();
		// check the message
		if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains(
				TestContext.lang.getProperty( "facesMsg.removeSuccess" ).substring( 0, 10 ) ) )
			org.testng.Assert.fail( "Wrong message: " + TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText() );
		// wait
		Thread.sleep( TestContext.wait * 10 );
		// refresh page
		TestContext.getInstance().getDriver().navigate().refresh();
		log.trace( "check if element was deleted correctly" );
		// check if element was deleted correctly
		if ( TestFunctions.isElementNotPresent( By.linkText( name ) ) )
			org.testng.Assert.fail( "It seems to be delete an emelent, but the element is still there" );

		log.info( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) + " test finished" );
	}

	@Test( priority = 301, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageProcesses() throws InterruptedException {
		manageSomething( "process", "process", false );
	}

	@Test( priority = 302, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageLCIAMethods() throws InterruptedException {
		manageSomething( "lciaMethod", "lciamethod", false );
	}

	@Test( priority = 303, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageElementaryFlows() throws InterruptedException {
		manageSomething( "elementaryFlow", "flow", false );
	}

	@Test( priority = 304, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageProductFlows() throws InterruptedException {
		manageSomething( "productFlow", "flow", false );
	}

	@Test( priority = 305, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageFlowProperties() throws InterruptedException {
		manageSomething( "flowProperty", "flowproperty", false );
	}

	@Test( priority = 306, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageUnitGroups() throws InterruptedException {
		manageSomething( "unitGroup", "unitgroup", true );
	}

	@Test( priority = 307, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageSources() throws InterruptedException {
		manageSomething( "source", "source", false );
	}

	@Test( priority = 308, dependsOnMethods = { "edu.kit.soda4lca.test.ui.admin.T015ImportExportTest.importData" } )
	public void manageContacts() throws InterruptedException {
		manageSomething( "contact", "contact", false );
	}
}
