![ ](images/soda4LCA_logo_sm.png)

# soda4LCA ${project.version} Developer Guide #

## Integration Tests ##

### Introduction ###

The integration tests are a way to ensure the functionality of all components of soda4LCA. Because the application consists of several modules (*Node* and *Registry* being the two which are actual web applications), after code changes it has to be ensured that everything is still working fine, especially interactions between the *Node* and *Registry* applications.

For that purpose, comprehensive integration tests have been created that can be run in any environment without requiring things like a servlet container or local database. 


### Prerequisites ###

In addition to the requirement by the application, *Perl* needs to be installed on the machine where the integration tests are executed. 


### Running Integration Tests from Maven ###

#### Node Application ####

For running the integration tests for the *Node* application, just execute the Maven goal `verify`, for example by calling

    mvn verify

on the command line. 

The tests will run in an embedded servlet container with an embedded database.