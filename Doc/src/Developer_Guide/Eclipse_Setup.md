![ ](images/soda4LCA_logo_sm.png)

# soda4LCA ${project.version} Developer Guide #

## Setup Eclipse IDE for soda4LCA Development ##

### Prerequisites ###

This guide assumes you have the following software installed on your system:

- Java 1.7 or newer
- Eclipse Mars Java EE edition
- MySQL 5.x (needs to be running, a remote instance will also do) 
- Apache Tomcat 8.0

It also assumes you have already a local Git repository with a clone of the soda4LCA repo. If you do not have already, Eclipse's *Git Repositories* view allows you to easily clone a remote repo.


### Importing Projects ###

1. Open the *Git Repositories* view and add your local soda4LCA Git repository (or clone the remote one).
 
![Git Repositories View](images/IDE_setup/Eclipse001GitRepos.png)


2. Now right-click in the *Project Explorer* view  and select *Import...*, then *Git/Projects from Git*.

![](images/IDE_setup/Eclipse002ImportFromGit.png)

3. Select *Existing local repository* and then *soda4LCA*.

![](images/IDE_setup/Eclipse003Import.png)

![](images/IDE_setup/Eclipse004Import.png)

4. Then, select *Import existing Eclipse projects*.

![](images/IDE_setup/Eclipse005Import.png)

5. In the next step, all modules should be highlighted. Click *Finish*.

![](images/IDE_setup/Eclipse007Import.png)

6. Now all modules are shown in the *Project Explorer* view.

![](images/IDE_setup/Eclipse008Imported.png)



### Setting up the Servlet Container ###

Open the *Servers* view and click to add a new server.
 
![](images/IDE_setup/Eclipse010Servers.png)

Choose *Tomcat v8.0 Server* from the list.

![](images/IDE_setup/Eclipse011ServersAddTomcat.png)

Add the `Node` module to the list of configured modules and finish the wizard.

![](images/IDE_setup/Eclipse012ServersAddNodeModule.png)


Doubleclick on the newly created *Tomcat 8.0* entry in the *Servers* view and navigate to the *Modules* tab. 

![](images/IDE_setup/Eclipse012aServersEditNodeModule.png)

Uncheck the *Auto reloading enabled* option.

![](images/IDE_setup/Eclipse012bServersEditNodeModule.png)

Now go to the `Servers` folder in the *Project Explorer* view and edit the `server.xml` file.

Paste the following line into `GlobalNamingResources` section, replacing your database URL, username and password:

		<Resource auth="Container" driverClassName="com.mysql.jdbc.Driver"
			logAbandoned="true" maxActive="8" maxIdle="4" name="jdbc/soda4LCAdbconnection"
			username="user" password="pass" removeAbandoned="true"
			removeAbandonedTimeout="300" testOnBorrow="true" type="javax.sql.DataSource"
			url="jdbc:mysql://localhost/soda_test?useUnicode=yes&amp;characterEncoding=UTF-8"
			validationQuery="SELECT 1" />


![](images/IDE_setup/Eclipse015ServerConfigDB.png)


In the *Servers* folder of your workspace, create a `soda4LCA.properties` file from the template in the `Doc` module. 

![](images/IDE_setup/Eclipse017ServerSoda4LCAProps.png)


Now edit the `context.xml` file and adjust the location of your `soda4LCA.properties` file:

	<Environment name="soda4LCAProperties"
		value="/path/to/workspace/Servers/Tomcat v8.0 Server at localhost-config/soda4LCA.properties"
		type="java.lang.String" />


![](images/IDE_setup/Eclipse016ServerConfigContext.png)


### Starting the Servlet Container ###

Now you can use the *Start* button in the *Servers* view to start your local Tomcat instance.

![](images/IDE_setup/Eclipse019ServerStart.png)



### Misc ###

For autocomplete/content assist for JSF, refer to this posting:

[http://stackoverflow.com/questions/2136218/eclipse-autocomplete-content-assist-with-facelets-jsf-and-xhtml#7242705](http://stackoverflow.com/questions/2136218/eclipse-autocomplete-content-assist-with-facelets-jsf-and-xhtml#7242705)

