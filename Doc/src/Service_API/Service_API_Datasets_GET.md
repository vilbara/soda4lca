---

GET Datasets
============

Basic GET operations that return a list of datasets. Applies to all
dataset types.

Requests
--------

### Syntax

    GET /processes
    GET /flows
    GET /flowproperties
    GET /unitgroups
    GET /sources
    GET /contacts
    GET /lciamethods

### Request Parameters

| Name           | Description                                               |
| :------------: | :-------------------------------------------------------- |
| *startIndex*   | As all result sets are paged, this specifies the index of the first item of the entire result set of the operation that shall be included in the response.|
|                | Type: Integer                                             |
|                | Default: 0                                                |
| *pageSize*     | The page size (number of items) for the response.         |
|                | Type: Integer                                             |
|                | Default: 500                                              |
| *search*       | Perform a search query that will return results matching the given query parameters.|
|                | Type: Boolean                                             |
|                | Default: false                                            |
| *distributed*  | Perform a distributed search across all registered network nodes. |
|                | Type: Boolean                                             |
|                | Default: false                                            |
| *name*         | search parameter                                          |
|                | Type: String                                              |
|                | Default: None                                             |
| *description*  | search parameter                                          |
|                | Type: String                                              |
|                | Default: None                                             |
| *classId*      | search parameter                                          |
|                | Type: String                                              |
|                | Default: None                                             |
| *lang*         | Return results that support the specified language. Currently, if an object's name field is present in a certain language, the dataset is seen as supporting that language.|
|                | Type: String                                              |
|                | Default: None                                             |
| *allVersions*  | Return all available versions for each dataset. By default, only the most recent version will be contained in the result. |
|                | Type: Boolean                                             |
|                | Default: false                                            |

Responses
---------

### Response Elements

The response returned is a list of dataset overview objects, wrapped in
a dataSetList object. See the section ["DataSetList Response
Elements"](#Response_Elements_DatasetList) for a detailed description.

Examples
--------

### Sample Request

    GET /processes

### Sample Response

    HTTP/1.1 200 OK
    Content-Type: application/xml

~~~~ {.myxml}
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<serviceapi:dataSetList xmlns:serviceapi="http://www.ilcd-network.org/ILCD/ServiceAPI" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:process="http://www.ilcd-network.org/ILCD/ServiceAPI/Process" serviceapi:totalSize="4" serviceapi:startIndex="0" serviceapi:pageSize="500">
    <process:process serviceapi:sourceId="ACME" xlink:href="http://lci.acme.com/DB/processes/0a1b40db-5645-4db8-a887-eb09300b7b74">
        <serviceapi:uuid>0a1b40db-5645-4db8-a887-eb09300b7b74</serviceapi:uuid>
        <serviceapi:permanentUri>http://lca.jrc.ec.europa.eu/lcainfohub/datasets/elcd/processes/0a1b40db-5645-4db8-a887-eb09300b7b74.xml</serviceapi:permanentUri>
        <serviceapi:dataSetVersion>03.00.000</serviceapi:dataSetVersion>
        <serviceapi:name xml:lang="en">Electricity Mix;AC;consumption mix, at consumer;1kV - 60kV</serviceapi:name>
        <serviceapi:classification name="ilcd">
            <serviceapi:class level="0">Energy carriers and technologies</serviceapi:class>
            <serviceapi:class level="1">Electricity</serviceapi:class>
        </serviceapi:classification>
        <process:type>LCI result</process:type>
        <process:location>EU-27</process:location>
        <process:time>
            <process:referenceYear>2002</process:referenceYear>
            <process:validUntil>2010</process:validUntil>
        </process:time>
        <process:parameterized>false</process:parameterized>
        <process:hasResults>false</process:hasResults>
        <process:lciMethodInformation>
            <process:methodPrinciple>Attributional</process:methodPrinciple>
            <process:approach>Allocation - mass</process:approach>
            <process:approach>Allocation - market value</process:approach>
            <process:approach>Allocation - exergetic content</process:approach>
            <process:approach>Allocation - net calorific value</process:approach>
        </process:lciMethodInformation>
        <process:complianceSystem name="ILCD Data Network - Entry-level">
            <serviceapi:reference type="source data set" version="00.00.000" uri="../sources/d92a1a12-2545-49e2-a585-55c259997756.xml"/>
            <process:overallCompliance>Not compliant</process:overallCompliance>
            <process:nomenclatureCompliance>Fully compliant</process:nomenclatureCompliance>
            <process:methodologicalCompliance>Fully compliant</process:methodologicalCompliance>
            <process:reviewCompliance>Not compliant</process:reviewCompliance>
            <process:documentationCompliance>Not compliant</process:documentationCompliance>
            <process:qualityCompliance>Not defined</process:qualityCompliance>
        </process:complianceSystem>
        <process:accessInformation/>
   </process:process>
</serviceapi:dataSetList>
~~~~
