---

GET Process Datasets (Query)
============================

Requests
--------

### Syntax

    GET /

### Request Parameters

| Name           | Description                                               |
| :------------: | :-------------------------------------------------------- |
| **             | Type: String                                              |
|                | Default: None                                             |

Responses
---------

### Response Elements

| Name           | Description                                               |
| :------------: | :-------------------------------------------------------- |
| **             | Type:                                                     |
|                | Ancestors:                                                |

Examples
--------

### Sample Request

    GET /

### Sample Response

    HTTP/1.1 200 OK
    Content-Type: application/xml
