![ ](images/soda4LCA_logo_sm.png)

#soda4LCA ${project.version} Service API Documentation

## Operations 

[Service_API_Authentication](Service_API_Authentication.md)

[Service_API_Dataset_Flow_GET_Ancestors_Descendants](Service_API_Dataset_Flow_GET_Ancestors_Descendants.md)

[Service_API_Dataset_Flow_GET_Producers_Consumers](Service_API_Dataset_Flow_GET_Producers_Consumers.md)

[Service_API_Dataset_Flow_GET_Products](Service_API_Dataset_Flow_GET_Products.md)

[Service_API_Dataset_GET](Service_API_Dataset_GET.md)

[Service_API_Dataset_Process_GET_Exchanges](Service_API_Dataset_Process_GET_Exchanges.md)

[Service_API_Dataset_Process_GET_Referenceyears](Service_API_Dataset_Process_GET_Referenceyears.md)

[Service_API_Dataset_Process_GET_Validuntilyears](Service_API_Dataset_Process_GET_Validuntilyears.md)

[Service_API_Dataset_Source_GET_DigitalFile](Service_API_Dataset_Source_GET_DigitalFile.md)

[Service_API_Dataset_Source_POST](Service_API_Dataset_Source_POST.md)

[Service_API_Datasets_GET](Service_API_Datasets_GET.md)

[Service_API_Datasets_GET_Categories](Service_API_Datasets_GET_Categories.md)

[Service_API_Datasets_POST](Service_API_Datasets_POST.md)

[Service_API_Datasets_Process_Query](Service_API_Datasets_Process_Query.md)

[Service_API_Datastock_GET_Datasets](Service_API_Datastock_GET_Datasets.md)

[Service_API_Datastocks_GET](Service_API_Datastocks_GET.md)

[Service_API_Datastock_GET_Export](Service_API_Datastock_GET_Export.md)

[Service_API_Namespace_URIs](Service_API_Namespace_URIs.md)

[Service_API_NodeInfo](Service_API_NodeInfo.md)


## Response Elements

[Service_API_Response_CategoryList](Service_API_Response_CategoryList.md)

[Service_API_Response_Contact](Service_API_Response_Contact.md)

[Service_API_Response_DatasetList](Service_API_Response_DatasetList.md)

[Service_API_Response_Flow](Service_API_Response_Flow.md)

[Service_API_Response_FlowProperty](Service_API_Response_FlowProperty.md)

[Service_API_Response_IntegerList](Service_API_Response_IntegerList.md)

[Service_API_Response_LCIAMethod](Service_API_Response_LCIAMethod.md)

[Service_API_Response_Process](Service_API_Response_Process.md)

[Service_API_Response_Source](Service_API_Response_Source.md)

[Service_API_Response_UnitGroup](Service_API_Response_UnitGroup.md)