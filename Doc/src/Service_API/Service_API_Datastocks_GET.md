---

GET Datastocks
==============

GET operation that returns a list of all datastocks.

Requests
--------

### Syntax

    GET /datastocks

### Request Parameters

None.

Responses
---------

### Response Elements

| Name           | Description                                               |
| :------------: | :-------------------------------------------------------- |
| *dataStockList* (datastock) | The container element for the list of data stock objects. |
|                | Type: Container                                           |
|                | Ancestors: none.                                          |
| *dataStock* (datastock)    | The container element for the data stock object.          |
|    			 | Type: Container                                           |
|                | may occur multiple times                                  |
|                | Ancestors: dataStockList                                  |
| *@root* (datastock)     | Indicates whether the data stock is a root data stock.    |
| 			     | Type: Boolean                                             |
|                | Ancestors: dataStock                                      |
| *uuid*         | The UUID of the data stock.                               |
|                | Type: UUID                                                |
|                | Ancestors: dataStock                                      |
| *shortName*    | The short name (handle) of the data stock.                |
|                | Type: String                                              |
|                | Ancestors: dataStock                                      |
| *name*         | The name of the data stock.                               |
|                | Type: String Multilang                                    |
|                | may occur multiple times                                  |
|                | Ancestors: dataStock                                      |
| *description* (datastock)  | A description for the data stock.             |
|    			 | Type: String Multilang                                    |
|                | may occur multiple times                                  |
|                | Ancestors: dataStock                                      |

Examples
--------

### Sample Request

    GET /datastocks

### Sample Response

    HTTP/1.1 200 OK
    Content-Type: application/xml

~~~~ {.myxml}
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<dataStockList xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.ilcd-network.org/ILCD/ServiceAPI/DataStock ../schemas/ILCD_Service_API_DataStocks.xsd"
    xmlns="http://www.ilcd-network.org/ILCD/ServiceAPI/DataStock" xmlns:ds="http://www.ilcd-network.org/ILCD/ServiceAPI/DataStock" xmlns:serviceapi="http://www.ilcd-network.org/ILCD/ServiceAPI">
    <dataStock ds:root="true">
        <serviceapi:uuid>8945a2f5-cdda-4ccd-b357-6debb3898ddd</serviceapi:uuid>
        <serviceapi:shortName>default</serviceapi:shortName>
        <serviceapi:name xml:lang="en">Default Root Data Stock</serviceapi:name>
            <serviceapi:name xml:lang="de">deutscher Name</serviceapi:name>
        <description xml:lang="en">description</description>
        <description xml:lang="de">deutsche Beschreibung</description>
    </dataStock>
    <dataStock>
        <serviceapi:uuid>600695cc-24da-4f9d-b549-78d0c5e126d0</serviceapi:uuid>
        <serviceapi:shortName>other</serviceapi:shortName>
        <serviceapi:name xml:lang="en">Other, Non-Root Data Stock</serviceapi:name>
        <description xml:lang="en">other data</description>
    </dataStock>
</dataStockList>
~~~~
