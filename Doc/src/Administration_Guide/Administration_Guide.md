![ ](images/soda4LCA_logo_sm.png)

# soda4LCA ${project.version} Administration Guide


General Information
===================

Before performing any administrative tasks on a database node, you have
to be authenticated with administrative permissions. The application
ships with a default administrative account, `admin`. The default
password for this account is `default`.

> **Important**
>
> Make sure you change the default password immediately after setting up
> the node.

Logging in as administrator
---------------------------

To log in to the application, click on "Login" on the lower left hand
side of the page. The login dialog appears:

![Login dialog](images/soda4LCA_Admin_Login.png)

Enter valid credentials and confirm by clicking "Login".

Upon successful login, the administrative menu bar will appear in the
top section of the page.

![ ](images/soda4LCA_Admin_Menu.png)

Changing the default password
-----------------------------

Once logged in, click on "My Profile" in the lower left hand corner of
the page. Then enter the new password in the "Password" and "Repeat
Password" fields.

![ ](images/soda4LCA_Admin_MyProfile_Change_Password_0.png)

Confirm by selecting the button "Change user information".

![ ](images/soda4LCA_Admin_MyProfile_Change_Password_1_Confirm.png)

A confirmation message will be shown indicating that the password has
been changed.

![ ](images/soda4LCA_Admin_MyProfile_Change_Password_2_Confirmed.png)

Managing Access
===============

User and Group Management
-------------------------

For managing access to the various features of the application as well
via the web user interface as well as the application service interface,
a role-based approach has been implemented.

By default, an anonymous user (one that is not authenticated) can
browse, search and read all datasets that are public. For all other
operations, special permissions are necessary. The following table lists
all available permissions (roles):

| Role                  |   Description									|
|------------ | -------------------------------------------------------- |
| *Metadata read*       |   This role grants permission to read the metadata of process datasets.|
|*Full read and export*|   This role grants permission to read/display all sections of a process dataset as well as export (download) it as XML.|
|*Checkout for editing*|   This role grants permission to check out a dataset for editing. The dataset will be locked for edit operations until checked in again.|
|*Checkin of data sets*|   This role grants permission to check in a dataset after editing.|
|*Release management*  |   This role grants permission to modify the *released* / *unreleased* status of a dataset.|
|*Delete data sets*    |   This role grants permission to delete datasets from the database.|
|*Manage user*         |   This role grants permission to add, edit and remove users and groups.|
|*Admin rights*        |   This role grants permission to access the administrative functions via the web interface.|
[Access rights (roles)]

These roles can be assigned to groups. For instance, an imaginary group
TOOLS may be assigned the roles *Checkout for editing* and *Checkin of
data sets*. Furthermore, a user can be assigned to a group. For example,
the user "tool1" could be assigned to the group TOOLS, hence this user
account may connect to the database and perform dataset checkin and
checkout operations.

### Managing users

The "User" menu offers the options to add and manage users or groups.

![ ](images/soda4LCA_Admin_Menu_Users.png)

#### Adding a user

To add a user, select "Add user" from the User menu. The "Create User"
page will appear.

![ ](images/soda4LCA_Admin_User_Create.png)

Fields marked with an asterisk (\*) are required. When finished with
entering the data, select the button "Create user":
![ ](images/soda4LCA_Admin_User_Create_Confirm_Button.png)

#### Editing a user

To edit a user profile, select "Manage users" from the "User" menu. A
page with the list of all users will appear. You can edit a user profile
by clicking its name, or delete users by selecting one or multiple
entries using the checkbox in the left column and then selecting the
"Delete" button.

![ ](images/soda4LCA_Admin_User_ManageUsers.png)

### Managing groups

#### Adding a group

To add a group, select "Add group" from the User menu. The "Create
Group" page will appear. You can select the roles that you want to be
associated with this group, as well as the users you want to belong to
this group. This can be changed at a later time as well.

![ ](images/soda4LCA_Admin_User_CreateGroup.png)

#### Editing a group

To edit a group, select "Manage groups" from the "User" menu. A page
with the list of all groups will appear, with the list of roles
associated with each group showing in the "Roles associated" column. You
can edit a group by clicking its name, or delete groups by selecting one
or multiple entries using the checkbox in the left column and then
selecting the "Delete" button.

![ ](images/soda4LCA_Admin_User_ManageGroups.png)

Managing Datasets
=================

Data Import
-----------

To import datasets into your soda4LCA node, select "Import" from the
"Data Import/Export" menu:

![ ](images/soda4LCA_Admin_Menu_Import.png)

Then select "Browse":

![ ](images/soda4LCA_Admin_Import_0.png)

Now an operating system file dialog will appear, allowing you to select
the desired file(s):

![ ](images/soda4LCA_Admin_Import_1_Select.png)

You may select one or several of the following file types:

-   XML documents containing ILCD datasets

-   ILCD ZIP archives containing ILCD datasets

When you are finished selecting files and confirm the file system
dialog, a list of selected files will appear on the page. Click "Upload"
to upload them to the server.

![ ](images/soda4LCA_Admin_Import_2_Pre_Upload.png)

Once the upload is finished, a corresponding message will appear:

![ ](images/soda4LCA_Admin_Import_3_Upload_Complete.png)

Now select the "Continue to step 2" button. A list of all uploaded files
will appear.

![ ](images/soda4LCA_Admin_Import_4_Pre_Import.png)

Select "Import files". As the import progresses, messages will be
written to the status log console.

> **Important**
>
> During the import, do not reload the page.

![ ](images/soda4LCA_Admin_Import_5_Import_Finished.png)

When the message "Import of files finished" appears, the import is
complete.

Review the status log for error messages. When a dataset of the same
UUID and version already exists in the database, this dataset will not
be imported and a message is written to the status log.

Data Export
-----------

The entire database can be exported to a ZIP file that can be stored
locally on the client. That file can later be imported to an empty
database.

To export the database, select "Export Database" from the "Data
Import/Export" Menu.

![ ](images/soda4LCA_Admin_Menu_Export.png)

Then click the link "Export entire database to ZIP file".

![ ](images/soda4LCA_Admin_Export_Export.png)

Your browser will prompt you with a download dialog and download the ZIP
file.

Versioning
----------

Each dataset carries a unique UUID (Universally Unique Identifier) as
well as a version number that is to be incremented when the dataset is
updated.

In general, always only the latest version of a dataset (i.e. the
dataset with the highest version number) is shown. Previous versions can
be accessed by navigating to the most recent version and then selecting
the desired version from the "Other versions" section on the dataset
overview page:

![ ](images/soda4LCA_Dataset_Versions.png)

Deleting datasets from the database
-----------------------------------

To delete one or more datasets, select the appropriate entry from the
"Manage Datasets" menu:

![ ](images/soda4LCA_Admin_Menu_Manage.png)

Then navigate to the dataset(s) that you intend to delete. Each dataset
version will be displayed as a separate entry.

![ ](images/soda4LCA_Admin_Manage_Processes.png)

For every dataset to be deleted on a page, select the checkbox in the
first column. Then select the "Delete" button:

![ ](images/soda4LCA_Admin_Manage_Processes_Delete.png){width="400"}

A confirmation dialog will appear.

![ ](images/soda4LCA_Admin_Manage_Processes_Delete_Confirm.png)

Upon confirmation, the datasets will be deleted from the database and
for each deleted dataset, and information message is shown.

![ ](images/soda4LCA_Admin_Manage_Processes_Delete_Confirmed.png)

Managing Networking
===================

Displaying information about the local node
-------------------------------------------

To review the information exposed by the local node to other nodes on
the network, select "About this node" from the "Network" menu:

![ ](images/soda4LCA_Admin_Menu_Network.png)

The node and administrator information is shown. Refer to the
Installation Guide for information on how to edit this information.

![ ](images/soda4LCA_Admin_Network_AboutThisNode.png)

Adding nodes
------------

To add a node to the list of foreign nodes known to the local node,
select "Add Node" from the "Network" menu. On the following page, you
can enter the following:

-   a service URL (required)

-   a username/password combination (optional).

![ ](images/soda4LCA_Admin_Network_01_Add_Node.png)

Enter the service URL of the node you want to add and select the "Add
node" button:

![ ](images/soda4LCA_Admin_Network_01a_Add_node.png)

The application will attempt to connect to the foreign node and retrieve
its metadata. If this was successful, the node is added to the list of
foreign nodes, and a confirmation message will appear:

![ ](images/soda4LCA_Admin_Network_02_Confirmation.png)

If the connection fails (due to the foreign node being unreachable, the
entering service URL being incorrect etc.), the list of nodes will not
be altered and the "Add node" page will still be shown.

Managing nodes
--------------

To see the list of foreign nodes, select "Manage Nodes" from the
"Network" menu.

![ ](images/soda4LCA_Admin_Network_03_Manage.png)

To see the details about a node, click on its name. The details/update
page will appear.

![ ](images/soda4LCA_Admin_Network_04_Show_Edit.png)

If necessary, you may change the information and confirm by clicking
"Update node information".
Registering with a registry
===========================

Note: The functionality described in this chapter is not yet available
in the 1.2.0 release.

Adding a new registry
---------------------

In order to utilize the advanced network capabilities of the
application, the node needs to be registered with an existing registry.
Before the registration process can start, the registry has to be added
to the application's list of known registries. Only users with
administrator privileges are allowed to perform this operation. In order
to add a new registry:

1.  Navigate to Network -&gt; Registries

2.  Press the "Add registry" button:

  ![ ](images/soda4LCA_Admin_Registry_01_Add.png)

3.  The following form will be displayed:

   ![ ](images/soda4LCA_Admin_Registry_02_Enter_Details.png)

4.  Fill in all mandatory fields and press "Save"

All values that have to be inserted will be published. It is important
to insert the exact values, especially UUID and Base URL. In case the
network doesn’t work as expected, please check the value of UUID.

Showing registry details
------------------------

Users with appropriate privileges have to possibility to inspect the
details of a registry.

1.  Select "Registries" from the "Network" menu

   ![ ](images/soda4LCA_Admin_Registry_Menu.png)

2.  Click on the appropriate registry name and consult the
    registry details.
    
 ![ ](images/soda4LCA_Admin_Registry_03_List_of_Registries.png)

3.  When a node is the part of the network, registry details and a list
    of nodes in the network will be displayed. Otherwise, only the
    registry details will be shown.
    
 ![ ](images/soda4LCA_Admin_Registry_04_Details.png)

Editing registry details
------------------------

Users with appropriate privileges have to possibility to edit details of
a registry.

1.  Navigate to "Network" -&gt; "Registries"

2.  Click "Edit registry" in the "Action" column of the selected
    registry
    
 ![ ](images/soda4LCA_Admin_Registry_05_List_of_Registries.png)

3.  You will be redirected to the "Registry details" page.

 ![ ](images/soda4LCA_Admin_Registry_06_Edit_Details.png)

4.  Insert new values and press "Save".

Removing a registry
-------------------

Users with appropriate privileges have the possibility to remove a
registry from the list of registries known to the node. Registries on
which the node is already registered cannot be removed, the node has to
be deregistered first.

1.  Navigate to Network -&gt; Registries

2.  Click the "Edit registry" link in the "Action" column of the
    selected registry.

 ![ ](images/soda4LCA_Admin_Registry_07_Remove_Registry.png)

3.  The system will ask for confirmation.

 ![ ](images/soda4LCA_Admin_Registry_08_Remove_Confirmation.png)

4.  Confirm the choice.

5.  A confirmation message will be displayed.

 ![ ](images/soda4LCA_Admin_Registry_09_Remove_Confirmed.png)

Node registration
-----------------

To send a node registration request to a registry, execute the following
steps:

1.  Navigate to Network -&gt; Registries

 ![ ](images/soda4LCA_Admin_Registry_Menu.png)

2.  Select "Registry" and in the "Action" column click on "Register" –
    the registration page will appear.

 ![ ](images/soda4LCA_Admin_Registry_10_Node_Registration.png)

3.  Complete the "Access Account" and "Access Password" fields. Note:
    these fields are not your credentials for the node application!
    These fields will be used to authenticate the deregistration action,
    so keep this information for later. Node ID and Base URL are entered
    by default by the system, but it is possible to change the values.
    Please be careful with the URL – in case of an incorrect value, the
    registration will be not processed.

 ![ ](images/soda4LCA_Admin_Registry_11_Register_Node.png)

4.  After successfully sending the node registration request, the status
    of this node on the registry is "Pending registration".

 ![ ](images/soda4LCA_Admin_Registry_12_Pending.png)

5.  When the registry administrator approves your request, the status
    will be changed to "Registered". You will be also informed about the
    approval by email.

 ![ ](images/soda4LCA_Admin_Registry_13_Registered.png)

   Note: A node can be registered in multiple networks.
   ![ ](images/soda4LCA_Admin_Registry_14_Multiple_Networks.png)

Node deregistration
-------------------

In order to deregister a node from a registry, follow these steps:

1.  Navigate to Network -&gt; Registries

2.  Find the registry you want to deregister from and click the
    "Deregister" link in the "Action" column

 ![ ](images/soda4LCA_Admin_Registry_15_Deregistration.png)

3.  You will be redirected to the authorization page. Enter user and
    password which were selected during registration and click
    Deregister

 ![ ](images/soda4LCA_Admin_Registry_16_Deregister_Authenticate.png)

4.  After pressing the button, you will be asked for confirmation. Click
    "Yes":

 ![ ](images/soda4LCA_Admin_Registry_17_Deregister_Confirm.png)

5.  The system will prevent you from deregistration if there are any
    registered datasets. You will be asked to deregister the
    datasets first.

 ![ ](images/soda4LCA_Admin_Registry_18_Error_Registered_Datasets.png)

   If the user name and/or password entered are incorrect, an
    appropriate message will be displayed.
 ![ ](images/soda4LCA_Admin_Registry_19_Error_Authentication.png)
