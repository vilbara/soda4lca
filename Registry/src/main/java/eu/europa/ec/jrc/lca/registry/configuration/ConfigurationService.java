package eu.europa.ec.jrc.lca.registry.configuration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.flyway.core.Flyway;
import com.googlecode.flyway.core.exception.FlywayException;
import com.googlecode.flyway.core.validation.ValidationException;

public class ConfigurationService {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationService.class);

	public static void migrateDatabaseSchema() {
		try {
			Context ctx = new InitialContext();
			DataSource dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/registryDS");

			Flyway flyway = new Flyway();
			flyway.setDataSource(dataSource);
			flyway.setBasePackage("de.iai.ilcd.db.migrations");
			flyway.setBaseDir("sql/migrations");

			ConfigurationService.logSchemaStatus(flyway);

			try {
				flyway.validate();
			} catch (ValidationException e) {
				ConfigurationService.logger
						.error("database schema: could not successfully validate database status, database needs to be initialized");
				throw new RuntimeException("FATAL ERROR: database schema is not properly initialized", e);
			}

			int migrations = flyway.migrate();

			if (migrations > 0) {
				ConfigurationService.logger.info("database schema: successfully migrated");
				ConfigurationService.logSchemaStatus(flyway);
			}

		} catch (FlywayException e) {
			ConfigurationService.logger.error("error migrating database schema", e);
			throw new RuntimeException("FATAL ERROR: database schema is not properly initialized", e);
		} catch (NamingException e) {
			ConfigurationService.logger.error("error looking up datasource", e);
			throw new RuntimeException("FATAL ERROR: could not lookup datasource", e);
		}

	}

	private static void logSchemaStatus(Flyway flyway) {
		if (flyway.status() != null) {
			ConfigurationService.logger.info("database schema: current version is " + flyway.status().getVersion());
		} else {
			ConfigurationService.logger.info("database schema: no migration has been applied yet.");
		}
	}

}
